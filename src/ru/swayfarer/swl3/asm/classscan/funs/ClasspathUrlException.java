package ru.swayfarer.swl3.asm.classscan.funs;

@SuppressWarnings("serial")
public class ClasspathUrlException extends RuntimeException {

    public ClasspathUrlException()
    {
        super();
    }

    public ClasspathUrlException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ClasspathUrlException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ClasspathUrlException(String message)
    {
        super(message);
    }

    public ClasspathUrlException(Throwable cause)
    {
        super(cause);
    }
}
