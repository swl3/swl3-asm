package ru.swayfarer.swl3.asm.classscan.funs;

import java.net.JarURLConnection;
import java.util.jar.JarFile;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.path.PathHelper;
import ru.swayfarer.swl3.io.zip.IZipFile;
import ru.swayfarer.swl3.io.zip.JavaZipFile;
import ru.swayfarer.swl3.io.zip.memory.InMemoryZipFileReader;
import ru.swayfarer.swl3.system.SystemUtils;

@Getter
@Setter
@Accessors(chain = true)
public class ZipSourcesFun implements IFunction1<String, ExtendedList<String>> {

    @NonNull
    public InMemoryZipFileReader inMemoryZipReader = new InMemoryZipFileReader();
    
    @NonNull
    public ExtendedList<String> availableClasses = new ExtendedList<>();
    
    @NonNull
    public IFunction0<ExceptionsHandler> exceptionsHandler;
    
    @SneakyThrows
    public ZipSourcesFun(@NonNull IFunction0<ExceptionsHandler> exceptionsHandlerFun)
    {
        this.exceptionsHandler = exceptionsHandlerFun;
    }
    
    @SneakyThrows
    public static ZipSourcesFun ofClasspath(@NonNull SystemUtils systemUtils, @NonNull IFunction0<ExceptionsHandler> exceptionsHandlerFun)
    {
    	var ret = new ZipSourcesFun(exceptionsHandlerFun);
    	
    	for (var url : systemUtils.getClasspathAndClassloaderURLs()) 
        {
            try
            {
                var protocol = url.getProtocol();
                
                if (protocol.equals("file"))
                {
                    var file = FileSWL.of(url);
                    
                    if (file.isDirectory())
                    {
                    	ret.readDirectory(file);
                    }
                    else
                    {
                    	ret.readJar(new JavaZipFile(file.as().zip()));
                    }
                    
                }
                else if (protocol.equals("jar"))
                {
                    var exceptionsHandler = ret.getExceptionsHandler();
                    
                    exceptionsHandler.safe(() -> {
                        var jarUrlConnection = (JarURLConnection) url.openConnection();
                        JarFile jarFile = jarUrlConnection.getJarFile();
                        ret.readJar(new JavaZipFile(jarFile));
                    }, "Error while getting content of jar", url);
                }
            }
            catch (Throwable e)
            {
                var eh = exceptionsHandlerFun.apply();
                
                var cpurlEx = new ClasspathUrlException("For " + url, e);
                
                eh.handle(cpurlEx, "Error while adding url", url, "to classpath scanner");
            }
        }
    	
    	return ret;
    }
    
    public void readJar(IZipFile file)
    {
        var exceptionsHandler = getExceptionsHandler();
        
        exceptionsHandler.safe(() -> {
            var entries = file.findAllEntries()
                    .map((e) -> e.getName())
                    .filter((name) -> name.endsWith(".class"))
                    .transform(this::transformName)
            ;
            
            availableClasses.addAll(entries);
        }, "Error while getting content of jar", file);
    }
    
    public void readDirectory(FileSWL file)
    {
        var subfilesNames = file.findAllSubfiles()
            .map((f) -> f.getLocalPath(file))
            .filter((f) -> f.endsWith(".class"))
            .toExList()
            // Превращаем имя файла в имя класса
            .transform(this::transformName)
        ;
        
        availableClasses.addAll(subfilesNames);
    }
    
    public String transformName(String name)
    {
        var newName = name;
        newName = PathHelper.fixSlashes(newName);
        
        if (newName.startsWith("/"))
            newName = newName.substring(1);
        
        newName = newName.replace("/", ".")
                .substring(0, newName.length() - ".class".length());
        
        return newName;
    }
    
    public ExceptionsHandler getExceptionsHandler()
    {
        return exceptionsHandler.apply();
    }
    
    @Override
    public ExtendedList<String> applyUnsafe(String packageName)
    {
        return availableClasses.exStream()
                .filter((s) -> s.startsWith(packageName))
                .toExList()
        ;
    }
}
