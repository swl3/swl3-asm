package ru.swayfarer.swl3.asm.classscan;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data @Accessors(chain = true)
public class ClassSourceEntry {

    @NonNull
    public String packageName;
    
	@NonNull
	public String classSimpleName;
	
	public String getClassName()
	{
	    return packageName + "." + classSimpleName;
	}
}
