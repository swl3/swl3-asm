package ru.swayfarer.swl3.asm.classscan;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.classscan.funs.ZipSourcesFun;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.stream.ClassInfoStream;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.system.SystemUtils;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Getter
@Setter
@Accessors(chain = true)
public class ClassScan {

	public FunctionsChain1<ClassInfo, Boolean> filters = new FunctionsChain1<ClassInfo, Boolean>()
			.filter(Boolean.FALSE::equals)
	;

	public ExtendedList<String> scannedClassNames = CollectionsSWL.list();
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();

	public IObservable<ClassInfo> eventClassScan = Observables.createObservable()
			.exceptions().throwOnFail()
	;
	
	public IFunction1<String, ExtendedList<String>> packageToClassesFun = (e) -> {
	    throw new UnsupportedOperationException("Package to class fun was not set in " + ClassScan.this);
	};
	
	public BytecodeScanner bytecodeScanner = new BytecodeScanner();
	
	public void scan(@NonNull String pkg)
	{
		var classes = findClassesInPackage(pkg);
		
		if (classes != null)
		{
		    for (var classEntry : classes)
		    {
		    	scanClass(classEntry);
		    }
		}
	}

	public boolean needsToScanClass(ClassInfo classInfo)
	{
		if (Boolean.FALSE.equals(filters.apply(classInfo)))
			return false;

		return true;
	}
	
	public ClassInfo findClassInfo(@NonNull String className)
	{
	    var typeDescriptor = "L" + className.replace(".", "/") + ";";
        
        var type = Type.getType(typeDescriptor);
        var classInfo = bytecodeScanner.findOrCreateClassInfo(type);
        
        return classInfo;
	}
	
	public void scanClass(@NonNull String className)
	{
	    if (scannedClassNames.contains(className))
            return;
        
        var classInfo = findClassInfo(className);

        if (needsToScanClass(classInfo))
			eventClassScan.next(classInfo);
	}
	
	public ClassInfoStream stream()
	{
		return stream("");
	}
	
	public ClassInfoStream stream(@NonNull String pkg)
	{
	    var ret = findClassesInPackage(pkg)
				.distinct()
                .map(this::findClassInfo)
				.filter(this::needsToScanClass)
        ;
	    
	    return new ClassInfoStream(() -> ret.stream())
	    		.setBytecodeScanner(bytecodeScanner)
		;
	}
	
	public ExtendedList<String> findClassesInPackage(@NonNull String pkg)
	{
		return packageToClassesFun.apply(pkg);
	}
	
	public ClassScan useClasspath(@NonNull SystemUtils systemUtils)
	{
	    packageToClassesFun = ZipSourcesFun.ofClasspath(systemUtils, this::getExceptionsHandler);
	    return this;
	}
	
	{
	    bytecodeScanner.exceptionsHandler.andAfter(exceptionsHandler);
	}
    
    public static ClassScan ofClasspath()
    {
        return ofClasspath(SystemUtils.getInstance());
    }
	
	public static ClassScan ofClasspath(@NonNull SystemUtils systemUtils)
	{
	    return new ClassScan().useClasspath(systemUtils);
	}
}
