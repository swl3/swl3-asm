package ru.swayfarer.swl3.asm.proxy;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.proxy.rules.reflection.ReflectionOverrideRule;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Data
@Accessors(chain = true)
public class ProxyHelperConfigurator {
    
    @NonNull
    @Accessors(fluent = true)
    public ProxyHelper helper;
    
    public OverwriteRuleConfigurator rule()
    {
        var rule = new OverwriteRule();
        helper.getProxyGenerator().rules.add(rule);
        return new OverwriteRuleConfigurator(rule, this);
    }
    
    public ProxyHelperConfigurator enableReflection()
    {
        return rule().any().process(new ReflectionOverrideRule());
    }
    
    @Data
    @Accessors(chain = true)
    public static class OverwriteRuleConfigurator {
        
        @NonNull
        public OverwriteRule rule;
        
        @NonNull
        public ProxyHelperConfigurator configurator;
        
        public ProxyHelperConfigurator process(IFunction1NoR<OverwriteEvent> fun)
        {
            rule.overwriteFun = rule.overwriteFun.andAfter(fun);
            return configurator;
        }
        
        public OverwriteRuleConfigurator any()
        {
            return filter((evt) -> true);
        }
        
        public OverwriteRuleConfigurator filterByMethod(IFunction1<MethodInfo, Boolean> filter) 
        {
            return filter((evt) -> filter.apply(evt.getMethodInfo()));
        }
        
        public OverwriteRuleConfigurator filter(IFunction1<OverwriteEvent, Boolean> filter) 
        {
            rule.filterFun = rule.filterFun.andThan(filter);
            return this;
        }
    }
}
