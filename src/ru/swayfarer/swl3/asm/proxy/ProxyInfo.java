package ru.swayfarer.swl3.asm.proxy;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data
@AllArgsConstructor @NoArgsConstructor
@Accessors(chain = true)
public class ProxyInfo {

    @NonNull
    public AsmUtils asmUtils = new AsmUtils();
    
    @NonNull
    public BytecodeScanner bytecodeScanner = new BytecodeScanner();
    
    @NonNull
    public String className;
    
    @NonNull
    public ClassInfo superClass;

    public Object wrappedObject;
    
    @NonNull
    public ExtendedList<ClassInfo> interfaces = CollectionsSWL.list();

    @NonNull
    public ExtendedMap<String, Object> customData = new ExtendedMap<>();

    public Type getProxyClassType()
    {
        return Type.getType("L" + className.replace(".", "/") + ";");
    }
    
    public ProxyInfo setSuperClass(ClassInfo cl)
    {
        this.superClass = cl;
        return this;
    }
    
    public ProxyInfo setSuperClass(Class<?> cl)
    {
        superClass = bytecodeScanner.findOrCreateClassInfo(Type.getType(cl));
        return this;
    }
    
    public ProxyInfo addInterface(Class<?>... cl)
    {
        for (var interfaceClass : cl)
        {
            interfaces.add(bytecodeScanner.findOrCreateClassInfo(Type.getType(interfaceClass)));
        }
        
        return this;
    }
    
}
