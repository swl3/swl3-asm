package ru.swayfarer.swl3.asm.proxy;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.observable.event.AbstractCancelableEvent;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.commons.AdviceAdapter;

@Data @EqualsAndHashCode(callSuper = true)
@AllArgsConstructor @NoArgsConstructor
@Accessors(chain = true)
public class OverwriteEvent extends AbstractCancelableEvent{

    @NonNull
    public ProxyGenerator generator;
    
    @NonNull
    public ProxyInfo proxyInfo;
    
    @NonNull
    public AdviceAdapter methodVisitor;
    
    @NonNull
    public MethodInfo methodInfo;
    
    @NonNull
    public ClassVisitor classVisitor; 
    
    @NonNull
    public MethodInfo superMethodInfo;
    
    @NonNull
    public Map<String, Object> customProperties = new HashMap<>();
    
    public synchronized String reserveKey()
    {
        String str = null;
        
        while (!customProperties.containsKey((str = UUID.randomUUID() + "-" + UUID.randomUUID()))) 
        {
            customProperties.put(str, new Object());
        }
        
        return str;
    }
    
    public OverwriteEvent property(String str, Object value)
    {
        customProperties.put(str, value);
        return this;
    }
}
