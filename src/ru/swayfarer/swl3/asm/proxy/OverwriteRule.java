package ru.swayfarer.swl3.asm.proxy;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Data
@AllArgsConstructor @NoArgsConstructor
@Accessors(chain = true)
public class OverwriteRule {
    
    public boolean acceptCanceled = false;
    
    public IFunction1<OverwriteEvent, Boolean> filterFun = (event) -> true;
    
    public IFunction1NoR<OverwriteEvent> overwriteFun = (evt) -> {};
    
    public boolean isAccepts(OverwriteEvent event)
    {
        if (event.isCanceled() && !acceptCanceled)
            return false;
        
        return filterFun == null ? true : filterFun.apply(event);
    }
    
    public void apply(OverwriteEvent event)
    {
        if (overwriteFun != null)
            overwriteFun.apply(event);
    }
    
    public void process(OverwriteEvent event)
    {
        if (isAccepts(event))
            apply(event);
    }
}
