package ru.swayfarer.swl3.asm.proxy.builder;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.asm.proxy.ProxyGenerator;
import ru.swayfarer.swl3.asm.proxy.ProxyHelper;
import ru.swayfarer.swl3.asm.proxy.ProxyInfo;
import ru.swayfarer.swl3.asm.proxy.rules.reflection.ReflectionRuleProxyBuilder;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

import java.util.concurrent.atomic.AtomicReference;

@SuppressWarnings("unchecked")
public class ProxyBuilder<Super_Type> {

    public ReflectionsUtils reflectionsUtils;
    public ProxyHelper proxyHelper;
    public ProxyGenerator proxyGenerator;
    
    public String className;
    public Type superClass = AsmUtils.OBJECT_TYPE;
    public ExtendedList<Type> interfaces = new ExtendedList<>();
    public Super_Type wrappedObject;

    public IObservable<ProxyClassCreationEvent<Super_Type>> eventNewClass = Observables.createObservable()
            .exceptions().throwOnFail()
    ;

    public ProxyInfo proxyInfo;

    public ProxyBuilder(ProxyHelper proxyHelper)
    {
        this.proxyHelper = proxyHelper;
        this.proxyGenerator = proxyHelper.getProxyGenerator();
        this.className = proxyHelper.getRandomProxyClassName();
        this.reflectionsUtils = proxyHelper.getReflectionsUtils();
    }

    public ProxyBuilder<Super_Type> wrapByInterfaces(@NonNull Object obj)
    {
        parent(Object.class);
        interfaces(obj.getClass().getInterfaces());
        this.wrappedObject = ReflectionsUtils.cast(obj);
        return this;
    }

    public <T> ProxyBuilder<T> wrap(@NonNull T obj)
    {
        parent(obj.getClass());
        this.wrappedObject = ReflectionsUtils.cast(obj);
        return ReflectionsUtils.cast(this);
    }

    public <T> ProxyBuilder<T> parent(Type parent)
    {
        this.superClass = parent;
        return ReflectionsUtils.cast(this);
    }
    
    public <T> ProxyBuilder<T> parent(Class<T> parent)
    {
        return parent(Type.getType(parent));
    }
    
    public ProxyBuilder<Super_Type> interfaces(Class<?>... types) 
    {
        return interfaces(ExtendedStream.of(types)
                .map(Type::getType)
                .toArray(Type.class))
        ;
    }
    
    public ProxyBuilder<Super_Type> interfaces(Type... types) 
    {
        interfaces.addAll(types);
        return this;
    }

    public ReflectionRuleProxyBuilder<Super_Type> reflection()
    {
        if (!proxyHelper.reflectionEnabled.get())
            proxyHelper.enableReflection();

        var ret = new ReflectionRuleProxyBuilder<>(this);
        eventNewClass.subscribe().by(ret);

        return ret;
    }
    
    public ProxyInfo asProxyInfo()
    {
        var bytecodeScanner = proxyHelper.getBytecodeScanner();
        
        return new ProxyInfo()
                .setAsmUtils(proxyHelper.getAsmUtils())
                .setBytecodeScanner(proxyHelper.getBytecodeScanner())
                .setClassName(className)
                .setSuperClass(bytecodeScanner.findOrCreateClassInfo(superClass))
                .setInterfaces(interfaces.map(bytecodeScanner::findOrCreateClassInfo))
                .setWrappedObject(wrappedObject)
        ;
    }
    
    public <T extends Super_Type> T newInstance()
    {
        T proxyInstance = proxyHelper.getReflectionsUtils().constructors()
                .newInstance(createClass())
                .orHandle(proxyHelper.getExceptionsHandler(), "Error while creating proxy instance!")
        ;



        return proxyInstance;
    }
    
    public <T extends Super_Type> T newInstance(Object... args)
    {
        T proxyInstance = proxyHelper.getReflectionsUtils().constructors()
                .newInstance(createClass(), args)
                .orHandle(proxyHelper.getExceptionsHandler(), "Error while creating proxy instance!")
        ;

        return proxyInstance;
    }
    
    public ProxyBuilder<Super_Type> dump(FileSWL file) 
    {
        file
            .out()
                .writeBytes(proxyGenerator.generate(asProxyInfo()))
                .close()
        ;
        
        return this;
    }
    
    public Class<Super_Type> createClass()
    {
        proxyInfo = asProxyInfo();
        byte[] bytes = proxyGenerator.generate(proxyInfo);
        var retRef = new AtomicReference<Class<Super_Type>>();

        ExceptionsUtils.doOnExceptionAndThrow(() -> {
            Class<Super_Type> retClass = ReflectionsUtils.cast(proxyHelper.getReflectionsUtils().types().defineClass(className, bytes, proxyHelper.proxyProtectionDomainFun.apply(proxyInfo)));
            retRef.set(retClass);

            var event = ProxyClassCreationEvent.<Super_Type>builder()
                    .proxyClass(retClass)
                    .proxyInfo(proxyInfo)
                    .proxyBuilder(ReflectionsUtils.cast(this))
                    .build()
            ;

            eventNewClass.next(ReflectionsUtils.cast(event));

        }, (ex) -> {
            var logger = LogFactory.getLogger();
            var file = FileSWL.of("debug/swl3/proxy/dumps/" + proxyInfo.getClassName() + ".class");
            file.out().writeBytes(bytes).close();

            logger.error("Can't define proxy class", proxyInfo.getClassName(), "bytes dumped to", file.getAbsolutePath());
        });


        return retRef.get();
    }
    
//    public <T extends Super_Type> T create()
//    {
//        return (T) proxyGenerator.generate(asProxyInfo());
//    }
}
