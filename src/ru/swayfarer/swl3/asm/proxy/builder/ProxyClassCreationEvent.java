package ru.swayfarer.swl3.asm.proxy.builder;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.asm.proxy.ProxyInfo;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ProxyClassCreationEvent<Super_Type> {
    public ProxyBuilder<Super_Type> proxyBuilder;
    public Class<Super_Type> proxyClass;
    public ProxyInfo proxyInfo;
}
