package ru.swayfarer.swl3.asm.proxy;

import java.security.ProtectionDomain;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.proxy.builder.ProxyBuilder;
import ru.swayfarer.swl3.asm.proxy.rules.reflection.ReflectionOverrideRule;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@AllArgsConstructor @NoArgsConstructor
@Accessors(chain = true)
public class ProxyHelper {

    @NonNull
    public AsmUtils asmUtils = new AsmUtils();
    
    @NonNull
    public BytecodeScanner bytecodeScanner = new BytecodeScanner();
    
    @NonNull
    public ReflectionsUtils reflectionsUtils = ReflectionsUtils.getInstance();
    
    @NonNull
    public ProxyGenerator proxyGenerator = new ProxyGenerator();
    
    @NonNull
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    
    public IFunction1<ProxyInfo, ProtectionDomain> proxyProtectionDomainFun = (proxyInfo) -> reflectionsUtils.types().getDomain(null);
    
    public IFunction0<String> proxyClassNameFun = () -> ("SWL3$Proxy_" + UUID.randomUUID() + UUID.randomUUID()).replace("-", "");

    public AtomicBoolean reflectionEnabled = new AtomicBoolean();

    public ProxyHelper enableReflection()
    {
        reflectionEnabled.set(true);
        proxyGenerator.rules.add(new OverwriteRule(false, (e) -> true, new ReflectionOverrideRule()));
        return this;
    }
    
    public <T> ProxyBuilder<T> newProxy()
    {
        return new ProxyBuilder<>(this);
    }
    
    public String getRandomProxyClassName()
    {
        return proxyClassNameFun.apply();
    }
    
    public ProxyHelperConfigurator configure()
    {
        return new ProxyHelperConfigurator(this);
    }
    
    public static ProxyHelper defaultHelper()
    {
        return new ProxyHelper()
                .configure()
                    .enableReflection()
                .helper()
        ;
    }
    
}
