package ru.swayfarer.swl3.asm.proxy.rules.reflection;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.proxy.builder.ProxyBuilder;
import ru.swayfarer.swl3.asm.proxy.builder.ProxyClassCreationEvent;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Getter
@Setter
@Accessors(chain = true)
public class ReflectionRuleProxyBuilder<Super_Type> implements IFunction1NoR<ProxyClassCreationEvent<Super_Type>> {

    public ProxyBuilder<Super_Type> proxyBuilder;
    public IMethodListener methodListener = IMethodListener.EMPTY;
    public Object wrapperObject;

    public ReflectionRuleProxyBuilder(ProxyBuilder<Super_Type> proxyBuilder)
    {
        this.proxyBuilder = proxyBuilder;
    }

    public ReflectionRuleProxyBuilder<Super_Type> listener(IMethodListener methodListener)
    {

        if (methodListener == null)
            methodListener = IMethodListener.EMPTY;

        this.methodListener = methodListener;
        return this;
    }

    @Override
    public void applyNoRUnsafe(ProxyClassCreationEvent<Super_Type> event) throws Throwable {
        var attribs = OverrideAttributes.getAttributes(event.getProxyInfo());

        var proxyClass = event.getProxyClass();

        proxyBuilder.proxyHelper.getReflectionsUtils().fields().setValue(
                proxyClass,
                methodListener,
                attribs.getMethodListenerFieldName()
        );

        if (attribs.isWrapperFieldGenerated())
        {
            proxyBuilder.proxyHelper.getReflectionsUtils().fields().setValue(
                    proxyClass,
                    event.getProxyInfo().wrappedObject,
                    attribs.getWrappedObjectFieldName()
            );
        }
    }

    public ProxyBuilder<Super_Type> back()
    {
        return proxyBuilder;
    }
}
