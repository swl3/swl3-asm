package ru.swayfarer.swl3.asm.proxy.rules.reflection;

import java.lang.reflect.Method;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;

@Data
@Accessors(chain = true)
@NoArgsConstructor @AllArgsConstructor
public class MethodInvocationEvent {
    public Object instance;
    public Object returnValue;
    public Method method;

    public ISuperInvocationProvider superInvocationProvider;
    public ExtendedList<Object> args = new ExtendedList<>();

    public MethodInvocationEvent setArgs(Object... args)
    {
        this.args = CollectionsSWL.list(args);
        return this;
    }

    public Object returnSuper()
    {
        setReturnValue(invokeSuper());
        return getReturnValue();
    }

    public Object invokeSuper()
    {
        return invokeSuper(args.toArray(new Object[0]));
    }

    public Object invokeSuper(Object... args)
    {
        if (superInvocationProvider != null)
            return superInvocationProvider.apply(args);

        return null;
    }
}
