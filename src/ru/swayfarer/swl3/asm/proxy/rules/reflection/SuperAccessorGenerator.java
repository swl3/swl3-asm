package ru.swayfarer.swl3.asm.proxy.rules.reflection;

import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.AnnotationVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassWriter;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.FieldVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Label;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.RecordComponentVisitor;

public class SuperAccessorGenerator implements Opcodes {
    public static byte[] dump() throws Exception {

        ClassWriter classWriter = new ClassWriter(0);
        FieldVisitor fieldVisitor;
        RecordComponentVisitor recordComponentVisitor;
        MethodVisitor methodVisitor;
        AnnotationVisitor annotationVisitor0;

        classWriter.visit(V11, ACC_SUPER, "ru/swayfarer/swl3/asm/proxy/rules/reflection/Sample$1", null, "java/lang/Object", new String[]{"ru/swayfarer/swl3/asm/proxy/rules/reflection/ISuperInvocationProvider"});

        classWriter.visitSource("Sample.java", null);

        classWriter.visitNestHost("ru/swayfarer/swl3/asm/proxy/rules/reflection/Sample");

        classWriter.visitOuterClass("ru/swayfarer/swl3/asm/proxy/rules/reflection/Sample", "helloWorld", "(Ljava/lang/String;Ljava/lang/String;)V");

        classWriter.visitInnerClass("ru/swayfarer/swl3/asm/proxy/rules/reflection/Sample$1", null, null, 0);

        {
            fieldVisitor = classWriter.visitField(ACC_FINAL | ACC_SYNTHETIC, "this$0", "Lru/swayfarer/swl3/ru.swayfarer.swl3.asmv2.objectweb.asm.asm/proxy/rules/reflection/Sample;", null, null);
            fieldVisitor.visitEnd();
        }
        {
            methodVisitor = classWriter.visitMethod(0, "<init>", "(Lru/swayfarer/swl3/ru.swayfarer.swl3.asmv2.objectweb.asm.asm/proxy/rules/reflection/Sample;)V", null, null);
            methodVisitor.visitParameter("this$0", ACC_FINAL | ACC_MANDATED);
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(14, label0);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitFieldInsn(PUTFIELD, "ru/swayfarer/swl3/asm/proxy/rules/reflection/Sample$1", "this$0", "Lru/swayfarer/swl3/ru.swayfarer.swl3.asmv2.objectweb.asm.asm/proxy/rules/reflection/Sample;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
            methodVisitor.visitInsn(RETURN);
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLocalVariable("this", "Lru/swayfarer/swl3/ru.swayfarer.swl3.asmv2.objectweb.asm.asm/proxy/rules/reflection/Sample$1;", null, label0, label1, 0);
            methodVisitor.visitLocalVariable("this$0", "Lru/swayfarer/swl3/ru.swayfarer.swl3.asmv2.objectweb.asm.asm/proxy/rules/reflection/Sample;", null, label0, label1, 1);
            methodVisitor.visitMaxs(2, 2);
            methodVisitor.visitEnd();
        }
        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC, "applyUnsafe", "([Ljava/lang/Object;)Ljava/lang/Object;", null, new String[]{"java/lang/Throwable"});
            methodVisitor.visitParameter("objects", 0);
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(17, label0);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD, "ru/swayfarer/swl3/asm/proxy/rules/reflection/Sample$1", "this$0", "Lru/swayfarer/swl3/ru.swayfarer.swl3.asmv2.objectweb.asm.asm/proxy/rules/reflection/Sample;");
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitInsn(ICONST_0);
            methodVisitor.visitInsn(AALOAD);
            methodVisitor.visitTypeInsn(CHECKCAST, "java/lang/String");
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitInsn(ICONST_1);
            methodVisitor.visitInsn(AALOAD);
            methodVisitor.visitTypeInsn(CHECKCAST, "java/lang/String");
            methodVisitor.visitMethodInsn(INVOKESTATIC, "ru/swayfarer/swl3/asm/proxy/rules/reflection/Sample", "access$001", "(Lru/swayfarer/swl3/ru.swayfarer.swl3.asmv2.objectweb.asm.asm/proxy/rules/reflection/Sample;Ljava/lang/String;Ljava/lang/String;)V", false);
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLineNumber(18, label1);
            methodVisitor.visitInsn(ACONST_NULL);
            methodVisitor.visitInsn(ARETURN);
            Label label2 = new Label();
            methodVisitor.visitLabel(label2);
            methodVisitor.visitLocalVariable("this", "Lru/swayfarer/swl3/ru.swayfarer.swl3.asmv2.objectweb.asm.asm/proxy/rules/reflection/Sample$1;", null, label0, label2, 0);
            methodVisitor.visitLocalVariable("objects", "[Ljava/lang/Object;", null, label0, label2, 1);
            methodVisitor.visitMaxs(4, 2);
            methodVisitor.visitEnd();
        }
        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC | ACC_BRIDGE | ACC_SYNTHETIC, "applyUnsafe", "(Ljava/lang/Object;)Ljava/lang/Object;", null, new String[]{"java/lang/Throwable"});
            methodVisitor.visitParameter("objects", ACC_SYNTHETIC);
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(14, label0);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitTypeInsn(CHECKCAST, "[Ljava/lang/Object;");
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "ru/swayfarer/swl3/asm/proxy/rules/reflection/Sample$1", "applyUnsafe", "([Ljava/lang/Object;)Ljava/lang/Object;", false);
            methodVisitor.visitInsn(ARETURN);
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLocalVariable("this", "Lru/swayfarer/swl3/ru.swayfarer.swl3.asmv2.objectweb.asm.asm/proxy/rules/reflection/Sample$1;", null, label0, label1, 0);
            methodVisitor.visitMaxs(2, 2);
            methodVisitor.visitEnd();
        }
        classWriter.visitEnd();

        return classWriter.toByteArray();
    }
}
