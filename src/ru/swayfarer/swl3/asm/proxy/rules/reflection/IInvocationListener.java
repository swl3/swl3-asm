package ru.swayfarer.swl3.asm.proxy.rules.reflection;

import java.lang.reflect.Method;

public interface IInvocationListener {

    public Object invokeByReflection(Object instance, Method method, Object... args) throws Throwable;

    public static interface Void extends IInvocationListener {
        
        void invokeByReflectionNoR(Object instance, Method method, Object... args) throws Throwable;
        
        @Override
        default Object invokeByReflection(Object instance, Method method, Object... args) throws Throwable
        {
            invokeByReflectionNoR(instance, method, args);
            return null;
        }
    }
    
}
