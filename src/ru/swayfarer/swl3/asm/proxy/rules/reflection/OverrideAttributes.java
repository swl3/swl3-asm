package ru.swayfarer.swl3.asm.proxy.rules.reflection;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.proxy.OverwriteEvent;
import ru.swayfarer.swl3.asm.proxy.ProxyInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
public class OverrideAttributes {

    public static String reflectionHandlerPropertiesKey = UUID.randomUUID().toString();

    public String methodListenerFieldName;
    public String wrappedObjectFieldName;

    public IMethodListener methodListener;
    public boolean listenerFieldGenerated;
    public boolean wrapperFieldGenerated;
    public boolean wrapperMode;

    public Map<String, String> cachedMethods = new HashMap<>();

    public static OverrideAttributes getAttributes(OverwriteEvent evt)
    {
        return getAttributes(evt.getProxyInfo());
    }

    public static OverrideAttributes getAttributes(ProxyInfo proxyInfo)
    {
        var key =reflectionHandlerPropertiesKey;
        var customProperties = proxyInfo.getCustomData();
        var attr = (OverrideAttributes) customProperties.get(key);

        if (attr == null)
        {
            attr = new OverrideAttributes();
            customProperties.put(key, attr);
        }

        return attr;
    }
}
