package ru.swayfarer.swl3.asm.proxy.rules.reflection;

import java.lang.reflect.Method;
import java.util.UUID;

import lombok.var;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.proxy.OverwriteEvent;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Handle;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Label;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

public class ReflectionOverrideRule implements IFunction1NoR<OverwriteEvent>, Opcodes {

    public static String helloWorld;

    public Type methodInvocationEventType = Type.getType("Lru/swayfarer/swl3/ru.swayfarer.swl3.asmv2.objectweb.asm.asm/proxy/rules/reflection/MethodInvocationEvent;");
    public Type methodType = Type.getType(Method.class);
    public Type invocationHandlerType = Type.getType(IMethodListener.class);

    public String invocationHandlerFieldName;
    public String proxyWrapperFieldName;

    public String invocationHandlerMethodName = "applyUnsafe";

    @Override
    public void applyNoRUnsafe(OverwriteEvent evt)
    {
        var proxyInfo = evt.getProxyInfo();
        var cv = evt.getClassVisitor();
        var mv = evt.getMethodVisitor();
        var method = evt.getMethodInfo();
        var methodReturnType = method.getReturnType();
        var methodFieldName = getRandomElementName();

        genListenerField(evt);

        var attributes = OverrideAttributes.getAttributes(evt);

        attributes
            .setMethodListenerFieldName(invocationHandlerFieldName)
            .setWrappedObjectFieldName(proxyWrapperFieldName)
        ;

        var asmUtils = evt.getProxyInfo().getAsmUtils();
        
        generateFieldAndAccessor(proxyInfo.getProxyClassType(), methodFieldName, cv, method, asmUtils);
        var eventLocalIndex= genFillAndSendEvent(evt, methodFieldName);

        if (!methodReturnType.equals(Type.VOID_TYPE))
        {
            mv.visitVarInsn(ALOAD, eventLocalIndex);
            mv.visitMethodInsn(INVOKEVIRTUAL, methodInvocationEventType.getInternalName(), "getReturnValue", "()Ljava/lang/Object;", false);
            asmUtils.types().convesion().convert(AsmUtils.OBJECT_TYPE, methodReturnType, mv);
        }

        mv.visitInsn(methodReturnType.getOpcode(IRETURN));
    }

    public boolean isAccepting(OverwriteEvent event)
    {
        return !event.getMethodInfo().getName().equals("<init>");
    }

    public int genFillAndSendEvent(OverwriteEvent event, String methodFieldName)
    {
        var asmUtils = event.getProxyInfo().getAsmUtils();
        var typesUtils = asmUtils.types();
        var method = event.getMethodInfo();
        var proxyInfo = event.getProxyInfo();

        var ownerName = proxyInfo.getProxyClassType().getInternalName();
        var fieldDescriptor = methodType.getDescriptor();

        var cv = event.getClassVisitor();
        var mv = event.getMethodVisitor();

        var eventLocalVarIndex = mv.newLocal(Type.getType(MethodInvocationEvent.class));
        var superStaticAccessorName = "access$" + getRandomElementName();
        var superAccessorLambdaName = "lambda$" + getRandomElementName();
        var methodParams = method.getParams();
        var methodReturnType = method.getReturnType();
        var isVoid = methodReturnType.equals(Type.VOID_TYPE);

        var isSuperSupported = !method.getAccess().isAbstract();
        var attributes = OverrideAttributes.getAttributes(event);
        var isWrapperMode = proxyInfo.getWrappedObject() != null;
        var supertype = proxyInfo.getSuperClass().getClassType();

        var buffer = new DynamicString();
        buffer.append("(L");
        buffer.append(ownerName);
        buffer.append(";");

        for (var param : methodParams)
        {
            buffer.append(param.getParamType().getDescriptor());
        }

        buffer.append(")");
        buffer.append(method.getReturnType().getDescriptor());

        var superStaticAccessorLambdaDesc = buffer.toString();

        buffer.clear();

        var lambdaDesc = "([Ljava/lang/Object;)Ljava/lang/Object;";

        if (isSuperSupported)
        {
            // Содзаем лямбду
            {
                var superAccMv = cv.visitMethod(ACC_PRIVATE | ACC_SYNTHETIC, superAccessorLambdaName, lambdaDesc, null, new String[]{"java/lang/Throwable"});
                superAccMv.visitCode();

                // Загружаем this для аксессора

                superAccMv.visitVarInsn(ALOAD, 0);

                var nextParamArrayIndex = 0;

                // Загружаем массив параметров в слоты для нормальных параметров супера
                for (var param : methodParams)
                {
                    superAccMv.visitVarInsn(ALOAD, 1);
                    superAccMv.visitLdcInsn(nextParamArrayIndex ++);
                    superAccMv.visitInsn(AALOAD);
                    asmUtils.types().convesion().convert(AsmUtils.OBJECT_TYPE, param.getParamType(), superAccMv);
                }

                superAccMv.visitMethodInsn(INVOKESTATIC, ownerName, superStaticAccessorName, superStaticAccessorLambdaDesc, false);

                if (isVoid)
                {
                    superAccMv.visitInsn(Opcodes.ACONST_NULL);
                    superAccMv.visitInsn(Opcodes.ARETURN);
                }
                else
                {
                    asmUtils.types().convesion().convert(methodReturnType, AsmUtils.OBJECT_TYPE, superAccMv);
                    superAccMv.visitInsn(Opcodes.ARETURN);
                }

                superAccMv.visitMaxs(0, 0);
                superAccMv.visitEnd();
            }

            // Генерируем поле обертки, если нужно
            {
                if (isWrapperMode && !attributes.isWrapperFieldGenerated())
                {
                    cv.visitField(ACC_PUBLIC | Opcodes.ACC_STATIC, proxyWrapperFieldName, supertype.getDescriptor(), null, null)
                            .visitEnd()
                    ;

                    attributes.setWrapperFieldGenerated(true);
                }
            }

            // Создание аксессора для лямбды
            {
                var methodVisitor = cv.visitMethod(ACC_STATIC | ACC_SYNTHETIC, superStaticAccessorName, superStaticAccessorLambdaDesc, null, null);
                methodVisitor.visitCode();

                if (isWrapperMode)
                {
                    methodVisitor.visitFieldInsn(Opcodes.GETSTATIC, ownerName, proxyWrapperFieldName, supertype.getDescriptor());
                }
                else
                {
                    methodVisitor.visitVarInsn(ALOAD, 0);
                }

                for (var param : methodParams)
                {
                    methodVisitor.visitVarInsn(param.getParamType().getOpcode(ILOAD), param.getIndex());
                }

                var opcode = isWrapperMode ? INVOKEVIRTUAL : INVOKESPECIAL;

                methodVisitor.visitMethodInsn(opcode, proxyInfo.getSuperClass().getClassType().getInternalName(), method.getName(), method.getDescriptor(), false);
                methodVisitor.visitInsn(methodReturnType.getOpcode(IRETURN));
                methodVisitor.visitMaxs(0, 0);
                methodVisitor.visitEnd();
            }
        }

        // Создание нового инстанса события
        {
            mv.visitTypeInsn(NEW, methodInvocationEventType.getInternalName());
            mv.visitInsn(DUP);
            mv.visitMethodInsn(INVOKESPECIAL, methodInvocationEventType.getInternalName(), "<init>", "()V", false);
        }

        // Загрузка аргументов
        {
            mv.visitLdcInsn(methodParams.size());
            mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");

            int nextIndex = 0;
            int nextParamIndex = method.getAccess().isStatic() ? 0 : 1;

            for (var param : methodParams)
            {
                mv.visitInsn(DUP);
                mv.visitLdcInsn(nextIndex ++);
                var paramType = param.getParamType();
                mv.visitVarInsn(paramType.getOpcode(ILOAD), nextParamIndex);
                nextParamIndex += paramType.getSize();
                if (typesUtils.isPrimitive(paramType))
                    typesUtils.convesion().box(paramType, mv);

                mv.visitInsn(AASTORE);
            }

            mv.visitMethodInsn(INVOKEVIRTUAL, methodInvocationEventType.getInternalName(), "setArgs", "([Ljava/lang/Object;)" + methodInvocationEventType.getDescriptor(), false);
        }

        // Задание метода
        {
            mv.visitMethodInsn(INVOKESTATIC, ownerName, methodFieldName, "()"+fieldDescriptor, false);
            mv.visitMethodInsn(INVOKEVIRTUAL, methodInvocationEventType.getInternalName(), "setMethod", "(Ljava/lang/reflect/Method;)" + methodInvocationEventType.getDescriptor(), false);
        }

        if (isSuperSupported)
        {
            // Создание провайдера для вызова супера
            {
                mv.loadThis();
                mv.visitInvokeDynamicInsn(invocationHandlerMethodName, "(L" + ownerName + ";)Lru/swayfarer/swl3/ru.swayfarer.swl3.asmv2.objectweb.asm.asm/proxy/rules/reflection/ISuperInvocationProvider;", new Handle(Opcodes.H_INVOKESTATIC, "java/lang/invoke/LambdaMetafactory", "metafactory", "(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/String;Ljava/lang/invoke/MethodType;Ljava/lang/invoke/MethodType;Ljava/lang/invoke/MethodHandle;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/CallSite;", false), new Object[]{Type.getType("(Ljava/lang/Object;)Ljava/lang/Object;"), new Handle(Opcodes.H_INVOKESPECIAL, ownerName, superAccessorLambdaName, lambdaDesc, false), Type.getType(lambdaDesc)});
                mv.visitMethodInsn(INVOKEVIRTUAL, methodInvocationEventType.getInternalName(), "setSuperInvocationProvider", "(Lru/swayfarer/swl3/ru.swayfarer.swl3.asmv2.objectweb.asm.asm/proxy/rules/reflection/ISuperInvocationProvider;)Lru/swayfarer/swl3/ru.swayfarer.swl3.asmv2.objectweb.asm.asm/proxy/rules/reflection/MethodInvocationEvent;", false);
            }
        }

        // Задаем инстанс
        {
            mv.loadThis();
            mv.visitMethodInsn(INVOKEVIRTUAL, methodInvocationEventType.getInternalName(), "setInstance", "(Ljava/lang/Object;)Lru/swayfarer/swl3/ru.swayfarer.swl3.asmv2.objectweb.asm.asm/proxy/rules/reflection/MethodInvocationEvent;", false);
        }

        // Сохраняем в переменную событие
        mv.visitVarInsn(ASTORE, eventLocalVarIndex);

        // Вызываем у листенера обработку события
        mv.visitFieldInsn(Opcodes.GETSTATIC, ownerName, invocationHandlerFieldName, invocationHandlerType.getDescriptor());

        mv.visitVarInsn(ALOAD, eventLocalVarIndex);
        mv.visitMethodInsn(INVOKEINTERFACE, "ru/swayfarer/swl3/asm/proxy/rules/reflection/IMethodListener", "apply", "(Ljava/lang/Object;)Ljava/lang/Object;", true);
        mv.visitInsn(POP);

        return eventLocalVarIndex;
//        mv.visitInsn(RETURN);
    }

    public void generateFieldAndAccessor(Type proxyClassType, String fieldName, ClassVisitor cv, MethodInfo targetMethod, AsmUtils asmUtils)
    {
        var proxyClassInternalName = proxyClassType.getInternalName();
        
        cv.visitField(ACC_PUBLIC | ACC_STATIC, fieldName, methodType.getDescriptor(), null, null)
            .visitEnd()
        ;

        var mv = cv.visitMethod(ACC_PUBLIC | ACC_STATIC, fieldName, "()Ljava/lang/reflect/Method;", null, new String[] { "java/lang/Throwable" });
        
        mv.visitCode();

        mv.visitFieldInsn(GETSTATIC, proxyClassInternalName, fieldName, "Ljava/lang/reflect/Method;");
        Label label1 = new Label();
        mv.visitJumpInsn(IFNONNULL, label1);
        
        mv.visitLdcInsn(targetMethod.getOwner().getClassType());
        mv.visitLdcInsn(targetMethod.getName());
        
        var params = targetMethod.getParams();
        mv.visitLdcInsn(params.size());
        mv.visitTypeInsn(ANEWARRAY, "java/lang/Class");
        int nextIndex = 0;
        
        for (var param : params)
        {
            mv.visitInsn(DUP);
            mv.visitLdcInsn(nextIndex ++);
            asmUtils.types().loadAsClass(param.getParamType(), mv);
            mv.visitInsn(AASTORE);
        }
        
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getDeclaredMethod", "(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;", false);
        mv.visitFieldInsn(PUTSTATIC, proxyClassInternalName, fieldName, "Ljava/lang/reflect/Method;");
        mv.visitFieldInsn(GETSTATIC, proxyClassInternalName, fieldName, "Ljava/lang/reflect/Method;");
        mv.visitInsn(ICONST_1);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/reflect/Method", "setAccessible", "(Z)V", false);
        mv.visitLabel(label1);
        mv.visitFieldInsn(GETSTATIC, proxyClassInternalName, fieldName, "Ljava/lang/reflect/Method;");
        mv.visitInsn(ARETURN);

        mv.visitMaxs(0, 0);
        mv.visitEnd();
    }

    public String getRandomElementName()
    {
        return ("__$swlProxy$" + UUID.randomUUID().toString() + UUID.randomUUID()).replace("-", "");
    }

    public void genListenerField(OverwriteEvent event)
    {
        var attribs = OverrideAttributes.getAttributes(event);

        if (!attribs.isListenerFieldGenerated())
        {
            proxyWrapperFieldName = "___$proxyWrapper" + getRandomElementName();
            invocationHandlerFieldName = "___$methodListener" + getRandomElementName();

            var cv = event.getClassVisitor();
            var proxyInfo = event.getProxyInfo();

            cv.visit(
                    proxyInfo.superClass.getVersion(),
                    ACC_PUBLIC | ACC_SUPER,
                    proxyInfo.className.replace('.', '/'),
                    null,
                    proxyInfo.superClass.getClassType().getInternalName(),
                    proxyInfo.interfaces.map((info) -> info.getClassType().getInternalName()).toArray(String.class)
            );

            cv.visitField(ACC_PUBLIC | Opcodes.ACC_STATIC, invocationHandlerFieldName, invocationHandlerType.getDescriptor(), null, null)
                    .visitEnd();
            ;

            attribs.setListenerFieldGenerated(true);
        }
    }
}
