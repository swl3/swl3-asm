package ru.swayfarer.swl3.asm.proxy.rules.reflection;

import ru.swayfarer.swl3.funs.GeneratedFuns;

public interface ISuperInvocationProvider extends GeneratedFuns.IFunction1<Object[], Object> {
}
