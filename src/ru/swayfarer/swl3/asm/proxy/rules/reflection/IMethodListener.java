package ru.swayfarer.swl3.asm.proxy.rules.reflection;

import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns;

public interface IMethodListener extends GeneratedFuns.IFunction1NoR<MethodInvocationEvent> {
    public static IMethodListener EMPTY = (e) -> {
        var superMethod = e.getSuperInvocationProvider();

        if (superMethod != null)
        {
            superMethod.apply(e.getArgs().toArray(new Object[0]));
        }
    };
}
