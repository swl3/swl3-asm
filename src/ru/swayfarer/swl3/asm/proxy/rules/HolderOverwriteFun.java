package ru.swayfarer.swl3.asm.proxy.rules;

import java.util.HashMap;
import java.util.Map;

import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.info.VariableInfo;
import ru.swayfarer.swl3.asm.info.stream.MethodInfoStream;
import ru.swayfarer.swl3.asm.proxy.OverwriteEvent;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

public class HolderOverwriteFun implements IFunction1NoR<OverwriteEvent> {

    public ExtendedList<ClassInfo> holders = CollectionsSWL.list();

    @Override
    public void applyNoRUnsafe(OverwriteEvent event)
    {
        var method = event.getMethodInfo();
        
        if (canOverrideMethod(method))
        {
            var mv = event.getMethodVisitor();
            event.setCanceled(true);
        }
    }
    
    public boolean canOverrideMethod(MethodInfo target)
    {
        var access = target.getAccess();
        
        if (access.isPublic() || access.isProtected() && !access.isFinal())
        {
            return true;
        }
        
        return false;
    }
    
    public static class Holder {
        public Map<String, ExtendedList<MethodInfo>> methodsByNames = new HashMap<>();
        public ClassInfo classInfo;
        
        public Holder(ClassInfo classInfo)
        {
            classInfo.methodsStream()
                .each(this::addMethod);
        }
        
        public MethodInfo findHolderMethod(MethodInfo target)
        {
            var methodName = target.getName();
            var methods = methodsByNames.get(methodName);
            
            var methodsStream = new MethodInfoStream(classInfo.getBytecodeScanner(), () -> methods.stream());
            
            var paramsTypes = target.getParams()
                    .map(VariableInfo::getParamType)
                    .toArray(Type.class)
            ;
            
            var returnType = target.getReturnType();
            
            return methodsStream.withParams()
                    .types(paramsTypes)
                    .returns().is(returnType)
                    .findAny().orNull()
            ;
        }
        
        public void addMethod(MethodInfo methodInfo)
        {
            getOrCreateList(methodInfo.getName()).add(methodInfo);
        }
        
        public ExtendedList<MethodInfo> getOrCreateList(String name)
        {
            var ret = methodsByNames.get(name);
            
            if (ret == null)
            {
                ret = CollectionsSWL.list();
                methodsByNames.put(name, ret);
            }
            
            return ret;
        }
    }
}
