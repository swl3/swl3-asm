package ru.swayfarer.swl3.asm.proxy.rules;

import lombok.var;
import ru.swayfarer.swl3.asm.proxy.OverwriteEvent;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;

public interface StandartOverwriteFuns extends Opcodes {

    public IFunction1NoR<OverwriteEvent> defaultValues = (evt) -> {
        var proxyInfo = evt.getProxyInfo();
        var mv = evt.getMethodVisitor();
        var retType = mv.getReturnType();
        
        proxyInfo.asmUtils.types().putDefaultType(retType, mv);
        evt.getMethodVisitor().visitInsn(retType.getOpcode(IRETURN));
    };
    
}
