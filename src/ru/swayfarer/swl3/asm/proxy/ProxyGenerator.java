package ru.swayfarer.swl3.asm.proxy;

import java.lang.reflect.Modifier;

import lombok.var;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.proxy.rules.reflection.IInvocationListener;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.objects.EqualsUtils;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassWriter;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.commons.AdviceAdapter;

public class ProxyGenerator implements Opcodes {

    public ExtendedList<OverwriteRule> rules = CollectionsSWL.list();
    
    public byte[] generate(ProxyInfo proxyInfo)
    {
        var cv = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        
        var constructors = proxyInfo.superClass.methodsStream()
            .name().is("<init>")
            .toExList();
        
        for (var constructor : constructors)
        {
            var mv = createMethod(constructor, cv);
            mv.visitCode();

            // this
            mv.visitVarInsn(ALOAD, 0);
            
            var params = constructor.getParams();
            
            // Параметры
            for (var param : params)
            {
                mv.visitVarInsn(param.getParamType().getOpcode(ILOAD), param.getIndex());
            }
            
            proxyInfo.asmUtils.methods().invokeSpecial(mv, constructor);
            mv.visitInsn(RETURN);

            mv.visitMaxs(0, mv.nextLocal);
            mv.visitEnd();
        }
        
        var abstracts = proxyInfo.superClass.methodsStream()
                .toExList();
        
        for (var method : abstracts)
        {
            if (!EqualsUtils.objectEqualsSome(method.getName(), "<init>", "<clinit>"))
                generateMethod(method, cv, proxyInfo);
        }
        
        for (var interfaceClass : proxyInfo.getInterfaces())
        {
            for (var method : interfaceClass.getMethods())
            {
                generateMethod(method, cv, proxyInfo);
            }
        }

        var bytes = cv.toByteArray();

        FileSWL.of("helloWorld.class").out().writeBytes(bytes).close();

        return bytes;
    }
    
    public void generateMethod(MethodInfo method, ClassVisitor cv, ProxyInfo proxyInfo)
    {
        var mv = createMethod(method, cv);
        
        mv.visitCode();
        
        var event = new OverwriteEvent()
                .setGenerator(this)
                .setProxyInfo(proxyInfo)
                .setClassVisitor(cv)
                .setMethodVisitor(mv)
                .setMethodInfo(method)
                .setSuperMethodInfo(method)
        ;
        
        for (var rule : rules)
        {
            rule.process(event);
        }

        mv.visitMaxs(0, mv.nextLocal);
        mv.visitEnd();
    }
    
    public void createMethodParamsAnnotations(MethodInfo methodInfo, MethodVisitor mv)
    {
        var params = methodInfo.getParams();
        
        for (var param : params)
        {
            var annotations = param.getAnnotationsInfo().getAnnotations();
            
            for (var annotation : annotations)
            {
                mv.visitParameterAnnotation(param.getIndex(), annotation.getAnnotationType().getDescriptor(), true);
            }
        }
    }
    
    public void createMethodAnnotations(MethodInfo methodInfo, MethodVisitor mv)
    {
        createMethodParamsAnnotations(methodInfo, mv);
        
        for (var annotation : methodInfo.getAnnotationsInfo().getAnnotations())
        {
            if (!annotation.getAnnotationType().getClassName().startsWith("jdk.internal"))
            {
                mv.visitAnnotation(annotation.getAnnotationType().getDescriptor(), true);
            }
        }
    }
    
    public AdviceAdapter createMethod(MethodInfo methodInfo, ClassVisitor cv)
    {
        var modifier = methodInfo.getAccess().getModifier();
        
        if (Modifier.isAbstract(modifier))
        {
            modifier = modifier ^ Opcodes.ACC_ABSTRACT;
        }
        
        var methodName = methodInfo.getName();
        var methodDescriptor = methodInfo.getDescriptor();
        var mv = cv.visitMethod(modifier, methodName, methodDescriptor, null, null);
        createMethodAnnotations(methodInfo, mv);
        
        return new AdviceAdapter(ASM8_EXPERIMENTAL, mv, ACC_PUBLIC, methodName, methodDescriptor) {};
    }
}
