package ru.swayfarer.swl3.asm.proxy;

import java.util.concurrent.atomic.AtomicBoolean;

import lombok.NonNull;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.collections.stream.AbstractFiltering;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

public class MethodInfoFiltering<Ret_Type> extends AbstractFiltering<MethodInfo, MethodInfoFiltering<MethodInfo>, Ret_Type>{

    public AtomicBoolean isAccepted = new AtomicBoolean();
    
    public MethodInfoFiltering(@NonNull IFunction1<IFunction1<MethodInfo, Boolean>, Ret_Type> filterFun)
    {
        super(filterFun);
    }
}
