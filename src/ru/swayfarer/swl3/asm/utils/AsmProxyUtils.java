package ru.swayfarer.swl3.asm.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.proxy.ProxyHelper;
import ru.swayfarer.swl3.asm.proxy.rules.reflection.IMethodListener;
import ru.swayfarer.swl3.asm.proxy.rules.reflection.MethodInvocationEvent;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.NoSuchElementException;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class AsmProxyUtils
{
    public AsmUtils asmUtils;
    public ReflectionsUtils reflectionsUtils = ReflectionsUtils.getInstance();

    public ProxyHelper newProxyHelper()
    {
        return new ProxyHelper()
                .setAsmUtils(asmUtils)
        ;
    }

    public Class<?> createCompoundProxyClass(
            Class<?> parentClass,
            CompoundProxyPair... pairs
    )
    {
        var invocationHandler = new CompoundInvocationHandler();

        ExtendedStream.of(pairs)
                .each((pair) -> {
                    var partType = pair.getAssociatedType();

                    reflectionsUtils.methods().stream(partType)
                            .each((method) -> invocationHandler.methodToPartSource.put(method, pair.getPartValueFun()))
                    ;
                })
        ;

        var proxyHelper = new ProxyHelper();

        var proxyClass = proxyHelper.newProxy()
                .reflection().listener(invocationHandler).back()
                .interfaces(ExtendedStream.of(pairs).map(CompoundProxyPair::getAssociatedType).toArray(Class.class))
                .parent(parentClass)
                .createClass()
        ;

        return proxyClass;
    }

    public <Compound_Proxy> Compound_Proxy createCompoundProxy(
            Class<?> parentClass,
            CompoundProxyPair... pairs
    )
    {
        return reflectionsUtils.constructors().newInstance(createCompoundProxyClass(parentClass, pairs)).orThrow();
    }

    public static CompoundProxyPair compound(Class<?> type, IFunction0<?> valueFun)
    {
        return new CompoundProxyPair(type, ReflectionsUtils.cast(valueFun));
    }

    @Internal
    @Getter
    @Setter
    @Accessors(chain = true)
    public static class CompoundProxyPair {
        @Internal
        public Class<?> associatedType;

        @Internal
        public IFunction0<Object> partValueFun;

        public CompoundProxyPair(Class<?> associatedType, IFunction0<Object> partValueFun)
        {
            this.associatedType = associatedType;
            this.partValueFun = partValueFun.memorized();
        }
    }

    @Internal
    public static class CompoundInvocationHandler implements IMethodListener
    {
        public ExtendedMap<Method, IFunction0<Object>> methodToPartSource = new ExtendedMap<>().synchronize();
        public ExtendedMap<Method, Object> methodToPart = new ExtendedMap<>().synchronize();

        @Override
        public void applyNoRUnsafe(MethodInvocationEvent methodInvocationEvent) throws Throwable
        {
            var args = methodInvocationEvent.getArgs();
            var method = methodInvocationEvent.getMethod();

            if (Modifier.isAbstract(method.getModifiers()))
            {
                var sourceFun = methodToPartSource.get(method);

                ExceptionsUtils.IfNull(sourceFun, NoSuchElementException.class, "Can't find source for method", method);

                var result = method.invoke(sourceFun.apply(), args.toArray());
                methodInvocationEvent.setReturnValue(result);
            }
            else
            {
                methodInvocationEvent.setReturnValue(methodInvocationEvent.invokeSuper());
            }
        }
    }
}
