package ru.swayfarer.swl3.asm.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Getter
@Setter
@Accessors(chain = true)
public class AsmTypesUtils implements Opcodes {
	
	@NonNull
	public AsmUtils asmUtils;
	
	@NonNull
	public AsmTypesBoxingUtils boxingUtils;

	public ExtendedList<Type> iconstTypes = CollectionsSWL.list(
	        Type.BYTE_TYPE,
            Type.INT_TYPE,
            Type.SHORT_TYPE,
            Type.BOOLEAN_TYPE,
            Type.CHAR_TYPE
    );
	
	public ExtendedList<Type> numberTypes = CollectionsSWL.list(
            Type.BYTE_TYPE,
            Type.DOUBLE_TYPE,
            Type.LONG_TYPE,
            Type.FLOAT_TYPE,
            Type.INT_TYPE,
            Type.SHORT_TYPE
    );
	
	public ExtendedList<Type> primitiveTypes = CollectionsSWL.list(
			Type.BOOLEAN_TYPE,
			Type.CHAR_TYPE
	);
	
	public AsmTypesUtils(AsmUtils asmUtils)
	{
		this.boxingUtils = new AsmTypesBoxingUtils(asmUtils);
		primitiveTypes.addAll(numberTypes);
	}
	
	public AsmTypesUtils putDefaultType(Type type, MethodVisitor mv)
	{
	    if (iconstTypes.contains(type))
	    {
	        mv.visitInsn(ICONST_0);
	    }
	    else if (type.equals(Type.DOUBLE_TYPE))
	    {
	        mv.visitInsn(DCONST_0);
	    }
        else if (type.equals(Type.FLOAT_TYPE))
        {
            mv.visitInsn(FCONST_0);
        }
        else if (type.equals(Type.LONG_TYPE))
        {
            mv.visitInsn(LCONST_0);
        }
        else
        {
            mv.visitInsn(ACONST_NULL);
        }
	    
	    return this;
	}
	
	public AsmTypesUtils loadAsClass(Type type, MethodVisitor mv)
	{
	    if (isPrimitive(type)) 
	    {
	        var boxedType = boxingUtils.getBoxedType(type);
	        mv.visitFieldInsn(GETSTATIC, boxedType.getInternalName(), "TYPE", "Ljava/lang/Class;");
	    }
	    else
	    {
	        mv.visitLdcInsn(type);
	    }
	    
	    return this;
	}
	
	public boolean isVoid(Type type)
	{
		return type == Type.VOID_TYPE;
	}
	
	public boolean isPrimitive(Type type)
	{
		return primitiveTypes.contains(type);
	}
	
	public AsmTypesBoxingUtils convesion()
	{
		return boxingUtils;
	}
}
