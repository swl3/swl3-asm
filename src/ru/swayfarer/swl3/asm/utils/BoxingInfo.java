package ru.swayfarer.swl3.asm.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Label;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true) @AllArgsConstructor(staticName = "box")
public class BoxingInfo {
	
	@NonNull
	public Type unboxed, boxed;
	
	public void box(MethodVisitor mv)
	{
		mv.visitMethodInsn (
			Opcodes.INVOKESTATIC, 
			boxed.getInternalName(), 
			"valueOf", 
			"(" + unboxed.getDescriptor() + ")" + boxed.getDescriptor(), 
			false
		);
	}
	
	public void unbox(MethodVisitor mv)
	{
		if (mv == null)
			throw new NullPointerException("Can't unwrap boolean from null!");

		mv.visitInsn(Opcodes.DUP);

		var lblIfNotNull = new Label();
		mv.visitJumpInsn(Opcodes.IFNONNULL, lblIfNotNull);
		mv.visitTypeInsn(Opcodes.NEW, "java/lang/NullPointerException");
		mv.visitInsn(Opcodes.DUP);
		mv.visitLdcInsn("Can't unwrap " + unboxed.getClassName() + " from null!");
		mv.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/NullPointerException", "<init>", "(Ljava/lang/String;)V", false);
		mv.visitInsn(Opcodes.ATHROW);
		mv.visitLabel(lblIfNotNull);

		mv.visitMethodInsn (
			Opcodes.INVOKEVIRTUAL, 
			boxed.getInternalName(), 
			unboxed.getClassName() + "Value", 
			"()" + unboxed.getDescriptor(), 
			false
		);
	}
}