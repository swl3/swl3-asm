package ru.swayfarer.swl3.asm.utils;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true)
public class AsmTypesBoxingUtils
{
	@NonNull
	public AsmUtils asmUtils;
	
	public ExtendedList<BoxingInfo> boxTypes = CollectionsSWL.list();
	
	public AsmTypesBoxingUtils(AsmUtils asmUtils)
	{
		registerDefaultTypes();
		this.asmUtils = asmUtils;
	}
	
	public AsmTypesBoxingUtils registerDefaultTypes()
	{
		registerBoxType(Type.BOOLEAN_TYPE, AsmUtils.BOOLEAN_TYPE);
		registerBoxType(Type.CHAR_TYPE, AsmUtils.CHARACTER_TYPE);
		
		registerBoxType(Type.BYTE_TYPE, AsmUtils.BYTE_TYPE);
		registerBoxType(Type.SHORT_TYPE, AsmUtils.SHORT_TYPE);
		registerBoxType(Type.INT_TYPE, AsmUtils.INTEGER_TYPE);
		registerBoxType(Type.LONG_TYPE, AsmUtils.LONG_TYPE);
		
		registerBoxType(Type.FLOAT_TYPE, AsmUtils.FLOAT_TYPE);
		registerBoxType(Type.DOUBLE_TYPE, AsmUtils.DOUBLE_TYPE);
		
		return this;
	}
	
	public AsmTypesBoxingUtils registerBoxType(Type unboxed, Type boxed)
	{
		var info = BoxingInfo.box(unboxed, boxed);
		boxTypes.add(info);
		return this;
	}
	
	public Type getUnboxedType(Type boxedType)
	{
		var type = boxTypes.exStream()
			.findFirst((box) -> box.getBoxed().equals(boxedType))
			.orNull();
		
		return type == null ? boxedType : type.getUnboxed();
	}
	
	public Type getBoxedType(Type unboxedType)
	{
		var type = boxTypes.exStream()
			.findFirst((box) -> box.getUnboxed().equals(unboxedType))
			.orNull();
		
		return type == null ? unboxedType : type.getBoxed();
	}
	
	public AsmTypesBoxingUtils unbox(Type type, MethodVisitor mv)
	{
		boxTypes.exStream()
				.findFirst((box) -> box.getBoxed().equals(type))
				.ifPresent((box) -> {
					box.unbox(mv);
				})
		;
		
		return this;
	}
	
	public AsmTypesBoxingUtils checkCast(Type type, MethodVisitor mv)
	{
		mv.visitTypeInsn(Opcodes.CHECKCAST, type.getInternalName());
		return this;
	}
	
	public AsmTypesBoxingUtils convert(Type type, Type destType, MethodVisitor mv)
	{
		if (type.equals(destType))
			return this;
		
		var typesUtils = asmUtils.types();
		
		// Если обрабатываем Object, то тип изначального объекта возьмется из целевого
		if (AsmUtils.OBJECT_TYPE.equals(type))
		{
			var newType = getBoxedType(destType);
			
			if (!type.equals(newType))
			{
				checkCast(newType, mv);
				type = newType;
			}
		}
		
		if (typesUtils.isPrimitive(destType) == typesUtils.isPrimitive(type))
		{
			if (destType.equals(Type.BOOLEAN_TYPE) || type.equals(Type.BOOLEAN_TYPE))
				ExceptionsUtils.ThrowToCaller(
					ClassCastException.class, 
					"Primitive type", type, "can't be cast to primitive", destType
				);
			else
				checkCast(destType, mv);
		}
		else
		{
			boxIfNeed(type, destType, mv);
			unboxIfNeed(type, destType, mv);
		}
		
		return this;
	}
	
	public AsmTypesBoxingUtils unboxIfNeed(Type type, Type destType, MethodVisitor mv)
	{
		if (type.equals(destType))
			return this;
		
		var typesUtils = asmUtils.types();
		
		if (typesUtils.isPrimitive(destType))
		{
			if (!typesUtils.isPrimitive(type))
			{
				unbox(type, mv);
			}
		}
		
		return this;
	}
	
	public AsmTypesBoxingUtils boxIfNeed(Type type, Type destType, MethodVisitor mv)
	{
		if (type.equals(destType))
			return this;
		
		var typesUtils = asmUtils.types();
		
		if (!typesUtils.isPrimitive(destType))
		{
			if (typesUtils.isPrimitive(type))
			{
				box(type, mv);
			}
		}
		
		return this;
	}
	
	public AsmTypesBoxingUtils box(Type type, MethodVisitor mv)
	{
		boxTypes.exStream()
				.findFirst((box) -> box.getUnboxed().equals(type))
				.ifPresent((box) -> {
					box.box(mv);
				})
		;
		return this;
	}
}
