package ru.swayfarer.swl3.asm.utils;

import lombok.var;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;

public class AsmMethodsUtils {

	@Internal
	public BytecodeScanner bytecodeScanner = new BytecodeScanner();

	public AsmMethodNameFinder findParametersNames()
	{
		var finder = new AsmMethodNameFinder();
		finder.bytecodeScanner(bytecodeScanner);
		return finder;
	}

	public AsmMethodsUtils invokeSpecial(MethodVisitor mv, MethodInfo methodInfo)
	{
		return invoke(Opcodes.INVOKESPECIAL, mv, methodInfo);
	}
	
	public AsmMethodsUtils invokeVirtual(MethodVisitor mv, MethodInfo methodInfo)
	{
		return invoke(Opcodes.INVOKEVIRTUAL, mv, methodInfo);
	}
	
	public AsmMethodsUtils invokeStatic(MethodVisitor mv, MethodInfo methodInfo)
	{
		return invoke(Opcodes.INVOKESTATIC, mv, methodInfo);
	}
	
	public AsmMethodsUtils invoke(int opcode, MethodVisitor mv, MethodInfo methodInfo)
	{
		var owner = methodInfo.getOwner();
		mv.visitMethodInsn(opcode, owner.getClassType().getInternalName(), methodInfo.getName(), methodInfo.getDescriptor(), false);
		
		return this;
	}
	
}
