package ru.swayfarer.swl3.asm.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.markers.Internal;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class AsmMethodNameFinder
{
    @Internal
    public BytecodeScanner bytecodeScanner;

    @Internal
    public String name;

    @Internal
    public Class<?>[] paramTypes;

    @Internal
    public Class<?> methodOwner;

    public AsmMethodNameFinder owner(Class<?> methodOwner)
    {
        this.methodOwner = methodOwner;
        return this;
    }

    public AsmMethodNameFinder inConstructor(Constructor<?> method)
    {
        this.name = "<init>";
        this.paramTypes = ExtendedStream.of(method.getParameters())
                .map(Parameter::getType)
                .toArray(Class.class)
        ;

        return this;
    }

    public AsmMethodNameFinder inMethod(Method method)
    {
        this.name = method.getName();
        this.paramTypes = ExtendedStream.of(method.getParameters())
                .map(Parameter::getType)
                .toArray(Class.class)
        ;

        return this;
    }

    public ExtendedList<String> get()
    {
        ExceptionsUtils.IfNull(methodOwner, IllegalStateException.class, "Method owner can't be null while searching method param names!");
        ExceptionsUtils.IfNull(name, IllegalStateException.class, "Method name can't be null while searching method param names!");
        ExceptionsUtils.IfNull(paramTypes, IllegalStateException.class, "Method parameter types can't be null while searching method param names!");

        return bytecodeScanner.getParametersNames(methodOwner, name, paramTypes);
    }

    public ExtendedList<String> getConstructorParametersNames(Class<?> classWithMethod, Class<?>... params)
    {
        return getMethodParametersNames(classWithMethod, "<init>", params);
    }

    public ExtendedList<String> getMethodParametersNames(Class<?> classWithMethod, Method method)
    {
        return getMethodParametersNames(classWithMethod, method.getName(), ExtendedStream.of(method.getParameters()).map(Parameter::getType).toArray(Class.class));
    }

    public ExtendedList<String> getMethodParametersNames(Class<?> classWithMethod, String name, Class<?>... params)
    {
        return bytecodeScanner.getParametersNames(classWithMethod, name, params);
    }
}
