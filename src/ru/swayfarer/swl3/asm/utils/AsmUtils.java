package ru.swayfarer.swl3.asm.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Getter
@Setter
@Accessors(chain = true)
public class AsmUtils {

	public static Type BOOLEAN_TYPE = Type.getType(Boolean.class);
	
	public static Type CHARACTER_TYPE = Type.getType(Character.class);
	
	public static Type BYTE_TYPE = Type.getType(Byte.class);
	public static Type SHORT_TYPE = Type.getType(Short.class);
	public static Type INTEGER_TYPE = Type.getType(Integer.class);
	public static Type LONG_TYPE = Type.getType(Long.class);
	
	public static Type FLOAT_TYPE = Type.getType(Float.class);
	public static Type DOUBLE_TYPE = Type.getType(Double.class);
	
	public static Type OBJECT_TYPE = Type.getType(Object.class);
	
	public static Type CLASS_TYPE = Type.getType(Class.class);
	
	public static Type THROWABLE_TYPE = Type.getType(Throwable.class);
	
	public static Type NULL_TYPE = Type.getType("Lnull;");
	
	public AsmTypesUtils typesUtils = new AsmTypesUtils(this);
	public AsmMethodsUtils methodsUtils = new AsmMethodsUtils();

	public AsmProxyUtils proxyUtils = new AsmProxyUtils().asmUtils(this);
	
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	
	public AsmTypesUtils types()
	{
		return typesUtils;
	}
	
	public AsmMethodsUtils methods()
	{
		return methodsUtils;
	}

	public AsmProxyUtils proxy()
	{
		return proxyUtils;
	}

	public static AsmUtils getInstance()
	{
		return INSTANCE;
	}

	public static AsmUtils INSTANCE = new AsmUtils();
	
}
