package ru.swayfarer.swl3.asm.info.stream.filters;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@SuppressWarnings("unchecked")
@Data @Accessors(chain = true)
public class InfoTypeFilters<Element_Type, Return_Type extends ExtendedStream<Element_Type>> {

	@NonNull
	public Return_Type wrappedStream;
	
	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	@NonNull
	public IFunction1<Element_Type, ClassInfo> typeFun;
	
	public Return_Type abstracts()
	{
		return typeFilter((classInfo) -> classInfo.getAccess().isAbstract());
	}
	
	public Return_Type interfaces()
	{
		return typeFilter((classInfo) -> classInfo.getAccess().isInterface());
	}
	
	public Return_Type assignable(Class<?>... types)
	{
		return assignable(ExtendedStream.of(types)
				.map(Type::getType)
				.toArray(Type.class)
		);
	}
	
	public Return_Type assignable(Type... types)
	{
		return typeFilter((classInfo) -> {
			
			for (var type : types)
			{
				if (classInfo.isAssignable(type))
					return true;
			}
			
			return false;
		});
	}
	
	public Return_Type extend(Class<?>... types)
	{
		return extend(ExtendedStream.of(types)
				.map(Type::getType)
				.toArray(Type.class)
		);
	}
	
	public Return_Type extend(Type... types)
	{
		return typeFilter((classInfo) -> {
			
			var parent = classInfo.getParentType();
			
			if (parent != null)
			{
				for (var type : types)
				{
					if (parent.equals(type))
						return true;
				}
			}
			
			return false;
		});
	}
	
	public Return_Type implement(Class<?>... types)
	{
		return implement(ExtendedStream.of(types)
				.map(Type::getType)
				.toArray(Type.class)
		);
	}
	
	public Return_Type is(Class<?>... types)
	{
		return is(ExtendedStream.of(types)
				.map(Type::getType)
				.toArray(Type.class)
		);
	}
	
	public Return_Type is(Type... types)
	{
		return typeFilter((classInfo) -> {
			
			for (var type : types)
			{
				if (classInfo.getClassType().equals(type))
					return true;
			}
			
			return false;
		});
	}
	
	public Return_Type implement(Type... types)
	{
		return typeFilter((classInfo) -> {
			
			for (var type : types)
			{
				if (classInfo.getInterfaces().contains(type))
					return true;
			}
			
			return false;
		});
	}
	
	protected Return_Type typeFilter(IFunction1<ClassInfo, Boolean> filter)
	{
		return (Return_Type) wrappedStream.filter((obj) -> filter.apply(typeFun.apply(obj)));
	}
}
