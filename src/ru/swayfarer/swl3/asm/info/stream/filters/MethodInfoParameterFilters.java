package ru.swayfarer.swl3.asm.info.stream.filters;

import java.util.List;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.VariableInfo;
import ru.swayfarer.swl3.asm.info.stream.MethodInfoStream;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true)
public class MethodInfoParameterFilters {

	@NonNull
	public MethodInfoStream wrappedStream;
	
	public MethodInfoStream isPresent()
	{
		return moreThan(0);
	}
	
	public MethodInfoStream assignable(Class<?>... types)
	{
		return assignable(ExtendedStream.of(types)
				.map(Type::getType)
				.toArray(Type.class))
		;
	}

	public MethodInfoStream assignable(Type... types)
	{
		return paramsFilter((params) -> {
			if (params.size() != types.length)
				return false;
			
			int nextTypeId = 0;
			
			for (var param : params)
			{
				var type = types[nextTypeId ++];
				
				if (!param.getParamTypeClassInfo().isAssignable(type))
					return false;
			}
			
			return true;
		});
	}
	
	public MethodInfoStream types(Class<?>... types)
	{
		return types(ExtendedStream.of(types)
				.map(Type::getType)
				.toArray(Type.class))
		;
	}
	
	public MethodInfoStream types(Type... types)
	{
		return paramsFilter((params) -> {
			if (params.size() != types.length)
				return false;
			
			int nextTypeId = 0;
			
			for (var param : params)
			{
				var type = types[nextTypeId ++];
				
				if (!param.getParamType().equals(type))
					return false;
			}
			
			return true;
		});
	}
	
	public MethodInfoStream isEmpty()
	{
		return count(0);
	}
	
	public MethodInfoStream count(int... counts)
	{
		return paramsCountFilter((paramsCount) -> {
			
			for (var count : counts)
			{
				if (count == paramsCount)
					return true;
			}
			
			return false;
		});
	}
	
	public MethodInfoStream lessThan(int count)
	{
		return paramsCountFilter((paramsCount) -> paramsCount < count);
	}
	
	public MethodInfoStream moreThan(int count)
	{
		return paramsCountFilter((paramsCount) -> paramsCount > count);
	}

	public MethodInfoStream someAnnotated(Type... annotations)
	{
		return paramsFilter((params) ->
			ExtendedStream.of(params)
				.filter((e) -> {
					for (var annotation : annotations)
					{
						if (e.getAnnotationsInfo().hasAssignable(annotation))
						{
							return true;
						}
					}

					return false;
				})
			.count() > 0);
	}
	
	protected MethodInfoStream paramsCountFilter(IFunction1<Integer, Boolean> fun)
	{
		return wrappedStream.filter((info) -> fun.apply(info.getParamsCount()));
	}
	
	protected MethodInfoStream paramsFilter(IFunction1<List<VariableInfo>, Boolean> fun)
	{
		return wrappedStream.filter((info) -> fun.apply(info.getParams()));
	}
}
