package ru.swayfarer.swl3.asm.info.stream.filters;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.stream.ClassInfoStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

@Data @Accessors(chain = true)
public class ClassInfoVersionFilters {

	@NonNull
	public ClassInfoStream classInfoStream;
	
	public ClassInfoStream smaller(int max)
	{
		return versionFilter((version) -> version < max);
	}
	
	public ClassInfoStream bigger(int min)
	{
		return versionFilter((version) -> version > min);
	}
	
	public ClassInfoStream between(int min, int max)
	{
		return versionFilter((version) -> min <= version && max >= version);
	}
	
	public ClassInfoStream is(int... versions)
	{
		return versionFilter((classVersion) -> {
			for (var version : versions)
				if (classVersion == version)
					return true;
			
			return false;
		});
	}
	
	protected ClassInfoStream versionFilter(IFunction1<Integer, Boolean> filter)
	{
		return classInfoStream.filter((classInfo) -> filter.apply(classInfo.getVersion()));
	}
}
