package ru.swayfarer.swl3.asm.info.stream.filters;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.string.StringUtils;

@SuppressWarnings("unchecked")
@Data @Accessors(chain = true)
public class InfoNameFilters<Element_Type, Return_Type extends ExtendedStream<Element_Type>> {

	@NonNull
	public Return_Type classInfoStream;
	
	@NonNull
	public IFunction1<Element_Type, String> nameFun;
	
	public Return_Type regex(String... masks)
	{
		return nameFilter((className) -> {
			for (var mask : masks)
			{
				if (StringUtils.isMatchesByRegex(mask, className))
					return true;
			}
			
			return false;
		});
	}
	
	public Return_Type mask(String... masks)
	{
		return nameFilter((className) -> {
			for (var mask : masks)
			{
				if (StringUtils.isMatchesByMask(mask, className))
					return true;
			}
			
			return false;
		});
	}
	
	public Return_Type is(String... names)
	{
		return nameFilter((className) -> {
			for (var name : names)
			{
				if (className.equals(name))
					return true;
			}
			
			return false;
		});
	}
	
	protected Return_Type nameFilter(IFunction1<String, Boolean> filter)
	{
		return (Return_Type) classInfoStream.filter((obj) ->filter.apply(nameFun.apply(obj)));
	}
}
