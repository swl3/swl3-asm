package ru.swayfarer.swl3.asm.info.stream.filters;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.AnnotationsInfo;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@SuppressWarnings("unchecked")
@Data @EqualsAndHashCode(callSuper = false) @Accessors(chain = true)
public class InfoAnnotationsFilter<Element_Type, Return_Type extends ExtendedStream<Element_Type>> {

	@NonNull
	public Return_Type classInfoStream;
	
	@NonNull
	public IFunction1<Element_Type, AnnotationsInfo> annotationsFun;
	
	public Return_Type assignable(Class<?>... types)
	{
		return assignable(ExtendedStream.of(types)
				.map(Type::getType)
				.toArray(Type.class)
		);
	}
	
	public Return_Type assignable(Type... types)
	{
		return annotationsFilter((annotationsInfo) -> {
			
			var assignableAnnotations = annotationsInfo.assignableAnnotations();
			
			for (var type : types)
			{
				if (assignableAnnotations.exStream().anyMatches((annotation) -> annotation.getAnnotationType().equals(type)))
					return true;
			}
			
			return false;
		});
	}
	
	public Return_Type has(Class<?>... types)
	{
		return has(ExtendedStream.of(types)
				.map(Type::getType)
				.toArray(Type.class)
		);
	}
	
	public Return_Type has(Type... types)
	{
		return annotationsFilter((annotationsInfo) -> {
			
			for (var type : types)
			{
				if (annotationsInfo.hasAnnotation(type))
					return true;
			}
			
			return false;
		});
	}
	
	protected Return_Type annotationsFilter(IFunction1<AnnotationsInfo, Boolean> filter)
	{
		return (Return_Type) classInfoStream.filter((obj) -> filter.apply(annotationsFun.apply(obj)));
	}
}
