package ru.swayfarer.swl3.asm.info.stream.filters;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.info.Access;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

@Data @Accessors(chain = true)
@SuppressWarnings("unchecked")
public class InfoAccessFilters<Element_Type, Return_Type extends ExtendedStream<Element_Type>> {

	@NonNull
	public Return_Type wrappedStream;
	
	@NonNull
	public IFunction1<Element_Type, Access> accessFun;

	public Return_Type statics()
	{
		return accessFilter(Access::isStatic);
	}
	
	public Return_Type publics()
	{
		return accessFilter(Access::isPublic);
	}
    
    public Return_Type abstracts()
    {
        return accessFilter(Access::isAbstract);
    }
	
	public Return_Type protecteds()
	{
		return accessFilter(Access::isProtected);
	}
	
	public Return_Type privates()
	{
		return accessFilter(Access::isPrivate);
	}
	
	public Return_Type accessFilter(IFunction1<Access, Boolean> filter)
	{
		return (Return_Type) wrappedStream.filter((obj) -> filter.apply(accessFun.apply(obj)));
	}
}
