package ru.swayfarer.swl3.asm.info.stream;

import java.util.stream.Stream;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.info.FieldInfo;
import ru.swayfarer.swl3.asm.info.stream.filters.InfoAccessFilters;
import ru.swayfarer.swl3.asm.info.stream.filters.InfoAnnotationsFilter;
import ru.swayfarer.swl3.asm.info.stream.filters.InfoNameFilters;
import ru.swayfarer.swl3.asm.info.stream.filters.InfoTypeFilters;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

@SuppressWarnings( {"rawtypes", "unchecked"} )
@Getter
@Setter
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class FieldInfoStream extends ExtendedStream<FieldInfo> {
	
	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	public FieldInfoStream(@NonNull IFunction0<Stream> streamCreationFun)
	{
		super(streamCreationFun);
	}
	
	public InfoNameFilters<FieldInfo, FieldInfoStream> name()
	{
		return new InfoNameFilters<FieldInfo, FieldInfoStream>(
				this, 
				FieldInfo::getName
		);
	}
	
	public InfoAccessFilters<FieldInfo, FieldInfoStream> access()
	{
		return new InfoAccessFilters<FieldInfo, FieldInfoStream>(
				this, 
				FieldInfo::getAccess
		);
	}
	
	public InfoAnnotationsFilter<FieldInfo, FieldInfoStream> annotations()
	{
		return new InfoAnnotationsFilter<FieldInfo, FieldInfoStream>(
				this, 
				FieldInfo::getAnnotationsInfo
		);
	}
	
	public InfoTypeFilters<FieldInfo, FieldInfoStream> type()
	{
		return new InfoTypeFilters<FieldInfo, FieldInfoStream>(
				this, 
				bytecodeScanner,
				FieldInfo::getFieldTypeClassInfo
		);
	}
	
	@Override
	public <T> ExtendedStream<T> create(@NonNull IFunction0<Stream> streamCreationFun)
	{
		return (ExtendedStream<T>) new FieldInfoStream(streamCreationFun);
	}
	
	@Override
	public FieldInfoStream filter(@NonNull IFunction1<FieldInfo, Boolean> filter)
	{
		return (FieldInfoStream) super.filter(filter);
	}
	
	@Override
	public FieldInfoStream not()
	{
		return (FieldInfoStream) super.not();
	}
}
