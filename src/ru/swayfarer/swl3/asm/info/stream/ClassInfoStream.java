package ru.swayfarer.swl3.asm.info.stream;

import java.util.stream.Stream;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.info.stream.filters.ClassInfoVersionFilters;
import ru.swayfarer.swl3.asm.info.stream.filters.InfoAccessFilters;
import ru.swayfarer.swl3.asm.info.stream.filters.InfoAnnotationsFilter;
import ru.swayfarer.swl3.asm.info.stream.filters.InfoNameFilters;
import ru.swayfarer.swl3.asm.info.stream.filters.InfoTypeFilters;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

@Getter
@Setter
@Accessors(chain = true) @EqualsAndHashCode(callSuper = false)
@SuppressWarnings( {"rawtypes", "unchecked"} )
public class ClassInfoStream extends ExtendedStream<ClassInfo> {

	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	public ClassInfoStream(@NonNull IFunction0<Stream> streamCreationFun)
	{
		super(streamCreationFun);
	}
	
	public ClassInfoVersionFilters version()
	{
		return new ClassInfoVersionFilters(this);
	}
	
	public InfoNameFilters<ClassInfo, ClassInfoStream> name()
	{
		return new InfoNameFilters<ClassInfo, ClassInfoStream>(
				this, 
				(classInfo) -> classInfo.getClassType().getClassName()
		);
	}
	
	public InfoAccessFilters<ClassInfo, ClassInfoStream> access()
	{
		return new InfoAccessFilters<ClassInfo, ClassInfoStream>(
				this,
				ClassInfo::getAccess
		);
	}

	public ClassInfoStream withField(GeneratedFuns.IFunction1NoR<FieldInfoStream> filter)
	{
		return filter((classInfo) -> {
			var methodsStream = classInfo.fieldsStream();
			filter.apply(methodsStream);
			return methodsStream.count() > 0;
		});
	}

	public ClassInfoStream withMethod(GeneratedFuns.IFunction1NoR<MethodInfoStream> filter)
	{
		return filter((classInfo) -> {
			var methodsStream = classInfo.methodsStream();
			filter.apply(methodsStream);
			return methodsStream.count() > 0;
		});
	}
	
	public InfoTypeFilters<ClassInfo, ClassInfoStream> type()
	{
		return new InfoTypeFilters<ClassInfo, ClassInfoStream>(
				this,
				bytecodeScanner,
				(info) -> info
		);
	}
	
	public InfoAnnotationsFilter<ClassInfo, ClassInfoStream> annotations()
	{
		return new InfoAnnotationsFilter<ClassInfo, ClassInfoStream>(
				this, 
				(info) -> info.getAnnotationsInfo()
		);
	}
	
	@Override
	public ClassInfoStream filter(@NonNull IFunction1<ClassInfo, Boolean> filter)
	{
		return ((ClassInfoStream) super.filter(filter)).setBytecodeScanner(bytecodeScanner);
	}
	
	@Override
	public ClassInfoStream not()
	{
		var ret = (ClassInfoStream) super.not();
		ret.setBytecodeScanner(bytecodeScanner);
		
		return ret;
	}
	
	@Override
	public <T> ExtendedStream<T> create(@NonNull IFunction0<Stream> streamCreationFun)
	{
		return (ExtendedStream<T>) new ClassInfoStream(streamCreationFun);
	}
}
