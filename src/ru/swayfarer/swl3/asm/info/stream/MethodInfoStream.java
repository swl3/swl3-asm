package ru.swayfarer.swl3.asm.info.stream;

import java.util.stream.Stream;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.info.stream.filters.InfoAccessFilters;
import ru.swayfarer.swl3.asm.info.stream.filters.InfoAnnotationsFilter;
import ru.swayfarer.swl3.asm.info.stream.filters.InfoNameFilters;
import ru.swayfarer.swl3.asm.info.stream.filters.InfoTypeFilters;
import ru.swayfarer.swl3.asm.info.stream.filters.MethodInfoParameterFilters;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

@Getter
@Setter
@Accessors(chain = true) @EqualsAndHashCode(callSuper = false)
@SuppressWarnings( {"rawtypes", "unchecked"} )
public class MethodInfoStream extends ExtendedStream<MethodInfo>
{
	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	public MethodInfoStream(BytecodeScanner bytecodeScanner, @NonNull IFunction0<Stream> streamCreationFun)
	{
		super(streamCreationFun);
		this.bytecodeScanner = bytecodeScanner;
	}

	public MethodInfoParameterFilters withParams()
	{
		return new MethodInfoParameterFilters(this);
	}

	public InfoNameFilters<MethodInfo, MethodInfoStream> name()
	{
		return new InfoNameFilters<MethodInfo, MethodInfoStream>(
				this, 
				(info) -> info.getName()
		);
	}
	
	public InfoAnnotationsFilter<MethodInfo, MethodInfoStream> annotations()
	{
		return new InfoAnnotationsFilter<MethodInfo, MethodInfoStream>(
				this, 
				(info) -> info.getAnnotationsInfo()
		);
	}
	
	public InfoAccessFilters<MethodInfo, MethodInfoStream> access()
	{
		return new InfoAccessFilters<MethodInfo, MethodInfoStream>(
				this, 
				(info) -> info.getAccess()
		);
	}
	
	public InfoTypeFilters<MethodInfo, MethodInfoStream> returns()
	{
		return new InfoTypeFilters<MethodInfo, MethodInfoStream>(
				this, 
				bytecodeScanner,
				MethodInfo::getReturnClassInfo
		);
	}
	
	@Override
	public MethodInfoStream filter(@NonNull IFunction1<MethodInfo, Boolean> filter)
	{
		return (MethodInfoStream) super.filter(filter);
	}
	
	@Override
	public MethodInfoStream not()
	{
		return (MethodInfoStream) super.not();
	}
	
	@Override
	public <T> ExtendedStream<T> create(@NonNull IFunction0<Stream> streamCreationFun)
	{
		return (ExtendedStream<T>) new MethodInfoStream(bytecodeScanner, streamCreationFun);
	}
}
