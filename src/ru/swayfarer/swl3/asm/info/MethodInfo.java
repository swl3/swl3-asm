package ru.swayfarer.swl3.asm.info;

import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true)
public class MethodInfo {

	public AnnotationsInfo annotationsInfo;
	
	public ExtendedList<VariableInfo> params = CollectionsSWL.list();
	
	public ExtendedList<VariableInfo> localVars = CollectionsSWL.list();
	
	@ToString.Exclude
	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	@ToString.Exclude
	@NonNull
	public ClassInfo owner;
	
	@NonNull
	public Type returnType;
	
	@NonNull
	public Access access;
	
	@NonNull
	public String name;
	
	@NonNull
	public String descriptor;

	public boolean secondaryConstructor;
	
	public int paramsCount;
	
	public MethodInfo(BytecodeScanner bytecodeScanner, String descriptor, ClassInfo owner, Type returnType, Access access, String name)
	{
		this.bytecodeScanner = bytecodeScanner;
		this.annotationsInfo = new AnnotationsInfo(bytecodeScanner);
		this.owner = owner;
		this.returnType = returnType;
		this.access = access;
		this.name = name;
		this.descriptor = descriptor;
	}

	public ExtendedList<VariableInfo> getParams()
	{
		return CollectionsSWL.list(params);
	}

	public ClassInfo getReturnClassInfo()
	{
		return bytecodeScanner.findOrCreateClassInfo(returnType);
	}
	
	public MethodInfo annotation(@NonNull AnnotationInfo annotationInfo)
	{
		annotationsInfo.getAnnotations().add(annotationInfo);
		return this;
	}
	
	public MethodInfo param(@NonNull VariableInfo paramInfo)
	{
		params.add(paramInfo);
		return this;
	}
	
	public MethodInfo local(@NonNull VariableInfo paramInfo)
	{
		localVars.add(paramInfo);
		return this;
	}

	public boolean isConstructor()
	{
		return "<init>".equals(getName());
	}
	
	public String getShortInfo()
	{
		return  getReturnType().getClassName()
				+ " "
				+ owner.getClassType().getClassName() 
				+ "." 
				+ name 
				+ "(" 
				+ StringUtils.concatWith(
						", ", 
						params
							.map(VariableInfo::getParamType)
							.map(Type::getClassName).toArray()
				)
				+ ") "
		;
	}
}
