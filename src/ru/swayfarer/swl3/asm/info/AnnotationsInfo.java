package ru.swayfarer.swl3.asm.info;

import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.scanner.AssignableAnnotationScanInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true)
public class AnnotationsInfo {
	
	@ToString.Exclude
	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	public ExtendedList<AnnotationInfo> annotations = CollectionsSWL.list();

	public ExtendedList<AnnotationInfo> all(@NonNull Class<?> type)
	{
		return all(Type.getType(type));
	}

	public ExtendedList<AnnotationInfo> all(@NonNull Type type)
	{
		return annotations.exStream()
				.filter((a) -> a.getAnnotationType().equals(type))
				.toExList()
		;
	}

	public AnnotationInfo first(@NonNull Class<?> type)
	{
		return first(Type.getType(type));
	}
	
	public AnnotationInfo first(@NonNull Type type)
	{
		return annotations.exStream()
				.findFirst((a) -> a.getAnnotationType().equals(type))
				.orNull()
		;
	}
	
	public boolean hasAssignable(Class<?> type)
	{
		return hasAssignable(Type.getType(type));
	}
	
	public boolean hasAssignable(Type type)
	{
		return !assignables(type).isEmpty();
	}

	public ExtendedList<AnnotationInfo> assignables(Type type)
	{
		return assignableAnnotations().exStream()
			.filter((annotation) -> type.equals(annotation.getAnnotationType()))
			.toExList()
		;
	}
	
	public ExtendedList<AnnotationInfo> assignableAnnotations()
	{
		var scanInfo = new AssignableAnnotationScanInfo(bytecodeScanner);
		scanInfo.load(annotations);
		return scanInfo.getVisitedTypes();
	}
	
	public boolean hasAnnotation(@NonNull Class<?> type)
	{
		return hasAnnotation(Type.getType(type));
	}
	
	public boolean hasAnnotation(@NonNull Type type)
	{
		return annotations.exStream().anyMatches((a) -> a.getAnnotationType().equals(type));
	}
}
