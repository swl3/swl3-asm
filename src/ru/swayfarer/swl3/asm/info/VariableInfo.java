package ru.swayfarer.swl3.asm.info;

import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true)
public class VariableInfo {

	public AnnotationsInfo annotationsInfo;

	@ToString.Exclude
	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	@ToString.Exclude
	@NonNull
	public MethodInfo owner;
	
	@NonNull
	public Type paramType;
	
	@NonNull
	public String name;
	
	public int index;
	
	public VariableInfo(BytecodeScanner bytecodeScanner, MethodInfo owner, Type paramType, String name)
	{
		this.bytecodeScanner = bytecodeScanner;
		this.annotationsInfo = new AnnotationsInfo(bytecodeScanner);
		this.owner = owner;
		this.paramType = paramType;
		this.name = name;
	}
	
	public VariableInfo annotation(AnnotationInfo annotationInfo)
	{
		this.annotationsInfo.getAnnotations().add(annotationInfo);
		return this;
	}
	
	public ClassInfo getParamTypeClassInfo()
	{
		return bytecodeScanner.findOrCreateClassInfo(paramType);
	}
	
	public boolean canAccept(@NonNull VariableInfo variableInfo)
	{
		if (paramType.equals(variableInfo.getParamType()))
			return true;
		
		var theirClassInfo = variableInfo.getParamTypeClassInfo();
		
		if (theirClassInfo.isAssignable(paramType))
		{
			return true;
		}
		
		return false;
	}
}
