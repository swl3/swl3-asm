package ru.swayfarer.swl3.asm.info;

import java.lang.reflect.Modifier;

import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.stream.FieldInfoStream;
import ru.swayfarer.swl3.asm.info.stream.MethodInfoStream;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true)
public class ClassInfo {

	public static BytecodeScanner emptyBytecodeScanner = new BytecodeScanner();
	
	public static ClassInfo BYTE_INFO = primitive(Type.VOID_TYPE);
	public static ClassInfo SHORT_INFO = primitive(Type.SHORT_TYPE);
	public static ClassInfo INT_INFO = primitive(Type.INT_TYPE);
	public static ClassInfo LONG_INFO = primitive(Type.LONG_TYPE);

	public static ClassInfo FLOAT_INFO = primitive(Type.FLOAT_TYPE);
	public static ClassInfo DOUBLE_INFO = primitive(Type.DOUBLE_TYPE);

	public static ClassInfo BOOLEAN_INFO = primitive(Type.BOOLEAN_TYPE);
	
	public static ClassInfo CHAR_INFO = primitive(Type.CHAR_TYPE);
	
	public static ClassInfo VOID_INFO = primitive(Type.VOID_TYPE);
	
	public AnnotationsInfo annotationsInfo;

	public int arrayDim;
	public ExtendedList<FieldInfo> fields = CollectionsSWL.list();
	public ExtendedList<MethodInfo> methods = CollectionsSWL.list();
	public ExtendedList<Type> interfaces = CollectionsSWL.list();

	@ToString.Exclude
	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	@NonNull
	public Type classType;
	
	@NonNull
	public Access access;
	
	public boolean isPrimitive;
	
	public Type parentType;
	
	public int version;
	
	public ClassInfo(BytecodeScanner bytecodeScanner, Type classType, Access access)
	{
		this.bytecodeScanner = bytecodeScanner;
		this.annotationsInfo = new AnnotationsInfo(bytecodeScanner);
		this.classType = classType;
		this.access = access;
	}
	
	public ClassInfo getParentClassInfo()
	{
		if (isPrimitive() || AsmUtils.OBJECT_TYPE.equals(parentType))
		{
			return null;
		}
		
		return bytecodeScanner.findOrCreateClassInfo(parentType);
	}
	
	public ClassInfo annotation(@NonNull AnnotationInfo annotationInfo)
	{
		annotationsInfo.getAnnotations().add(annotationInfo);
		return this;
	}
	
	public ClassInfo field(@NonNull FieldInfo fieldInfo)
	{
		fields.add(fieldInfo);
		return this;
	}
	
	public ClassInfo method(@NonNull MethodInfo methodInfo)
	{
		methods.add(methodInfo);
		return this;
	}
	
	public ClassInfo implement(@NonNull Type interfaceType)
	{
		interfaces.add(interfaceType);
		return this;
	}
	
	public MethodInfoStream methodsStream()
	{
		return new MethodInfoStream(bytecodeScanner, methods::stream);
	}
	
	public FieldInfoStream fieldsStream()
	{
		return new FieldInfoStream(fields::stream).setBytecodeScanner(bytecodeScanner);
	}
	
	public boolean isAssignable(Type type)
	{
		var classInfoType = getClassType();
		
		if (isAssignableFrom(type, this))
			return true;
		
		var classInfo = this;
		Type parent = getClassType();
		Type lastType = classInfoType;
		
		while ((parent = classInfo.getParentType()) != null)
		{
			try
			{
				if (!AsmUtils.NULL_TYPE.equals(parent))
				{
					var parentClassInfo = bytecodeScanner.findOrCreateClassInfo(parent);
					
					if (isAssignableFrom(type, parentClassInfo))
						return true;
					
					if (AsmUtils.OBJECT_TYPE.equals(parent))
						return false;
					
					classInfo = parentClassInfo;
				}
				else
				{
					break;
				}
			}
			catch (Throwable e)
			{
				bytecodeScanner.getExceptionsHandler().handle(e, "Error while scanning parent of", lastType);
				break;
			}
			
			lastType = parent;
		}
		
		return false;
	}
	
	protected boolean isAssignableFrom(Type assignableType, ClassInfo from)
	{
		if (from.getClassType().equals(assignableType))
			return true;
		
		if (from.getInterfaces().contains(assignableType))
			return true;
		
		var parent = from.getParentType();
		
		if (parent != null && parent.equals(assignableType))
			return true;
		
		return false;
	}

	public ExtendedList<Type> getAllInterfaces()
	{
		var result = new ExtendedList<Type>();
		var parent = getParentType();

		result.addAll(getInterfaces());

		while (parent != null)
		{
			var parentClassInfo = bytecodeScanner.findOrCreateClassInfo(parent);

			if (parentClassInfo == null)
				parent = null;
			else
			{
				result.addAll(parentClassInfo.getInterfaces());
				parent = parentClassInfo.getParentType();
			}
		}

		result.distinct();
		return result;
	}
	
	public static ClassInfo ofPrimitive(@NonNull Type type)
	{
		int arrayDeep = 0;

		if (type.getSort() == Type.ARRAY)
		{
			arrayDeep = StringUtils.countMatches(type.getInternalName(), "[");
			type = Type.getType(type.getDescriptor());
		}

		if (type.equals(Type.BOOLEAN_TYPE))
			return BOOLEAN_INFO;
		else if (type.equals(Type.CHAR_TYPE))
			return CHAR_INFO;
		
		else if (type.equals(Type.BYTE_TYPE))
			return BYTE_INFO;
		else if (type.equals(Type.SHORT_TYPE))
			return SHORT_INFO;
		else if (type.equals(Type.INT_TYPE))
			return INT_INFO;
		else if (type.equals(Type.LONG_TYPE))
			return LONG_INFO;
		
		else if (type.equals(Type.FLOAT_TYPE))
			return FLOAT_INFO;
		else if (type.equals(Type.DOUBLE_TYPE))
			return DOUBLE_INFO;
		
		else if (type.equals(Type.VOID_TYPE))
			return VOID_INFO;
		
		return null;
	}
	
	public static ClassInfo primitive(@NonNull Type type)
	{
		return empty(type).setPrimitive(true);
	}
	
	public static ClassInfo empty(@NonNull Type type)
	{
		return new ClassInfo(
				emptyBytecodeScanner, 
				type, 
				new Access(Modifier.PUBLIC)
		);
	}
}
