package ru.swayfarer.swl3.asm.info;

import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true)
public class FieldInfo {
	
	public AnnotationsInfo annotationsInfo;
	
	@ToString.Exclude
	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	@ToString.Exclude
	@NonNull
	public ClassInfo owner;
	
	@NonNull
	public String name;
	
	@NonNull
	public Access access;
	
	@NonNull
	public Type fieldType;
	
	public FieldInfo(BytecodeScanner bytecodeScanner, ClassInfo owner, String name, Access access, Type fieldType)
	{
		this.bytecodeScanner = bytecodeScanner;
		this.annotationsInfo = new AnnotationsInfo(bytecodeScanner);
		this.owner = owner;
		this.name = name;
		this.access = access;
		this.fieldType = fieldType;
	}
	
	public FieldInfo annotation(@NonNull AnnotationInfo annotationInfo)
	{
		annotationsInfo.getAnnotations().add(annotationInfo);
		return this;
	}
	
	public ClassInfo getFieldTypeClassInfo()
	{
		return bytecodeScanner.findOrCreateClassInfo(fieldType);
	}
}
