package ru.swayfarer.swl3.asm.info;

import java.lang.reflect.Modifier;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;

@AllArgsConstructor @NoArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class Access {

	public int modifier;

	public boolean isStatic()
	{
		return Modifier.isStatic(modifier);
	}
	
	public boolean isProtected()
	{
		return Modifier.isProtected(modifier);
	}
	
	public boolean isPublic()
	{
		return Modifier.isPublic(modifier);
	}
	
	public boolean isPrivate()
	{
		return Modifier.isPrivate(modifier);
	}
	
	public boolean isAbstract()
	{
		return Modifier.isAbstract(modifier);
	}
	
	public boolean isInterface()
	{
		return Modifier.isInterface(modifier);
	}
	
	public boolean isFinal()
	{
		return Modifier.isFinal(modifier);
	}

	public boolean isEnum()
	{
		return (modifier & Opcodes.ACC_ENUM) != 0;
	}

	public boolean isSynthetic()
	{
		return (modifier & Opcodes.ACC_SYNTHETIC) != 0;
	}
	
	public Access open()
	{
		if (isFinal())
		{
			modifier = modifier ^ Opcodes.ACC_FINAL;
		}
		
		if (isPrivate())
		{
			modifier = (modifier ^ Opcodes.ACC_PRIVATE) | Opcodes.ACC_PUBLIC;
		}
		
		if (isProtected())
		{
			modifier = (modifier ^ Opcodes.ACC_PROTECTED) | Opcodes.ACC_PUBLIC;
		}
		
		return this;
	}
	
	public static int open(int modifier)
	{
		return new Access(modifier).open().getModifier();
	}
}
