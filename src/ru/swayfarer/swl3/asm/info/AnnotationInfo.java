package ru.swayfarer.swl3.asm.info;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@SuppressWarnings("unchecked")
@Data @Accessors(chain = true)
public class AnnotationInfo {

	public AnnotationsInfo annotationsInfo;
	public Map<String, Object> params = new HashMap<>();
	
	@ToString.Exclude
	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	@NonNull
	public Type annotationType;
	
	public AnnotationInfo(BytecodeScanner bytecodeScanner, Type annotationType)
	{
		this.bytecodeScanner = bytecodeScanner;
		this.annotationsInfo = new AnnotationsInfo(bytecodeScanner);
		this.annotationType = annotationType;
	}
	
	public AnnotationInfo annotation(AnnotationInfo annotationInfo)
	{
		annotationsInfo.getAnnotations().add(annotationInfo);
		return this;
	}
	
	public AnnotationInfo param(String name, Object value)
	{
		params.put(name, value);
		return this;
	}
	
	public <T> ExtendedList<T> getParams(String name, Class<T> typeOfParam)
	{
		var paramValue = params.get(name);
		
		if (paramValue != null)
		{
			ExtendedList<T> ret = null;

			if ((paramValue instanceof List))
			{
				var list = (List<T>) paramValue;
				ret = new ExtendedList<>();
				ret.addAll(list);
			}
			else
			{
				ret = ReflectionsUtils.cast(CollectionsSWL.list(paramValue));
			}
			
			if (ret.isEmpty())
				return null;

			var firstElem = ret.first();
			
			if (typeOfParam.isAssignableFrom(firstElem.getClass()))
				return ret;
		}
		
		return null;
	}
	
	public <T> T getParam(String name, Class<T> typeOfParam)
	{
		var paramValue = params.get(name);
		
		if (paramValue != null)
		{
			var typeOfValue = paramValue.getClass();
			if (typeOfParam != typeOfValue || !typeOfParam.isAssignableFrom(typeOfValue))
				return null;
		}
		
		return (T) paramValue;
	}
	
	public <T> T getParam(String name)
	{
		return (T) params.get(name);
	}
}
