package ru.swayfarer.swl3.asm.scanner;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.Access;
import ru.swayfarer.swl3.asm.info.AnnotationInfo;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.FieldInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.AnnotationVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.FieldVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true) @EqualsAndHashCode(callSuper = false)
public class ClassScanner extends ClassVisitor {

	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	@NonNull
	public ClassInfo scannedClassInfo;
	
	public ClassScanner(ClassVisitor classVisitor)
	{
		super(ASM8_EXPERIMENTAL, classVisitor);
	}
	
	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces)
	{
		scannedClassInfo = new ClassInfo(bytecodeScanner, findTypeByInternalName(name), new Access(access));
		scannedClassInfo.setParentType(findTypeByInternalName(superName));
		scannedClassInfo.setVersion(version);
		
		ExtendedStream.of(interfaces)
			.map((desc) -> Type.getType("L" + desc + ";"))
			.each(scannedClassInfo::implement)
		;
		
		super.visit(version, access, name, signature, superName, interfaces);
	}
	
	@Override
	public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions)
	{
		var methodInfo = new MethodInfo(
				bytecodeScanner, 
				descriptor,
				scannedClassInfo, 
				Type.getReturnType(descriptor), 
				new Access(access), 
				name
		);
		scannedClassInfo.method(methodInfo);
		methodInfo.setParamsCount(Type.getArgumentTypes(descriptor).length);
		
		var mv = super.visitMethod(access, name, descriptor, signature, exceptions);
		var scanner = new MethodScanner(mv);
		scanner.setScannedMethod(methodInfo);
		scanner.setBytecodeScanner(bytecodeScanner);
		scanner.loadMethodParamsFromDescriptor();
		
		return scanner;
	}
	
	@Override
	public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value)
	{
		var cv = super.visitField(access, name, descriptor, signature, value);
		var fieldScanner = new FieldScanner(cv);
		
		var fieldInfo = new FieldInfo(
				bytecodeScanner,
				scannedClassInfo, 
				name, 
				new Access(access), 
				Type.getType(descriptor)
		);
		
        scannedClassInfo.field(fieldInfo);
		
		fieldScanner.setScannedFieldInfo(fieldInfo);
		fieldScanner.setBytecodeScanner(bytecodeScanner);
		
		
		return fieldScanner;
	}
	
	@Override
	public AnnotationVisitor visitAnnotation(String descriptor, boolean visible)
	{
		var annotation = new AnnotationInfo(bytecodeScanner, Type.getType(descriptor));
		scannedClassInfo.annotation(annotation);
		
		var av = super.visitAnnotation(descriptor, visible);
		var scanner = new AnnotationScanner(av);
		scanner.setScannedAnnotation(annotation);
		scanner.setBytecodeScanner(bytecodeScanner);
		return scanner;
	}
	
	protected Type findTypeByInternalName(String internalName)
	{
		return Type.getType("L"+internalName+";");
	}
}
