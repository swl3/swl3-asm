package ru.swayfarer.swl3.asm.scanner;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.AnnotationInfo;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.AnnotationVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true) @EqualsAndHashCode(callSuper = false)
public class AnnotationArrayScanner extends AnnotationVisitor {
	
	@NonNull
	public BytecodeScanner bytecodeScanner;
	public ExtendedList<Object> values = CollectionsSWL.list();
	
	public AnnotationArrayScanner(BytecodeScanner bytecodeScanner, AnnotationVisitor av)
	{
		super(ASM8_EXPERIMENTAL, av);
		this.bytecodeScanner = bytecodeScanner;
	}

	@Override
	public void visit(String name, Object value)
	{
		values.add(value);
		super.visit(name, value);
	}
	
	@Override
	public void visitEnum(String name, String descriptor, String value)
	{
		values.add(value);
		super.visitEnum(name, descriptor, value);
	}
	
	@Override
	public AnnotationVisitor visitArray(String name)
	{
		var arrayScanner = new AnnotationArrayScanner(bytecodeScanner, super.visitArray(name));
		values.add(arrayScanner.getValues());
		return arrayScanner;
	}
	
	@Override
	public AnnotationVisitor visitAnnotation(String name, String descriptor)
	{
		var annotationScanner = new AnnotationScanner(super.visitAnnotation(name, descriptor));
		annotationScanner.setBytecodeScanner(bytecodeScanner);
		var annotation = new AnnotationInfo(bytecodeScanner, Type.getType(descriptor));
		annotationScanner.setScannedAnnotation(annotation);
		
		values.add(annotation);
		
		return annotationScanner;
	}
}
