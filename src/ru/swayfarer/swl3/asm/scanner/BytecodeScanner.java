package ru.swayfarer.swl3.asm.scanner;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.VariableInfo;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.set.LinkedSet;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassReader;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static ru.swayfarer.swl3.string.StringUtils.concat;

@Getter
@Setter
@Accessors(chain = true)
public class BytecodeScanner {
	public Set<String> alreadyLoggedMissingResources = new LinkedSet<>();

	public ILogger logger = LogFactory.getLogger();

	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	
	public IFunction1<String, InputStream> resourceToBytesFun = getClass().getClassLoader()::getResourceAsStream;
	
	public Map<Type, ClassInfo> cachedClassInfos = Collections.synchronizedMap(new HashMap<>());
	
	public ClassInfo findClassInfo(@NonNull Type type)
	{
		return cachedClassInfos.get(type);
	}
	
	public ClassInfo findOrCreateClassInfo(@NonNull Type type)
	{
		var cached = cachedClassInfos.get(type);
		
		if (cached == null)
		{
			cached = scanClass(type);
			cachedClassInfos.put(type, cached);
		}
		
		return cached;
	}
	
	public ClassInfo scanClass(@NonNull Class<?> cl)
	{
		return scanClass(Type.getType(cl));
	}
	
	@SneakyThrows
	public ClassInfo scanClass(@NonNull Type type)
	{
		if (type.getSort() == Type.ARRAY)
			type = type.getElementType();

		var prim = ClassInfo.ofPrimitive(type);
		
		if (prim != null)
			return prim;

		var typeResourceName = getTypeResourceName(type);
		
		var classResourceStream = resourceToBytesFun.apply(typeResourceName);

		if (classResourceStream == null)
		{
			if (!alreadyLoggedMissingResources.contains(typeResourceName))
			{
				logger.warn("Can't find ", typeResourceName, "resource!");
				alreadyLoggedMissingResources.add(typeResourceName);
			}

			return null;
		}

		return scanClass(classResourceStream);
	}
	
	@SneakyThrows
	public ClassInfo scanClass(@NonNull InputStream stream)
	{
		var classReader = new ClassReader(stream);
		var classScanner = new ClassScanner(null);
		classScanner.setBytecodeScanner(this);
		
		classReader.accept(classScanner, ClassReader.EXPAND_FRAMES);
		return classScanner.getScannedClassInfo();
	}
	
	public String getTypeResourceName(@NonNull Type type)
	{
		var internalName = type.getInternalName();
		return internalName + ".class";
	}

	public ExtendedList<String> getParametersNames(Class<?> classWithMethod, String methodName, Class<?>... paramTypes)
	{
		var classWithMethodInfo = findOrCreateClassInfo(Type.getType(classWithMethod));

		return classWithMethodInfo.methodsStream()
				.name().is(methodName)
				.withParams().types(paramTypes)
				.findFirst()
				.map((m) -> m.getParams().map(VariableInfo::getName))
				.orElseThrow(() -> new NoSuchMethodException("Can't find method " + methodName + "(" + concat(",", paramTypes) + ") in" + classWithMethod))
		;
	}
}
