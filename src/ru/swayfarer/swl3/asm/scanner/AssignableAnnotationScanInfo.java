package ru.swayfarer.swl3.asm.scanner;

import java.util.List;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.AnnotationInfo;
import ru.swayfarer.swl3.asm.info.AnnotationsInfo;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;

@Data @Accessors(chain = true)
public class AssignableAnnotationScanInfo {
	
	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	public ExtendedList<AnnotationInfo> visitedTypes = CollectionsSWL.list();
	
	public AssignableAnnotationScanInfo load(AnnotationsInfo annotationsInfo)
	{
		return load(annotationsInfo.getAnnotations());
	}
	
	public AssignableAnnotationScanInfo load(List<AnnotationInfo> annotations)
	{
		for (var annotation : annotations)
		{
			if (!visitedTypes.contains(annotation))
			{
				visitedTypes.add(annotation);
				
				var classInfo = bytecodeScanner.findOrCreateClassInfo(annotation.getAnnotationType());
				
				if (classInfo != null) 
				{
					load(classInfo.getAnnotationsInfo());
				}
			}
		}
		
		return this;
	}
}
