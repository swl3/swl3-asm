package ru.swayfarer.swl3.asm.scanner;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.AnnotationInfo;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.AnnotationVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true) @EqualsAndHashCode(callSuper = false)
public class AnnotationScanner extends AnnotationVisitor {

	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	@NonNull
	public AnnotationInfo scannedAnnotation;
	
	public AnnotationScanner(AnnotationVisitor annotationVisitor)
	{
		super(ASM8_EXPERIMENTAL, annotationVisitor);
	}
	
	@Override
	public void visit(String name, Object value)
	{
		scannedAnnotation.param(name, value);
		super.visit(name, value);
	}
	
	@Override
	public void visitEnum(String name, String descriptor, String value)
	{
		scannedAnnotation.param(name, value);
		super.visitEnum(name, descriptor, value);
	}
	
	@Override
	public AnnotationVisitor visitArray(String name)
	{
		var arrayScanner = new AnnotationArrayScanner(bytecodeScanner, super.visitArray(name));
		scannedAnnotation.param(name, arrayScanner.getValues());
		return arrayScanner;
	}
	
	@Override
	public AnnotationVisitor visitAnnotation(String name, String descriptor)
	{
		AnnotationInfo annotationInfo = new AnnotationInfo(bytecodeScanner, Type.getType(descriptor));
		scannedAnnotation.param(name, annotationInfo);
		
		var av = super.visitAnnotation(name, descriptor);
		var scanner = new AnnotationScanner(av);
		
		scanner.setBytecodeScanner(bytecodeScanner);
		scanner.setScannedAnnotation(annotationInfo);
		return scanner;
	}
}
