package ru.swayfarer.swl3.asm.scanner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.AnnotationInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.info.VariableInfo;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.AnnotationVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Label;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Getter
@Setter
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class MethodScanner extends MethodVisitor {
	
    public Map<Integer, List<AnnotationInfo>> varIdToAnnotation = new HashMap<>();
	
	@NonNull 
	public BytecodeScanner bytecodeScanner;
	
	@NonNull
	public MethodInfo scannedMethod;
	
	public MethodScanner(MethodVisitor methodVisitor)
	{
		super(ASM8_EXPERIMENTAL, methodVisitor);
	}
	
	@Override
	public AnnotationVisitor visitParameterAnnotation(int parameter, String descriptor, boolean visible)
	{
		var annotation = new AnnotationInfo(bytecodeScanner, Type.getType(descriptor));
		cacheVarAnnotation(parameter, annotation);
		
		var av = super.visitParameterAnnotation(parameter, descriptor, visible);
		var scanner = new AnnotationScanner(av);
		scanner.setScannedAnnotation(annotation);
		scanner.setBytecodeScanner(bytecodeScanner);
		
		return scanner;
	}
	
	public void cacheVarAnnotation(int index, AnnotationInfo annotationInfo)
	{
		var list = varIdToAnnotation.get(index);
		
		if (list == null)
		{
			list = new ArrayList<>();
			varIdToAnnotation.put(index, list);
		}
		
		list.add(annotationInfo);
	}
	
	@Override
	public void visitLocalVariable(String name, String descriptor, String signature, Label start, Label end, int index)
	{
		VariableInfo variableInfo = null;

		var paramById = findParameterById(index);

		if (paramById == null)
		{
			variableInfo = new VariableInfo(
					bytecodeScanner,
					scannedMethod,
					Type.getType(descriptor), name
			);

			variableInfo.setIndex(index);
			scannedMethod.local(variableInfo);
		}
		else
		{
			paramById.setName(name);
			variableInfo = paramById;
		}

		var cachedAnnotations = varIdToAnnotation.get(index);
		
		if (cachedAnnotations != null)
			variableInfo.getAnnotationsInfo().getAnnotations().addAll(cachedAnnotations);

		super.visitLocalVariable(name, descriptor, signature, start, end, index);
	}

	protected VariableInfo findParameterById(int index)
	{
		return scannedMethod.getParams().exStream().first((param) -> param.getIndex() == index);
	}

	public MethodScanner loadMethodParamsFromDescriptor()
	{
		String descriptor = scannedMethod.getDescriptor();
		var argumentTypes = Type.getArgumentTypes(descriptor);

		int nextIndex = scannedMethod.getAccess().isStatic() ? 0 : 1;

		for (var type : argumentTypes)
		{
			var cachedAnnotations = varIdToAnnotation.get(nextIndex);
			var param = new VariableInfo(bytecodeScanner, scannedMethod, type, "par"+nextIndex);

			param.setIndex(nextIndex);

			if (cachedAnnotations != null)
				param.getAnnotationsInfo().getAnnotations().addAll(cachedAnnotations);

			scannedMethod.param(param);

			nextIndex += type.getSize();
		}
	    
	    return this;
	}

	@Override
	public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface)
	{
		if (opcode == INVOKESPECIAL && name.equals("<init>") && owner.equals(scannedMethod.getOwner().getClassType().getInternalName()))
			scannedMethod.setSecondaryConstructor(true);

		super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
	}

	@Override
	public void visitMethodInsn(int opcode, String owner, String name, String descriptor)
	{
		if (opcode == INVOKESPECIAL && name.equals("<init>") && owner.equals(scannedMethod.getOwner().getClassType().getInternalName()))
			scannedMethod.setSecondaryConstructor(true);

		super.visitMethodInsn(opcode, owner, name, descriptor);
	}

	protected boolean isStatic()
	{
		return scannedMethod.getAccess().isStatic();
	}
	
	@Override
	public AnnotationVisitor visitAnnotation(String descriptor, boolean visible)
	{
		var annotation = new AnnotationInfo(bytecodeScanner, Type.getType(descriptor));
		scannedMethod.annotation(annotation);
		
		var av = super.visitAnnotation(descriptor, visible);
		var scanner = new AnnotationScanner(av);
		scanner.setScannedAnnotation(annotation);
		scanner.setBytecodeScanner(bytecodeScanner);
		
		return scanner;
	}

	@Override
	public void visitEnd()
	{
        super.visitEnd();
	}
}
