package ru.swayfarer.swl3.asm.scanner;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.AnnotationInfo;
import ru.swayfarer.swl3.asm.info.FieldInfo;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.AnnotationVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.FieldVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true) @EqualsAndHashCode(callSuper = false)
public class FieldScanner extends FieldVisitor {

	@NonNull 
	public BytecodeScanner bytecodeScanner;
	
	@NonNull
	public FieldInfo scannedFieldInfo;
	
	public FieldScanner(FieldVisitor fieldVisitor)
	{
		super(ASM8_EXPERIMENTAL, fieldVisitor);
	}
	
	@Override
	public AnnotationVisitor visitAnnotation(String descriptor, boolean visible)
	{
		var annotation = new AnnotationInfo(bytecodeScanner, Type.getType(descriptor));
		scannedFieldInfo.annotation(annotation);
		
		var av = super.visitAnnotation(descriptor, visible);
		var scanner = new AnnotationScanner(av);
		scanner.setScannedAnnotation(annotation);
		scanner.setBytecodeScanner(bytecodeScanner);
		
		return scanner;
	}

}
