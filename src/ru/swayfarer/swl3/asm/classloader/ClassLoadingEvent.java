package ru.swayfarer.swl3.asm.classloader;

import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.observable.event.AbstractCancelableEvent;

@EqualsAndHashCode(callSuper = true)
@Data @Accessors(chain = true)
public class ClassLoadingEvent extends AbstractCancelableEvent{

	@NonNull
	public String className;
	
	@NonNull
	public DynamicClassLoader classLoader;
	
	public byte[] classBytes;
	
	public URL classLocation;
	public CodeSource codeSource;
	public ProtectionDomain protectionDomain;
	
	public boolean isLocationFound()
	{
		return classLocation != null;
	}
	
	public boolean isCodeSourceFound()
	{
		return codeSource != null;
	}
	
	public boolean isProtectionDomainFound()
	{
		return protectionDomain != null;
	}
	
	public boolean isJavaRoot()
	{
		return isLocationFound() && classLocation.getProtocol().equals("jrt");
	}
}
