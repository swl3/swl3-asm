package ru.swayfarer.swl3.asm.classloader.filtering;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.classloader.ClassLoadingEvent;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.string.StringUtils;

public class FilteringFun implements IFunction1NoR<ClassLoadingEvent> {

	public ExtendedList<IFunction1NoR<ClassLoadingEvent>> filters = CollectionsSWL.list();
	
	@Override
	public void applyNoRUnsafe(ClassLoadingEvent event)
	{
		for (var filter : filters)
		{
			filter.apply(event);
		}
	}
	
	public RuleConfigurator rule()
	{
		return new RuleConfigurator(this);
	}
	
	@Data @Accessors(chain = true)
	public static class RuleConfigurator {
		
		@NonNull
		public FilteringFun filteringFun;
		
		public IFunction1<ClassLoadingEvent, Boolean> filterFun = (evt) -> false;
		
		public RuleConfigurator nameMask(String nameMask)
		{
			filterFun = (evt) -> StringUtils.isMatchesByMask(nameMask, evt.getClassName());
			return this;
		}
		
		public FilteringFun allow()
		{
			filteringFun.filters.add((evt) -> {
				if (filterFun.apply(evt))
				{
					evt.setCanceled(false);
				}
			});
			return filteringFun;
		}
		
		public FilteringFun deny()
		{
			filteringFun.filters.add((evt) -> {
				if (filterFun.apply(evt))
				{
					evt.setCanceled(true);
				}
			});
			return filteringFun;
		}
	}
}
