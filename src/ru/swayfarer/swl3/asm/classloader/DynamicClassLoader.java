package ru.swayfarer.swl3.asm.classloader;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.security.ProtectionDomain;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.Manifest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.io.streams.StreamsUtils;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false) @Accessors(chain = true)
public class DynamicClassLoader extends URLClassLoader {

	public volatile Map<String, Class<?>> cachedClasses = Collections.synchronizedMap(new HashMap<>());
	
	@Accessors(fluent = true)
	public IObservable<ClassLoadingEvent> eventFiltering = Observables.createObservable();
	
	@Accessors(fluent = true)
	public IObservable<ClassLoadingEvent> eventLocationFind = Observables.createObservable();
	
	@Accessors(fluent = true)
	public IObservable<ClassLoadingEvent> eventCodeSourceFind = Observables.createObservable();
	
	@Accessors(fluent = true)
	public IObservable<ClassLoadingEvent> eventProtectionDomainFind = Observables.createObservable();
	
	@Accessors(fluent = true)
	public IObservable<ClassLoadingEvent> eventClassLoading = Observables.createObservable();
	
	@NonNull
	public ClassLoader wrappedClassloader;
	
	public Method findBootstrapClassOrNullMethod;
	
	@SneakyThrows
	protected Class<?> findBootstrapClassOrNullByReflection(String name)
	{
		return (Class<?>) findBootstrapClassOrNullMethod.invoke(this, name);
	}
	
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	
	public DynamicClassLoader()
	{
		this(DynamicClassLoader.class.getClassLoader(), null);
	}
	
	public DynamicClassLoader(ClassLoader wrappedClassloader, ClassLoader parent)
	{
		this(wrappedClassloader, new URL[0], parent);
	}
	
	public DynamicClassLoader(ClassLoader wrappedClassloader, URL[] urls, ClassLoader parent)
	{
		super(urls, parent);
		this.wrappedClassloader = wrappedClassloader;
		
		try
		{
			findBootstrapClassOrNullMethod = ClassLoader.class.getDeclaredMethod("findBootstrapClassOrNull", String.class);
			findBootstrapClassOrNullMethod.setAccessible(true);
		}
		catch (Throwable e)
		{
			e.printStackTrace();
		}
		
		if (wrappedClassloader instanceof URLClassLoader)
		{
		    var wrappedUrls = ((URLClassLoader) wrappedClassloader).getURLs();
		    
		    for (var wrappedUrl : wrappedUrls)
		    {
		        addURL(wrappedUrl);
		    }
		}
	}
	
	public ClassloaderStarter starter()
	{
		return new ClassloaderStarter(this);
	}
	
	public DynamicClassLoaderConfigurator configure()
	{
		return new DynamicClassLoaderConfigurator(this);
	}
	
	public DynamicClassLoader defaultConfiguration()
	{
		configure().defaultConfiguration();
		return this;
	}
	
	protected Class<?> findClass(ClassLoadingEvent event) throws Throwable
	{
		eventClassLoading.next(event);
		var className = event.getClassName();
		var classBytes = event.getClassBytes();
		var protectionDomain = event.getProtectionDomain();
		
		return defineClass(className, classBytes, 0, classBytes.length, protectionDomain);
	}
	
	public Class<?> defineClass(String name, byte[] bytes, ProtectionDomain protectionDomain)
	{
		return defineClass(name, bytes, 0, bytes.length, protectionDomain);
	}
	
	public ExtendedOptional<Class<?>> findClassByName(String name)
	{
		var loadedClass = exceptionsHandler.safeReturn(() -> loadClass(name), null, "Error while loading class", name);
		return ExtendedOptional.of(loadedClass);
	}
	
	@Override
	public URL getResource(String name)
	{
	    var ret = super.getResource(name);
	    
	    if (ret != null)
	        return ret;
	    
		return wrappedClassloader.getResource(name);
	}
	
	@Override
	public Enumeration<URL> getResources(String name) throws IOException
	{
	    var ret = super.getResources(name);
    
        if (ret != null)
            return ret;
    
		return wrappedClassloader.getResources(name);
	}
	
	@Override
	public InputStream getResourceAsStream(String name)
	{
	    var ret = super.getResourceAsStream(name);
	    
        if (ret != null)
            return ret;
    
        return wrappedClassloader.getResourceAsStream(name);
	}
	
	@Override
	public URL[] getURLs()
	{
	    var ret = new ExtendedList<URL>();
	    ret.addAll(super.getURLs());
	    ret.distinct();
	    
	    return ret.toArray(URL.class);
	}
	
	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException
	{
		Class<?> ret = null;
		
		try
		{
			var event = new ClassLoadingEvent(name, this);
			
			eventFiltering.next(event);
			
			if (!event.isCanceled())
			{
				eventLocationFind.next(event);
				
				if (event.isLocationFound() && !event.isJavaRoot())
				{
					event.setClassBytes(StreamsUtils.readAllAndClose(event.getClassLocation().openStream()));
					
					eventCodeSourceFind.next(event);
					
					if (event.isCodeSourceFound())
					{
						eventProtectionDomainFind.next(event);
					}
					
					loadPackage(event);
					ret = findClass(event);
				}
			}
		}
		catch (Throwable e)
		{
			exceptionsHandler.handle(e, "Error while loading class", name);
		}
		
		if (ret == null)
		{
			if (wrappedClassloader != null)
				ret = wrappedClassloader.loadClass(name);
			else
				ret = super.loadClass(name);
		}
		
		if (ret == null)
			ExceptionsUtils.ThrowToCaller(ClassNotFoundException.class, "Class", name, "can't be found!");
		
		return ret;
	}
	
	@SuppressWarnings("deprecation")
	protected void loadPackage(ClassLoadingEvent event)
	{
		ExceptionsUtils.safe(() -> {
			var pkgName = event.getClassName();
			var source = event.getCodeSource();
			
			if (pkgName.contains("."))
			{
				pkgName = pkgName.substring(0, pkgName.lastIndexOf("."));
				Package pkg = getPackage(pkgName);
				
				if (pkg != null)
					return;
				
				URLConnection connection = source.getLocation().openConnection();
				
				if (connection instanceof JarURLConnection)
				{
					Manifest man = ((JarURLConnection) connection).getManifest();
					definePackage(pkgName, man, source.getLocation());
				}
				else
				{
					definePackage(pkgName, null, null, null, null, null, null, null);
				}
			}
		});
	}
	
	@SuppressWarnings("resource")
    public static DynamicClassLoader defaultSettings()
	{
	    return new DynamicClassLoader()
	            .defaultConfiguration()
        ;
	}
	
	@Override
	public void addURL(URL url)
	{
	    super.addURL(url);
	}
}
