package ru.swayfarer.swl3.asm.classloader.dumps;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.classloader.ClassLoadingEvent;
import ru.swayfarer.swl3.collections.stream.AbstractFiltering;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.string.filtering.StringFiltering;

@Getter
@Setter
@Accessors(chain = true) @EqualsAndHashCode(callSuper = false)
public class ClassloadingEventFiltering<Ret_Type> extends AbstractFiltering<ClassLoadingEvent, ClassloadingEventFiltering<Ret_Type>, Ret_Type> {

	public ClassloadingEventFiltering(@NonNull IFunction1<IFunction1<ClassLoadingEvent, Boolean>, Ret_Type> filterFun)
	{
		super(filterFun);
	}

	public StringFiltering<Ret_Type> name()
	{
		return new StringFiltering<Ret_Type>(
				(filter) -> this.filter((evt) -> filter.apply(evt.getClassName()))
		);
	}
}
