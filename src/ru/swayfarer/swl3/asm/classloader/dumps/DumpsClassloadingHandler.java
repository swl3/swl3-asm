package ru.swayfarer.swl3.asm.classloader.dumps;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.classloader.ClassLoadingEvent;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.io.streams.PacketBytesOutStream;

@Getter
@Setter
@Accessors(chain = true)
public class DumpsClassloadingHandler implements IFunction1NoR<ClassLoadingEvent> {

	public ExtendedList<DumpRule> rules = CollectionsSWL.list();
	
	@Override
	public void applyNoRUnsafe(@NonNull ClassLoadingEvent evt)
	{
		for (var rule : rules)
		{
			rule.processIfAccepts(evt);
		}
	}
	
	public DumpRuleConfigurator rule()
	{
		var rule = new DumpRule();
		this.rules.add(rule);
		return new DumpRuleConfigurator(rule, this);
	}
	
	@Getter @Setter @Accessors(chain = true)
	public static class DumpRuleConfigurator {
	
		@NonNull
		public DumpRule rule;
		
		@NonNull
		public DumpsClassloadingHandler handler;

		public DumpRuleConfigurator(@NonNull DumpRule rule, @NonNull DumpsClassloadingHandler handler)
		{
			this.rule = rule;
			this.handler = handler;
		}

		public ClassloadingEventFiltering<DumpRuleConfigurator> filter()
		{
			return new ClassloadingEventFiltering<>(this::addFilter);
		}
		
		public DumpRuleDestConfigurator to()
		{
			return new DumpRuleDestConfigurator(handler, rule);
		}
		
		protected DumpRuleConfigurator addFilter(IFunction1<ClassLoadingEvent, Boolean> filter)
		{
			rule.addFilter(filter);
			return this;
		}
	}
	
	@Getter
	@Setter
	@Accessors(chain = true)
	public static class DumpRuleDestConfigurator {
		
		@NonNull
		public DumpsClassloadingHandler handler;
		
		@NonNull
		public DumpRule rule;

		public DumpRuleDestConfigurator(@NonNull DumpsClassloadingHandler handler, @NonNull DumpRule rule)
		{
			this.handler = handler;
			this.rule = rule;
		}

		public DumpsClassloadingHandler stream(@NonNull DataOutStream dos)
		{
			return dest((evt) -> {
				var classBytes = evt.getClassBytes();
				
				dos.packet()
					.writeUTF8(evt.getClassName())
					.writeInt(classBytes.length)
					.<PacketBytesOutStream>writeBytes(classBytes)
				.send();
			});
		}
		
		public DumpsClassloadingHandler dir(@NonNull String path)
		{
			return dir(FileSWL.of(path));
		}
		
		public DumpsClassloadingHandler dir(@NonNull FileSWL dir)
		{
			return dest((evt) -> {
				var classFile = FileSWL.of(dir, evt.getClassName().replace(".", "/").concat(".class"));
				
				classFile.createIfNotFound()
					.out()
						.writeBytes(evt.getClassBytes())
						.close()
				;
			});
		}
		
		public DumpsClassloadingHandler dest(IFunction1NoR<ClassLoadingEvent> writer)
		{
			rule.addWriter(writer);
			return handler;
		}
	}
	
	@NoArgsConstructor @AllArgsConstructor
	@Getter @Setter @Accessors(chain = true)
	public static class DumpRule {
		
		@NonNull
		public IFunction1<ClassLoadingEvent, Boolean> filterFun = (e) -> true;
		
		@NonNull
		public IFunction1NoR<ClassLoadingEvent> writerFun = (e) -> {};
	
		public boolean isAccepts(@NonNull ClassLoadingEvent evt)
		{
			return filterFun == null ? true : filterFun.apply(evt);
		}
		
		public void process(@NonNull ClassLoadingEvent evt)
		{
			writerFun.apply(evt);
		}
		
		public void processIfAccepts(@NonNull ClassLoadingEvent evt)
		{
			if (isAccepts(evt))
				process(evt);
		}
		
		public DumpRule addWriter(IFunction1NoR<ClassLoadingEvent> writer)
		{
			this.writerFun = this.writerFun.andThan(writer);
			return this;
		}
		
		public DumpRule addFilter(IFunction1<ClassLoadingEvent, Boolean> filter)
		{
			this.filterFun = this.filterFun.and(filter);
			return this;
		}
	}
}
