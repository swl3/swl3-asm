package ru.swayfarer.swl3.asm.classloader.location;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.classloader.ClassLoadingEvent;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Getter
@Setter
@Accessors(chain = true)
public class ClassloaderLocationFun implements IFunction1NoR<ClassLoadingEvent> {

	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	public IFunction1<ClassLoadingEvent, ClassLoader> classloaderFun = ClassLoadingEvent::getClassLoader;
	
	@Override
	public void applyNoRUnsafe(ClassLoadingEvent event)
	{
		if (event.isLocationFound())
			return;
		
		var className = event.getClassName();
		var classloader = classloaderFun.apply(event);
		
		try
		{
			if (classloader != null)
			{
				event.setClassLocation(classloader.getResource(getClassResourceName(className)));
			}
		}
		catch (Throwable e)
		{
			exceptionsHandler.handle(e, "Error while finding class", className, "location by classloader", classloader);
		}
	}

	public String getClassResourceName(String className)
	{
		return className.replace(".", "/").concat(".class");
	}
}
