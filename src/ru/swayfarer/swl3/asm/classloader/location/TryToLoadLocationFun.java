package ru.swayfarer.swl3.asm.classloader.location;

import lombok.var;
import ru.swayfarer.swl3.asm.classloader.ClassLoadingEvent;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.reflection.ClassUtil;

public class TryToLoadLocationFun implements IFunction1NoR<ClassLoadingEvent> {

	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();

	@Override
	public void applyNoRUnsafe(ClassLoadingEvent event)
	{
		if (event.isLocationFound())
			return;
		
		try
		{
			var loadedClass = ClassUtil.forName(event.getClassName());
			var res = loadedClass.getResource("/" + event.getClassName().replace(".", "/").concat(".class"));
			event.setClassLocation(res);
		}
		catch (Throwable e)
		{
			System.out.println("Class " + event.getClassName() + " can be loaded!");
			exceptionsHandler.handle(e, "");
		}
	}

}
