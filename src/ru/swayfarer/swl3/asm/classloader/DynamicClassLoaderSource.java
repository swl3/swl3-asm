package ru.swayfarer.swl3.asm.classloader;

import java.io.File;
import java.net.URL;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.io.file.FileSWL;

@Data
@Accessors(chain = true)
public class DynamicClassLoaderSource {
    
    @NonNull
    public DynamicClassLoaderConfigurator dynamicClassLoaderConfigurator;
    
    public DynamicClassLoaderConfigurator file(@NonNull File file)
    {
        return file(FileSWL.of(file.getParent()));
    }
    
    public DynamicClassLoaderConfigurator file(@NonNull String filepath)
    {
        return file(FileSWL.of(filepath));
    }
    
    public DynamicClassLoaderConfigurator file(@NonNull FileSWL file)
    {
        return url(file.asURL());
    }
    
    public DynamicClassLoaderConfigurator allJars(@NonNull String dir)
    {
        return allJars(FileSWL.of(dir));
    }
    
    public DynamicClassLoaderConfigurator allJars(@NonNull FileSWL dir)
    {
        dir.findAllSubfiles()
                .filter((f) -> f.exists() && f.isFile() && f.getExtension().equals("jar"))
                .each(this::file)
        ;
        
        return dynamicClassLoaderConfigurator;
    }
    
    public DynamicClassLoaderConfigurator url(@NonNull URL url)
    {
        dynamicClassLoaderConfigurator.classLoader.addURL(url);
        return dynamicClassLoaderConfigurator;
    }
}
