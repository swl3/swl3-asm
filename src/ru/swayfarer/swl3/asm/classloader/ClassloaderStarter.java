package ru.swayfarer.swl3.asm.classloader;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Getter
@Setter
@Accessors(chain = true)
@SuppressWarnings("unchecked")
public class ClassloaderStarter {

	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	
	public ReflectionsUtils reflectionsUtils = new ReflectionsUtils();
	
	@NonNull
	public ClassLoader classLoader;

	public ClassloaderStarter(@NonNull ClassLoader classLoader)
	{
		this.classLoader = classLoader;
	}

	@SneakyThrows
	public void start(Class<?> targetClass, String... args)
	{
		start(targetClass.getName(), args);
	}
	
	@SneakyThrows
	public void start(String className, String... args)
	{
		var thread = Thread.currentThread();
		thread.setContextClassLoader(classLoader);
		
		var annotationByClassloader = (Class<? extends Annotation>) classLoader.loadClass(Start.class.getName());
		var targetClass = classLoader.loadClass(className);

		var methodFilters = reflectionsUtils.methods().filters();

		var methods = reflectionsUtils.methods().stream(targetClass)
			.filter(methodFilters.mod().statics())
			.filter(methodFilters.annotation().assignable(annotationByClassloader))
			.filter(methodFilters.params().canAcceptTypes(String[].class))
		;
		
		var size = methods.count();
		
		if (size != 1)
			ExceptionsUtils.ThrowToCaller(NoSuchMethodException.class, "Class", className, "must contains only one public static method with List<String> params and annotated with", annotationByClassloader.getName(), "annotation. Now methods count:", size);
		
		var invocationResult = methods
				.map((method) -> reflectionsUtils.methods()
						.invoke(null, method, (Object[]) args))
						.findFirst()
						.get()
		;
		
		invocationResult
			.ifException((e) -> exceptionsHandler.handle(e, "Error while stating class", className, "by classloader", classLoader));
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface Start {}
	
}
