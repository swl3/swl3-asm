package ru.swayfarer.swl3.asm.classloader;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.classloader.dumps.DumpsClassloadingHandler;
import ru.swayfarer.swl3.asm.classloader.filtering.FilteringFun;
import ru.swayfarer.swl3.asm.classloader.location.ClassloaderLocationFun;
import ru.swayfarer.swl3.asm.classloader.protectiondomain.ProtectionDomainFun;
import ru.swayfarer.swl3.asm.classloader.source.ClassLoaderCodeSourceFun;
import ru.swayfarer.swl3.asm.classloader.source.JarCodeSourceFun;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.system.SystemUtils;

@SuppressWarnings("unchecked")
@Getter
@Setter
@Accessors(chain = true)
public class DynamicClassLoaderConfigurator {

	@NonNull
	public DynamicClassLoader classLoader;
	
	public SystemUtils systemUtils = SystemUtils.getInstance();

	public DynamicClassLoaderConfigurator(DynamicClassLoader classLoader)
	{
		this.classLoader = classLoader;
	}

	public DynamicClassLoaderConfigurator codeSource(IFunction1NoR<ClassLoadingEvent> fun)
	{
		classLoader.eventCodeSourceFind().subscribe().by(fun);
		return this;
	}
	
	public DynamicClassLoaderConfigurator filtering(IFunction1NoR<ClassLoadingEvent> fun)
	{
		classLoader.eventFiltering().subscribe().by(fun);
		return this;
	}
	
	public BytecodeTransformingConfigurator transforms()
	{
		var config = new BytecodeTransformingConfigurator();
        classLoader.eventClassLoading().subscribe().by(config.wrappedTransformer);
        return config;
	}
	
	public DumpsClassloadingHandler dumps()
	{
		var dumpsClassloadingHandler = new DumpsClassloadingHandler();
		classLoader.eventClassLoading().subscribe().by(dumpsClassloadingHandler, Integer.MAX_VALUE);
		return dumpsClassloadingHandler;
	}
	
	public FilteringFun filtering()
	{
		var fun = new FilteringFun();
		filtering(fun);
		return fun;
	}
	
	public DynamicClassLoaderConfigurator protectionDomain(IFunction1NoR<ClassLoadingEvent> fun)
	{
		classLoader.eventProtectionDomainFind().subscribe().by(fun);
		return this;
	}
	
	public DynamicClassLoaderConfigurator classLocation(IFunction1NoR<ClassLoadingEvent> fun)
	{
		classLoader.eventLocationFind().subscribe().by(fun);
		return this;
	}
	
	public DynamicClassLoaderConfigurator classLoading(IFunction1NoR<ClassLoadingEvent> fun)
	{
		classLoader.eventClassLoading().subscribe().by(fun);
		return this;
	}
	
	public DynamicClassLoaderConfigurator enableCurrentClassloaderCodeSource()
	{
		var loader = getClass().getClassLoader();
		var fun = new ClassLoaderCodeSourceFun().setClassLoaderFun((evt) -> loader);
		fun.getExceptionsHandler().andAfter(classLoader.getExceptionsHandler());
		return codeSource(fun);
	}
	
	public DynamicClassLoaderConfigurator enableParentClassloaderCodeSource()
	{
		var fun = new ClassLoaderCodeSourceFun().setClassLoaderFun((evt) -> evt.getClassLoader().getParent());
		fun.getExceptionsHandler().andAfter(classLoader.getExceptionsHandler());
		return codeSource(fun);
	}
	
	public DynamicClassLoaderConfigurator enableJarCodeSource()
	{
		var fun = new JarCodeSourceFun();
		fun.getExceptionsHandler().andAfter(classLoader.getExceptionsHandler());
		return codeSource(fun);
	}
	
	public DynamicClassLoaderConfigurator enableClassloaderLocation(@NonNull ClassLoader loader)
    {
        var fun = new ClassloaderLocationFun()
                .setClassloaderFun((evt) -> loader)
        ;
        
        fun.getExceptionsHandler().andAfter(classLoader.getExceptionsHandler());
        return classLocation(fun);
    }
	
	public DynamicClassLoaderConfigurator enableCurrentClassloaderLocation()
	{
		var loader = getClass().getClassLoader();
		
		var fun = new ClassloaderLocationFun()
				.setClassloaderFun((evt) -> loader)
		;
		
		fun.getExceptionsHandler().andAfter(classLoader.getExceptionsHandler());
		return classLocation(fun);
	}
	
	public DynamicClassLoaderConfigurator enableClassloaderLocation()
	{
		var fun = new ClassloaderLocationFun();
		fun.getExceptionsHandler().andAfter(classLoader.getExceptionsHandler());
		return classLocation(fun);
	}
	
	public DynamicClassLoaderSource source()
	{
	    return new DynamicClassLoaderSource(this);
	}
	
	public DynamicClassLoaderConfigurator enableParentClassloaderLocation()
	{
		var fun = new ClassloaderLocationFun()
				.setClassloaderFun((event) -> event.getClassLoader().getParent());
		
		fun.getExceptionsHandler().andAfter(classLoader.getExceptionsHandler());
		
		return classLocation(fun);
	}
	
	public DynamicClassLoaderConfigurator enableAutoProtectionDomains()
	{
		var fun = new ProtectionDomainFun();
		return protectionDomain(fun);
	}
	
	public DynamicClassLoaderConfigurator defaultConfiguration()
	{
		enableClassloaderLocation();
		enableCurrentClassloaderLocation();
		enableParentClassloaderLocation();
		
		enableJarCodeSource();
		
		enableCurrentClassloaderCodeSource();
		enableParentClassloaderCodeSource();
		
		enableAutoProtectionDomains();
		
		filtering()
			.rule()
				.nameMask("java.lang*")
					.deny();
		
		return this;
	}
	
	public <T extends DynamicClassLoader> T end()
	{
		return (T) classLoader;
	}
}
