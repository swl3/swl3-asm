package ru.swayfarer.swl3.asm.classloader;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.asm.transformer.BytecodeTransformingClassloadingHandler;
import ru.swayfarer.swl3.asm.transformer.IBytecodeTransformer;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.asm.visitor.InformatedClassVisitor;
import ru.swayfarer.swl3.asm.visitor.retry.RetryClassVisitor;
import ru.swayfarer.swl3.asm.visitor.standart.AccessOpenVisitor;
import ru.swayfarer.swl3.asm.visitor.standart.decorators.DecoratingSettings;
import ru.swayfarer.swl3.asm.visitor.standart.decorators.DecoratorClassVisitor;
import ru.swayfarer.swl3.asm.visitor.standart.decorators.MethodDecorationScanner;
import ru.swayfarer.swl3.asm.visitor.standart.decorators.MethodDecorationsFinder;
import ru.swayfarer.swl3.asm.visitor.standart.injections.InjectionsClassVisitor;
import ru.swayfarer.swl3.asm.visitor.standart.injections.InjectionsContainerScanner;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Getter
@Setter
@Accessors(chain = true)
public class BytecodeTransformingConfigurator {
	
	public AsmUtils asmUtils = new AsmUtils();
	public ExtendedList<Type> injectionTypes = CollectionsSWL.list();
	public BytecodeScanner bytecodeScanner = new BytecodeScanner();
	public BytecodeTransformingClassloadingHandler wrappedTransformer = new BytecodeTransformingClassloadingHandler();
	
	public InjectionsContainerScanner injectionsContainerScanner;
	
	public BytecodeTransformingConfigurator() {}
	
	public BytecodeTransformingConfigurator inject(String... typesNames)
	{
		return inject(ExtendedStream.of(typesNames)
				.map((str) -> "L" + str.replace(".", "/") + ";")
				.map(Type::getType)
				.toArray(Type.class))
		;
	}
	
	public BytecodeTransformingConfigurator inject(Class<?>... types)
	{
		return inject(ExtendedStream.of(types)
				.map(Type::getType)
				.toArray(Type.class))
		;
	}
	
	public BytecodeTransformingConfigurator inject(Type... types)
	{
		if (injectionTypes.isEmpty())
		{
			injectionsContainerScanner = new InjectionsContainerScanner();
			
			wrappedTransformer.configure()
				.setBytecodeScannerByDefault(bytecodeScanner)
				.transfomer((cv, info) -> new InjectionsClassVisitor(info, cv)
						.setInjections(injectionsContainerScanner.foundInjections)
				);
		}
		
		for (var type : types)
		{
			if (!injectionTypes.contains(type))
			{
				injectionTypes.add(type);
				var classInfo = bytecodeScanner.findOrCreateClassInfo(type);
				injectionsContainerScanner.scan(classInfo);
			}
		}
		
		return this;
	}
	
	public BytecodeTransformingConfigurator decorators()
	{
		return decorators(new DecoratingSettings());
	}
	
	@SafeVarargs
    public final BytecodeTransformingConfigurator transfomer(@NonNull IFunction2<ClassVisitor, ClassInfo, InformatedClassVisitor>... visitors)
    {
	    wrappedTransformer.configure()
            .setBytecodeScannerByDefault(bytecodeScanner)
            .transfomer(visitors)
        ;
        
        return this;
    }
	
	public BytecodeTransformingConfigurator transfomer(@NonNull IBytecodeTransformer... transformers)
    {
	    wrappedTransformer.configure()
            .setBytecodeScannerByDefault(bytecodeScanner)
            .transfomer(transformers)
        ;
	    
	    return this;
    }
	
    @SafeVarargs
    public final BytecodeTransformingConfigurator transfomer(@NonNull BytecodeScanner bytecodeScanner, @NonNull IFunction2<ClassVisitor, ClassInfo, InformatedClassVisitor>... visitors)
    {
        wrappedTransformer.configure()
            .setBytecodeScannerByDefault(bytecodeScanner)
            .transfomer(visitors)
        ;

        return this;
    }
	
	@SafeVarargs
    public final BytecodeTransformingConfigurator transfomer(@NonNull IFunction1<ClassVisitor, ClassVisitor>... visitors)
    {
	    wrappedTransformer.configure()
            .setBytecodeScannerByDefault(bytecodeScanner)
            .transfomer(visitors)
        ;
	    
	    return this;
    }
    
    public BytecodeTransformingConfigurator retry()
    {
        wrappedTransformer.configure()
            .setBytecodeScannerByDefault(bytecodeScanner)
            .transfomer((cv, info) -> {
                var ret = new RetryClassVisitor(cv, info);
                return ret;
            })
        ;
        return this;
    }
	
	public BytecodeTransformingConfigurator decorators(DecoratingSettings settings)
	{
		wrappedTransformer.configure()
			.setBytecodeScannerByDefault(bytecodeScanner)
			.transfomer((cv, info) -> {
				var ret = new DecoratorClassVisitor(
						cv, 
						info, 
						settings, 
						new MethodDecorationsFinder(settings, bytecodeScanner), 
						new MethodDecorationScanner(bytecodeScanner), 
						asmUtils
				);
				
				return ret;
			})
		;
		
		return this;
	}
	
	public BytecodeTransformingConfigurator accessOpen()
	{
		wrappedTransformer.configure()
			.setBytecodeScannerByDefault(bytecodeScanner)
			.transfomer(AccessOpenVisitor::new)
		;
		
		return this;
	}
}
