package ru.swayfarer.swl3.asm.classloader.protectiondomain;

import java.lang.reflect.ReflectPermission;
import java.security.AllPermission;
import java.security.CodeSource;
import java.security.Permissions;
import java.security.ProtectionDomain;

import ru.swayfarer.swl3.asm.classloader.ClassLoadingEvent;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

public class ProtectionDomainFun implements IFunction1NoR<ClassLoadingEvent> {

	@Override
	public void applyNoRUnsafe(ClassLoadingEvent event)
	{
		if (event.isProtectionDomainFound() || !event.isCodeSourceFound())
			return;
		
		event.setProtectionDomain(getDomain(event.getCodeSource()));
	}
	
	public ProtectionDomain getDomain(CodeSource source)
	{
		return new ProtectionDomain(source, getPermissions());
	}

	public Permissions getPermissions()
	{
		Permissions permissions = new Permissions();
		permissions.add(new AllPermission());
		permissions.add(new ReflectPermission("suppressAccessChecks"));
		return permissions;
	}
}
