package ru.swayfarer.swl3.asm.classloader;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.classloader.dumps.DumpsClassloadingHandler;

@Getter
@Setter
@Accessors(chain = true)
public class DynamicClassLoaderConfiguratorDumps{

	@NonNull
	public DynamicClassLoader classLoader;
	public DumpsClassloadingHandler dumpsClassloadingHandler = new DumpsClassloadingHandler();
	
	public DynamicClassLoaderConfiguratorDumps(DynamicClassLoader dynamicClassLoader)
	{
		this.classLoader = dynamicClassLoader;
	}
}
