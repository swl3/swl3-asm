package ru.swayfarer.swl3.asm.classloader.source;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.classloader.ClassLoadingEvent;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Getter
@Setter
@Accessors(chain = true)
public class ClassLoaderCodeSourceFun implements IFunction1NoR<ClassLoadingEvent> {

	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	public IFunction1<ClassLoadingEvent, ClassLoader> classLoaderFun = ClassLoadingEvent::getClassLoader;
	
	@Override
	public void applyNoRUnsafe(ClassLoadingEvent event)
	{
		if (event.isCodeSourceFound())
			return;
		
		var className = event.getClassName();
		
		try
		{
			var classLoader = classLoaderFun.apply(event);
			
			if (classLoader != null)
			{
				var protectionDomain = classLoader.loadClass(className).getProtectionDomain();
				event.setProtectionDomain(protectionDomain);
				
				if (protectionDomain != null)
				{
					var source = protectionDomain.getCodeSource();
					event.setCodeSource(source);
				}
			}
		}
		catch (Throwable e)
		{
			exceptionsHandler.handle(e, "Error while getting CodeSource of class", className, "by parent classloader");
		}
	}
}
