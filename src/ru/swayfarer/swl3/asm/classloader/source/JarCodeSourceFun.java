package ru.swayfarer.swl3.asm.classloader.source;

import java.net.JarURLConnection;
import java.security.CodeSource;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.classloader.ClassLoadingEvent;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Getter
@Setter
@Accessors(chain = true)
public class JarCodeSourceFun implements IFunction1NoR<ClassLoadingEvent> {

	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	
	@Override
	public void applyNoRUnsafe(ClassLoadingEvent event)
	{
		if (event.isCodeSourceFound())
			return;
		
		var res = event.getClassLocation();
		
		if (res != null)
		{
			if (res.getProtocol().equals("jar"))
			{
				try
				{
					JarURLConnection jarURLConnection = (JarURLConnection) res.openConnection();
					var url = jarURLConnection.getJarFileURL();
					var source = new CodeSource(url, jarURLConnection.getJarEntry().getCodeSigners());
					event.setCodeSource(source);
				}
				catch (Throwable e)
				{
					exceptionsHandler.handle(e, "Error while scanning class", event.getClassName(), " for code source");
				}
			}
		}
	}
}
