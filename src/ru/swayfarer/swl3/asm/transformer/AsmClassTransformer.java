package ru.swayfarer.swl3.asm.transformer;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassReader;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassWriter;

@Data @Accessors(chain = true) 
public abstract class AsmClassTransformer implements IBytecodeTransformer {

	public static int classWriterFlags = ClassWriter.COMPUTE_FRAMES;
	
	@Internal
	@NonNull 
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	
	@Override
	public void transform(@NonNull ClassBytecodeInfo classLoadingEvent)
	{
		var writer = getWriter(classLoadingEvent);
		var reader = getReader(classLoadingEvent);

		var cv = transform (reader, writer, classLoadingEvent);

		if (cv == null)
			cv = writer;

		reader.accept(cv, ClassReader.EXPAND_FRAMES);

		classLoadingEvent.setClassBytes(writer.toByteArray());
	}
	
	public abstract ClassVisitor transform(@NonNull ClassReader reader, @NonNull ClassWriter writer, @NonNull ClassBytecodeInfo event);
	
	@Internal
	public ClassWriter getWriter(@NonNull ClassBytecodeInfo event)
	{
		return new ClassWriter(classWriterFlags);
	}
	
	@Internal
	public ClassReader getReader(@NonNull ClassBytecodeInfo event)
	{
		return new ClassReader(event.getClassBytes());
	}
}
