package ru.swayfarer.swl3.asm.transformer;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassReader;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassWriter;

@Data @Accessors(chain = true) @EqualsAndHashCode(callSuper = false)
public abstract class InformatedAsmClassTransformer extends AsmClassTransformer {

	@Internal
	@NonNull
	public BytecodeScanner bytecodeScanner;

	@Internal
	public ILogger logger = LogFactory.getLogger();
	
	@Override
	public ClassVisitor transform(ClassReader reader, ClassWriter writer, ClassBytecodeInfo event)
	{
		var classInfo = findClassInfo(event);

		if (classInfo == null)
		{
			logger.warn("Can't find class with name", event.getClassType().getClassName(), "Skipping transform...");
			return writer;
		}

		return transformInformated(bytecodeScanner.findOrCreateClassInfo(event.getClassType()), reader, writer, event);
	}

	public ClassInfo findClassInfo(ClassBytecodeInfo bytecodeInfo)
	{
		return bytecodeScanner.findOrCreateClassInfo(bytecodeInfo.getClassType());
	}
	
	public abstract ClassVisitor transformInformated(ClassInfo classInfo, ClassReader reader, ClassWriter writer, ClassBytecodeInfo event);
}
