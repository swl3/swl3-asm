package ru.swayfarer.swl3.asm.transformer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ClassBytecodeInfo {

    @NonNull
    public boolean changed;

    @NonNull
    public String className;
    
    @NonNull
    public byte[] classBytes;
    
    public Type getClassType()
    {
        return Type.getType("L" + className.replace(".", "/") + ";");
    }

    public ClassBytecodeInfo setClassName(String className)
    {
        changed = true;
        this.className = className;
        return this;
    }

    public ClassBytecodeInfo setClassBytes(byte[] classBytes)
    {
        changed = true;
        this.classBytes = classBytes;
        return this;
    }
}
