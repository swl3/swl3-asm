package ru.swayfarer.swl3.asm.transformer;

import lombok.NonNull;

public interface IBytecodeTransformer {

	public void transform(@NonNull ClassBytecodeInfo classLoadingEvent);
	
}
