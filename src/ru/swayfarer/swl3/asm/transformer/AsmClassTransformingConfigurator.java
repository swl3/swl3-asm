package ru.swayfarer.swl3.asm.transformer;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.asm.visitor.InformatedClassVisitor;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassReader;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassWriter;

@Data @Accessors(chain = true)
public class AsmClassTransformingConfigurator {

	@NonNull
	public BytecodeTransformingClassloadingHandler handler;
	
	public BytecodeScanner bytecodeScannerByDefault;
	
	public AsmClassTransformingConfigurator transfomer(@NonNull IBytecodeTransformer... transformers)
	{
		for (var transformer : transformers)
			handler.transformer(transformer);
		
		return this;
	}
	
	@SafeVarargs
	public final AsmClassTransformingConfigurator transfomer(@NonNull IFunction2<ClassVisitor, ClassInfo, InformatedClassVisitor>... visitors)
	{
		if (bytecodeScannerByDefault == null)
			ExceptionsUtils.ThrowToCaller(IllegalStateException.class, "Bytecode scanner by default must not be null for do it!");
		
		return transfomer(bytecodeScannerByDefault, visitors);
	}
	
	@SafeVarargs
	public final AsmClassTransformingConfigurator transfomer(@NonNull BytecodeScanner bytecodeScanner, @NonNull IFunction2<ClassVisitor, ClassInfo, InformatedClassVisitor>... visitors)
	{
		var transformer = new InformatedAsmClassTransformer(bytecodeScanner)
		{
			@Override
			public ClassVisitor transformInformated(ClassInfo classInfo, ClassReader reader, ClassWriter writer, ClassBytecodeInfo event)
			{
				ClassVisitor cv = writer;
				
				for (var visitorFun : visitors)
				{
					cv = visitorFun.apply(cv, classInfo);
				}
				
				return cv;
			}
		};
		
		handler.transformer(transformer);
		
		return this;
	}
	
	@SafeVarargs
	public final AsmClassTransformingConfigurator transfomer(@NonNull IFunction1<ClassVisitor, ClassVisitor>... visitors)
	{
		var transformer = new AsmClassTransformer()
		{
			@Override
			public ClassVisitor transform(@NonNull ClassReader reader, @NonNull ClassWriter writer, @NonNull ClassBytecodeInfo event)
			{
				ClassVisitor cv = writer;
				
				for (var visitorFun : visitors)
				{
					cv = visitorFun.apply(cv);
				}
				
				return cv;
			}
		};
		
		handler.transformer(transformer);
		
		return this;
	}
}
