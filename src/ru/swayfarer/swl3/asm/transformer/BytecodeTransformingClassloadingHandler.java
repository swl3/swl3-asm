package ru.swayfarer.swl3.asm.transformer;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.classloader.ClassLoadingEvent;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@SuppressWarnings("unchecked")
@Data @Accessors(chain = true)
public class BytecodeTransformingClassloadingHandler implements IFunction1NoR<ClassLoadingEvent> {

	@NonNull
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	
	@NonNull
	public ExtendedList<IBytecodeTransformer> transformers = CollectionsSWL.list();
	
	@Override
	public void applyNoRUnsafe(ClassLoadingEvent event)
	{
	    var classBytecodeInfo = createClassBytecodeInfo(event);
        transform(classBytecodeInfo);
        writeBytecodeInfo(event, classBytecodeInfo);
	}
	
    public void transform(ClassBytecodeInfo classBytecodeInfo)
    {
        for (var transformer : transformers)
        {
            exceptionsHandler.safe(() -> {
                transformer.transform(classBytecodeInfo);
            }, "Error while transforming class", classBytecodeInfo.getClassName(), "by", transformer);
        }
    }
    
    public void writeBytecodeInfo(@NonNull ClassLoadingEvent classLoadingEvent, ClassBytecodeInfo classBytecodeInfo)
    {
    	classLoadingEvent
    		.setClassBytes(classBytecodeInfo.getClassBytes())
    		.setClassName(classBytecodeInfo.getClassName())
		;
    }
	
	public ClassBytecodeInfo createClassBytecodeInfo(@NonNull ClassLoadingEvent classLoadingEvent)
	{
	    return ClassBytecodeInfo.builder()
	            .classBytes(classLoadingEvent.getClassBytes())
	            .className(classLoadingEvent.getClassName())
	            .build()
        ;
	}
	
	public <T extends BytecodeTransformingClassloadingHandler> T transformer(IBytecodeTransformer transformer)
	{
		this.transformers.add(transformer);
		return (T) this;
	}
	
	public AsmClassTransformingConfigurator configure()
	{
		return new AsmClassTransformingConfigurator(this);
	}
}
