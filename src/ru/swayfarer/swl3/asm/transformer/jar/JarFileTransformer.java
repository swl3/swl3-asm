package ru.swayfarer.swl3.asm.transformer.jar;

import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.var;
import ru.swayfarer.swl3.asm.classloader.BytecodeTransformingConfigurator;
import ru.swayfarer.swl3.asm.classloader.DynamicClassLoader;
import ru.swayfarer.swl3.asm.classloader.DynamicClassLoaderSource;
import ru.swayfarer.swl3.asm.transformer.BytecodeTransformingClassloadingHandler;
import ru.swayfarer.swl3.asm.transformer.ClassBytecodeInfo;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.streams.BytesInStream;
import ru.swayfarer.swl3.io.streams.StreamsUtils;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringUtils;

public class JarFileTransformer {
    
	public IFunction1<FileSWL, FileSWL> resultFileFun = (f) -> FileSWL.of("result/" + f.getName());
	
    public BytecodeTransformingClassloadingHandler transformingHandler = new BytecodeTransformingClassloadingHandler();
    public DynamicClassLoader classLoader = DynamicClassLoader.defaultSettings();

    public JarFileTransformer processJar(@NonNull String file)
    {
        return processJar(FileSWL.of(file));
    }

    public JarFileTransformer processAllJars(@NonNull String file)
    {
        return processAllJars(FileSWL.of(file));
    }
    
    @Internal
    @SneakyThrows
    public JarFileTransformer processAllJars(@NonNull FileSWL file)
    {
        file.findAllSubfiles()
            .filter((f) -> f.isFile() && f.getExtension().equals("jar"))
            .each((f) -> processJar(f))
        ;
        
        return this;
    }
    
    @Internal
    @SneakyThrows
    public JarFileTransformer processJar(@NonNull FileSWL file)
    {
        source().file(file);
        var resultFile = resultFileFun.apply(file);
        
        var zipFile = new JarFile(file);
        
        var zos = new ZipOutputStream(resultFile.out());
        
        for (var zipEntry : CollectionsSWL.iterable(zipFile.entries()))
        {
            var entryName = zipEntry.getName();
            var entryBytes = StreamsUtils.copy().toBytes(zipFile.getInputStream(zipEntry));
            
            if (entryName.endsWith(".class"))
            {
                var className = StringUtils.subString(
                        0, 
                        -6, 
                        entryName.replace("/", ".")
                );
                
                var classBytecodeInfo = ClassBytecodeInfo.builder()
                        .classBytes(entryBytes)
                        .className(className)
                        .build()
                ;
                
                transformingHandler.transform(classBytecodeInfo);
                
                entryBytes = classBytecodeInfo.getClassBytes();
                entryName = classBytecodeInfo.getClassName().replace(".", "/").concat(".class");
            }
            
            zos.putNextEntry(new ZipEntry(entryName));
            StreamsUtils.copyStream(BytesInStream.of(entryBytes), zos, true, false);
            zos.closeEntry();
        }
        
        zos.close();
        zipFile.close();
        
        return this;
    }
    
    public BytecodeTransformingConfigurator transforms()
    {
        return new BytecodeTransformingConfigurator()
                .setWrappedTransformer(transformingHandler)
        ; 
    }
    
    public DynamicClassLoaderSource source()
    {
        return classLoader.configure().source();
    }
}
