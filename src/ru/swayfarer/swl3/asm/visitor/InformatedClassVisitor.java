package ru.swayfarer.swl3.asm.visitor;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.FieldInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.FieldVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;

@Data @Accessors(chain = true) @EqualsAndHashCode(callSuper = true)
@SuppressWarnings("unchecked")
public class InformatedClassVisitor extends ClassVisitor {

	@NonNull
	@Internal
	public Map<String, FieldInfo> fields = new HashMap<>();
	
	@NonNull
	@Internal
	public Map<String, MethodInfo> methods = new HashMap<>();
	
	@Internal
	public ClassInfo classInfo;
	
	public InformatedClassVisitor(ClassVisitor classVisitor, @NonNull ClassInfo classInfo)
	{
		super(classVisitor);
		loadClassInfo(classInfo);
	}

	public InformatedClassVisitor(int api, ClassVisitor classVisitor, @NonNull ClassInfo classInfo)
	{
		super(api, classVisitor);
		loadClassInfo(classInfo);
	}

	public InformatedClassVisitor(int api, @NonNull ClassInfo classInfo)
	{
		super(api);
		loadClassInfo(classInfo);
	}
	
	@Override
	@Internal
	@Deprecated
	public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions)
	{
		return visitMethodInformated(methods.get(getMethodKey(name, descriptor)), access, name, descriptor, signature, exceptions);
	}
	
	public MethodVisitor visitMethodInformated(MethodInfo methodInfo, int access, String name, String descriptor, String signature, String[] exceptions)
	{
		return super.visitMethod(access, name, descriptor, signature, exceptions);
	}
	
	@Override
	@Internal
	@Deprecated
	public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value)
	{
		return visitFieldInformated(fields.get(name), access, name, descriptor, signature, value);
	}
	
	public FieldVisitor visitFieldInformated(FieldInfo fieldInfo, int access, String name, String descriptor, String signature, Object value)
	{
		return super.visitField(access, name, descriptor, signature, value);
	}
	
	@Internal
	public <T extends InformatedClassVisitor> T loadClassInfo(@NonNull ClassInfo classInfo)
	{
		this.classInfo = classInfo;
		fields.clear();
		methods.clear();
		
		for (var field : classInfo.getFields())
		{
			fields.put(field.getName(), field);
		}
		
		for (var method : classInfo.getMethods())
		{
			methods.put(getMethodKey(method), method);
		}
		
		return (T) this;
	}
	
	@Internal
	public String getMethodKey(@NonNull MethodInfo methodInfo)
	{
		return getMethodKey(methodInfo.getName(), methodInfo.getDescriptor());
	}
	
	@Internal
	public String getMethodKey(@NonNull String name, @NonNull String desc)
	{
		return name + "::" + desc;
	}
}
