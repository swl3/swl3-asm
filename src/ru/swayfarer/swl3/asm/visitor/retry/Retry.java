package ru.swayfarer.swl3.asm.visitor.retry;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

@Retention(RUNTIME)
public @interface Retry {
    
    public long retryCount() default 0;
    public long delay() default 0;
    
    @Retention(RUNTIME)
    public static @interface Catch {
        public Class<?>[] include() default {};
        public Class<?>[] exclude() default {};
    }
}
