package ru.swayfarer.swl3.asm.visitor.retry;

import java.io.IOException;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Label;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.commons.AdviceAdapter;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class RetryMethodVisitor extends AdviceAdapter{

    public static long defaultRetryDelay = 1000;
    public static long defaultMaxAttempts = 5;
    
    @NonNull
    public MethodInfo methodInfo;
    
    public int maxAttemptsVarId;
    public int lastExceptionVarId;
    public int lastAttemptVarId;
    public int attemptVarId;
    public int catchedThrowableVarId;
    public int catchedThrowableClassVarId;
    
    public Label lblStartTry;
    public Label lblEndTry;
    public Label lblCatch;
    public Label lblAfterTry;
    
    public long retryDelay = defaultRetryDelay;
    public long maxAttempts = defaultMaxAttempts;
    
    public ExtendedList<Type> inclusionTypes = CollectionsSWL.list();
    
    public ExtendedList<Type> exclusionTypes = CollectionsSWL.list();
    
    public RetryMethodVisitor(MethodInfo methodInfo, MethodVisitor methodVisitor, int access, String name, String descriptor)
    {
        super(ASM8_EXPERIMENTAL, methodVisitor, access, name, descriptor);
    }
    
    public static String helloWorld() throws Throwable
    {
        Throwable th = null;
        long maxAttemts = 5;
        long last = maxAttemts - 1;
        
        for (long attemt = 0; attemt < maxAttemts; attemt++)
        {
            try
            {
                System.out.println("Hello, World!");
                return "Hello";
            }
            catch (Throwable e)
            {
                th = e;
                
                Class<?> classOfE = e.getClass();
                
                if (IOException.class.isAssignableFrom(classOfE))
                    System.out.println(12);
                
                if (attemt != last)
                    Thread.sleep(5000l);
            }
        }
        
        throw th;
    }
    
    @Override
    public void visitCode()
    {
        maxAttemptsVarId = newLocal(Type.LONG_TYPE);
        lastAttemptVarId = newLocal(Type.LONG_TYPE);
        lastExceptionVarId = newLocal(AsmUtils.THROWABLE_TYPE);
        attemptVarId = newLocal(Type.LONG_TYPE);
        catchedThrowableVarId = newLocal(AsmUtils.THROWABLE_TYPE);
        
        lblStartTry = new Label();
        lblEndTry = new Label();
        lblCatch = new Label();
        
        visitInsn(ACONST_NULL);
        visitVarInsn(ASTORE, lastExceptionVarId);
        
        visitTryCatchBlock(lblStartTry, lblEndTry, lblCatch, "java/lang/Throwable");
        visitLdcInsn(maxAttempts);
        visitVarInsn(LSTORE, maxAttemptsVarId);
        visitVarInsn(LLOAD, maxAttemptsVarId);
        visitInsn(LCONST_1);
        visitInsn(LSUB);
        visitVarInsn(LSTORE, lastAttemptVarId);
        visitInsn(LCONST_0);
        visitVarInsn(LSTORE, attemptVarId);
        lblAfterTry = new Label();
        visitJumpInsn(GOTO, lblAfterTry);
        visitLabel(lblStartTry);
        
        super.visitCode();
    }
    
    public void validateCatchedException(boolean isInclusions, ExtendedList<Type> types)
    {
        if (!CollectionsSWL.isNullOrEmpty(types))
        {
            var lblThrow = newLabel();
            catchedThrowableClassVarId = newLocal(AsmUtils.CLASS_TYPE);
            
            visitVarInsn(ALOAD, catchedThrowableVarId);
            visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;", false);
            visitVarInsn(ASTORE, catchedThrowableClassVarId);
            
            for (Type type : types) 
            {
                visitLdcInsn(type);
                visitVarInsn(ALOAD, catchedThrowableClassVarId);
                visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "isAssignableFrom", "(Ljava/lang/Class;)Z", false);
                visitJumpInsn(isInclusions ? IFNE : IFEQ, lblThrow);
            }
            
            visitVarInsn(ALOAD, catchedThrowableVarId);
            visitInsn(ATHROW);
            
            visitLabel(lblThrow);
        }
    }
    
    @Override
    public void visitMaxs(int maxStack, int maxLocals)
    {
        visitLabel(lblEndTry);
        visitLabel(lblCatch);
        visitInsn(DUP);
        visitVarInsn(ASTORE, catchedThrowableVarId);
        visitVarInsn(ASTORE, lastExceptionVarId);
        
        validateCatchedException(true, inclusionTypes);
        validateCatchedException(false, exclusionTypes);
        
        visitVarInsn(LLOAD, attemptVarId);
        visitVarInsn(LLOAD, lastAttemptVarId);
        visitInsn(LCMP);
        Label label11 = new Label();
        visitJumpInsn(IFEQ, label11);
        visitLdcInsn(retryDelay);
        visitMethodInsn(INVOKESTATIC, "java/lang/Thread", "sleep", "(J)V", false);
        visitLabel(label11);
        
        visitVarInsn(LLOAD, attemptVarId);
        visitInsn(LCONST_1);
        visitInsn(LADD);
        visitVarInsn(LSTORE, attemptVarId);
        visitLabel(lblAfterTry);
        visitVarInsn(LLOAD, attemptVarId);
        visitVarInsn(LLOAD, maxAttemptsVarId);
        visitInsn(LCMP);
        visitJumpInsn(IFLT, lblStartTry);
        visitVarInsn(ALOAD, lastExceptionVarId);
        visitInsn(ATHROW);
        
        
        super.visitMaxs(maxStack, maxLocals);
    }
}
