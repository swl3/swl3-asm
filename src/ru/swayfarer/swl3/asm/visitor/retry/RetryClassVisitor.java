package ru.swayfarer.swl3.asm.visitor.retry;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.visitor.InformatedClassVisitor;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class RetryClassVisitor extends InformatedClassVisitor {

    public RetrySettings retrySettings = new RetrySettings();
    
    public RetryClassVisitor(ClassVisitor classVisitor, @NonNull ClassInfo classInfo)
    {
        super(classVisitor, classInfo);
    }

    @Override
    public MethodVisitor visitMethodInformated(MethodInfo methodInfo, int access, String name, String descriptor, String signature, String[] exceptions)
    {
        var mv = super.visitMethodInformated(methodInfo, access, name, descriptor, signature, exceptions);

        var retryAnnotationSettings = retrySettings.getRetryAnnotationSettings();
        var retryAnnotationType = retryAnnotationSettings.getType();
        
        if (methodInfo.getAnnotationsInfo().hasAnnotation(retryAnnotationType)) 
        {
            var retryMv = new RetryMethodVisitor(methodInfo, mv, access, name, descriptor);
            mv = retryMv;
            configureMethodVisitor(methodInfo, retryMv);
        }
        
        return mv;
    }
    
    public void configureMethodVisitor(MethodInfo methodInfo, RetryMethodVisitor retryMv) 
    {
        var retryAnnotationSettings = retrySettings.getRetryAnnotationSettings();
        var retryAnnotationType = retryAnnotationSettings.getType();
        var retryAnnotation = methodInfo.getAnnotationsInfo().first(retryAnnotationType);
        
        var delay = retryAnnotation.getParam(retryAnnotationSettings.getDelayParamName(), Long.class);
        var maxAttempts = retryAnnotation.getParam(retryAnnotationSettings.getMaxAttemtsParamName(), Long.class);
    
        if (delay != null) 
            retryMv.setRetryDelay(delay);
        
        if (maxAttempts != null)
            retryMv.setMaxAttempts(maxAttempts);
        
        var catchAnnotationSettings = retrySettings.getCatchAnnotationSettings();
        var catchAnnotationType = catchAnnotationSettings.getType();
        
        var catchAnnotation = methodInfo.getAnnotationsInfo().first(catchAnnotationType);
        
        if (catchAnnotation != null)
        {
            var inclusions = catchAnnotation.getParams(catchAnnotationSettings.getInclusionsParamName(), Type.class);
            var exclusions = catchAnnotation.getParams(catchAnnotationSettings.getExclusionsParamName(), Type.class);
            
            if (!CollectionsSWL.isNullOrEmpty(inclusions))
                retryMv.setInclusionTypes(inclusions);

            if (!CollectionsSWL.isNullOrEmpty(exclusions))
                retryMv.setExclusionTypes(exclusions);
        }
    }
}
