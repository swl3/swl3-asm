package ru.swayfarer.swl3.asm.visitor.retry;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.visitor.retry.Retry.Catch;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data
@NoArgsConstructor @AllArgsConstructor
@Accessors(chain = true)
public class RetrySettings {
    
    @NonNull
    public RetryAnnotationSettings retryAnnotationSettings = new RetryAnnotationSettings();
    
    public CatchAnnotationSettings catchAnnotationSettings = new CatchAnnotationSettings();
    
    @Data
    @NoArgsConstructor @AllArgsConstructor
    @Accessors(chain = true)
    public static class CatchAnnotationSettings {
        
        @NonNull
        public String inclusionsParamName = "include";
        @NonNull
        public String exclusionsParamName = "exclude";
        
        @NonNull
        public Type type = Type.getType(Catch.class);
    }
    
    @Data
    @NoArgsConstructor @AllArgsConstructor
    @Accessors(chain = true)
    public static class RetryAnnotationSettings {
        @NonNull
        public String delayParamName = "delay";
        
        @NonNull
        public String maxAttemtsParamName = "retryCount";
        
        @NonNull
        public Type type = Type.getType(Retry.class);
    }
}
