package ru.swayfarer.swl3.asm.visitor.standart.decorators;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;

@Data @Accessors(chain = true)
public class MethodDecoration {

	public ExtendedList<MethodInfo> onEnterMethods = CollectionsSWL.list();
	public ExtendedList<MethodInfo> onExitMethods = CollectionsSWL.list();
	public ExtendedList<MethodInfo> onThrowMethods = CollectionsSWL.list();
	
}
