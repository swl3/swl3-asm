package ru.swayfarer.swl3.asm.visitor.standart.decorators;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.visitor.standart.decorators.annotations.DecorateBy;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true)
public class DecoratingSettings {

	public boolean useAssignableAnnotations = true;
	public Type decorateByAnnotationType = Type.getType(DecorateBy.class);
}
