package ru.swayfarer.swl3.asm.visitor.standart.decorators;

import lombok.NonNull;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.asm.transformer.ClassBytecodeInfo;
import ru.swayfarer.swl3.asm.transformer.InformatedAsmClassTransformer;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassReader;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassWriter;

public class DecorationClassTransformer extends InformatedAsmClassTransformer {

	public MethodDecorationsFinder decorationsFinder;
	public MethodDecorationScanner decorationScanner;
	
	public DecorationClassTransformer(@NonNull DecoratingSettings settings, @NonNull BytecodeScanner bytecodeScanner)
	{
		super(bytecodeScanner);
		decorationsFinder = new MethodDecorationsFinder(settings, bytecodeScanner);
		decorationScanner = new MethodDecorationScanner(bytecodeScanner);
	}

	@Override
	public ClassVisitor transformInformated(ClassInfo classInfo, ClassReader reader, ClassWriter writer, ClassBytecodeInfo event)
	{
		return null;
	}

}
