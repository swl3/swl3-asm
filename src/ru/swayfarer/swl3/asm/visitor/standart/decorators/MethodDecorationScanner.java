package ru.swayfarer.swl3.asm.visitor.standart.decorators;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.info.VariableInfo;
import ru.swayfarer.swl3.asm.info.stream.MethodInfoStream;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.asm.visitor.standart.decorators.annotations.OnEnter;
import ru.swayfarer.swl3.asm.visitor.standart.decorators.annotations.OnExit;
import ru.swayfarer.swl3.asm.visitor.standart.decorators.annotations.OnThrow;
import ru.swayfarer.swl3.asm.visitor.standart.decorators.annotations.UseArgs;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true)
public class MethodDecorationScanner {
	
	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	public MethodDecoration scan(@NonNull MethodInfo methodToDecorate, @NonNull ClassInfo classInfo)
	{
		var methods = classInfo.methodsStream()
			.access().statics()
			.access().publics()
			.returns().is(void.class)
			.annotations().has(OnEnter.class, OnExit.class, OnThrow.class)
			.toExList()
		;
		
		var ret = new MethodDecoration();
		
		var methodToDecorateParamTypes = methodToDecorate.getParams().map(VariableInfo::getParamType);
		
		new MethodInfoStream(classInfo.getBytecodeScanner(), () -> methods.stream());
		
		IFunction1<MethodInfo, Boolean> decoratorMethodParamsPreFilter = (method) -> {

			var useArgs = useArgs(method);
			var methodParams = method.getParams().map(VariableInfo::getParamType);
			
			if (useArgs && methodToDecorate.getParamsCount() > method.getParamsCount() - 1)
				return false;
			
			var firstParam = methodParams.first();
			
			if (methodToDecorate.getAccess().isStatic())
			{
				if (firstParam == null || !(AsmUtils.CLASS_TYPE.equals(firstParam) || AsmUtils.OBJECT_TYPE.equals(firstParam)))
					return false;
			}
			else
			{
				if (firstParam == null || !methodToDecorate.getOwner().isAssignable(firstParam))
					return false;
			}
			
			methodParams.removeFirst();
			
			return !useArgs || CollectionsSWL.isStartsWith(methodParams, methodToDecorateParamTypes);
		};
		
		IFunction1<MethodInfo, Boolean> enterFilter = (method) -> {
			
			if (!decoratorMethodParamsPreFilter.apply(method))
				return false;
			
			if (useArgs(method))
			{
				if (methodToDecorateParamTypes.size() != method.getParamsCount() - 1)
				{
					return false;
				}
			}
			else
			{
				if (method.getParamsCount() != 1)
				{
					return false;
				}
			}
			
			return true;
		};
		
		IFunction1<MethodInfo, Boolean> exitFilter = (method) -> {
			
			if (!decoratorMethodParamsPreFilter.apply(method))
				return false;
			
			System.out.println(123);
			
			var isVoid = Type.VOID_TYPE.equals(methodToDecorate.getReturnType());
			
			if (useArgs(method))
			{
				if (methodToDecorate.getParamsCount() != method.getParamsCount() - (isVoid ? 1 : 2))
					return false;
			}
			else
			{
				if (method.getParamsCount() != (isVoid ? 1 : 2))
					return false;
			}
			
			var methodParams = method.getParams().map(VariableInfo::getParamType);
			var lastParam = methodParams.last();
			
			if (!isVoid && (lastParam == null || !lastParam.equals(methodToDecorate.getReturnType())))
				return false;
			
			return true;
		};
		
		IFunction1<MethodInfo, Boolean> throwsFilter = (method) -> {
			
			if (!decoratorMethodParamsPreFilter.apply(method))
				return false;
			
			if (useArgs(method))
			{
				if (methodToDecorate.getParamsCount() != method.getParamsCount() - 2)
					return false;
			}
			else
			{
				if (method.getParamsCount() != 2)
					return false;
			}
			
			var methodParams = method.getParams().map(VariableInfo::getParamType);
			var lastParam = methodParams.last();
			
			
			if (lastParam == null || !AsmUtils.THROWABLE_TYPE.equals(lastParam))
				return false;
			
			return true;
		};
		
		asMethodsStream(classInfo, methods)
			.annotations().has(OnExit.class)
			.filter(exitFilter)
			.each(ret.getOnExitMethods()::add)
		;
		
		asMethodsStream(classInfo, methods)
			.annotations().has(OnEnter.class)
			.filter(enterFilter)
			.each(ret.getOnEnterMethods()::add)
		;
		
		asMethodsStream(classInfo, methods)
			.annotations().has(OnThrow.class)
			.filter(throwsFilter)
			.each(ret.getOnThrowMethods()::add)
		;
		
		return ret;
	}
	
	protected boolean useArgs(MethodInfo method)
	{
		return method.getAnnotationsInfo().hasAssignable(UseArgs.class);
	}
	
	protected MethodInfoStream asMethodsStream(ClassInfo classInfo, ExtendedList<MethodInfo> methods)
	{
		return new MethodInfoStream(classInfo.getBytecodeScanner(), () -> methods.stream());
	}
}
