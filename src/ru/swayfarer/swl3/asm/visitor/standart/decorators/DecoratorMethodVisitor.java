package ru.swayfarer.swl3.asm.visitor.standart.decorators;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.asm.visitor.standart.decorators.annotations.UseArgs;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.commons.AdviceAdapter;

@Data @Accessors(chain = true) @EqualsAndHashCode(callSuper = true)
public class DecoratorMethodVisitor extends AdviceAdapter {

	@NonNull
	public ExtendedList<MethodDecoration> decorations;
	
	@NonNull
	public MethodInfo methodInfo;
	
	@NonNull
	public AsmUtils asmUtils;
	
	public DecoratorMethodVisitor(AsmUtils asmUtils, ExtendedList<MethodDecoration> decorations, MethodInfo methodInfo, MethodVisitor methodVisitor, int access, String name, String descriptor)
	{
		super(ASM8_EXPERIMENTAL, methodVisitor, access, name, descriptor);
		this.methodInfo = methodInfo;
		this.decorations = decorations;
		this.asmUtils = asmUtils;
	}
	
	@Override
	protected void onMethodEnter()
	{
		appendEnter();
		super.onMethodEnter();
	}
	
	@Override
	protected void onMethodExit(int opcode)
	{
		if (opcode == ATHROW)
			appendThrow();
		else
			appendExit();
		
		super.onMethodExit(opcode);
	}
	
	public void appendEnter()
	{
		for (var decoration : decorations)
		{
			var methods = decoration.getOnEnterMethods();
			
			for (var method : methods)
			{
				loadBasicParams(method.getAnnotationsInfo().hasAssignable(UseArgs.class));
				asmUtils.methods().invokeStatic(this, method);
			}
		}
	}
	
	public void appendExit()
	{
		var returnType = methodInfo.getReturnType();
		boolean isVoid = Type.VOID_TYPE.equals(returnType);
		
		int retVarId = -1 ;
		
		if (!isVoid)
		{
			retVarId = newLocal(returnType);
			storeLocal(retVarId);
		}
		
		for (var decoration : decorations.copy().reverse())
		{
			var methods = decoration.getOnExitMethods();
			
			for (var method : methods)
			{
				loadBasicParams(method.getAnnotationsInfo().hasAssignable(UseArgs.class));
				
				if (!isVoid)
					loadLocal(retVarId);
				
				asmUtils.methods().invokeStatic(this, method);
			}
		}
		
		if (!isVoid)
			loadLocal(retVarId);
	}
	
	public void appendThrow()
	{
		int throwableVarId = newLocal(Type.getType(Throwable.class));
		storeLocal(throwableVarId);
		
		for (var decoration : decorations.copy().reverse())
		{
			var methods = decoration.getOnThrowMethods();
			
			for (var method : methods)
			{
				loadBasicParams(method.getAnnotationsInfo().hasAssignable(UseArgs.class));
				loadLocal(throwableVarId);
				asmUtils.methods().invokeStatic(this, method);
			}
		}
		
		loadLocal(throwableVarId);
	}
	
	public void loadBasicParams(boolean useParams)
	{
		if (methodInfo.getAccess().isStatic())
			visitLdcInsn(methodInfo.getOwner().getClassType());
		else
			loadThis();
		
		
		if (useParams)
			loadArgs();
	}
}
