package ru.swayfarer.swl3.asm.visitor.standart.decorators;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.AnnotationInfo;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true)
public class MethodDecorationsFinder {

	@NonNull
	public DecoratingSettings settings;
	
	@NonNull
	public BytecodeScanner bytecodeScanner;
	
	public ExtendedList<ClassInfo> findDecoratorsFor(@NonNull MethodInfo methodInfo)
	{
		var decorateByAnnotations = findDecorateAnnotation(methodInfo);
		var ret = new ExtendedList<ClassInfo>();
		
		if (!CollectionsSWL.isNullOrEmpty(decorateByAnnotations))
		{
			var typesList = new ExtendedList<Type>();
			
			for (var decorateByAnnotation : decorateByAnnotations)
			{
				var type = decorateByAnnotation.getAnnotationType();
				
				if (typesList.contains(type))
					continue;
				
				typesList.add(type);
				ret.add(bytecodeScanner.findOrCreateClassInfo(decorateByAnnotation.getParam("value", Type.class)));
			}
		}
		
		return ret;
	}
	
	public ExtendedList<AnnotationInfo> findDecorateAnnotation(@NonNull MethodInfo methodInfo)
	{
		var annotations = methodInfo.getAnnotationsInfo();
		var type = settings.getDecorateByAnnotationType();
		return settings.isUseAssignableAnnotations() ? annotations.assignables(type) : annotations.all(type);
	}
}
