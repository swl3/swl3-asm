package ru.swayfarer.swl3.asm.visitor.standart.decorators.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

@Retention(RUNTIME)
public @interface OnThrow
{

}
