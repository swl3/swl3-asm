package ru.swayfarer.swl3.asm.visitor.standart.decorators;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.asm.visitor.InformatedClassVisitor;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;

@Data @Accessors(chain = true) @EqualsAndHashCode(callSuper = true)
public class DecoratorClassVisitor extends InformatedClassVisitor {

	@NonNull
	public MethodDecorationsFinder decorationsFinder;
	
	@NonNull
	public MethodDecorationScanner decorationScanner;
	
	@NonNull
	public DecoratingSettings decoratingSettings;
	
	@NonNull
	public AsmUtils asmUtils;
	
	public DecoratorClassVisitor 
	(
			ClassVisitor classVisitor, 
			ClassInfo classInfo, 
			DecoratingSettings decoratingSettings, 
			MethodDecorationsFinder decorationsFinder, 
			MethodDecorationScanner decorationScanner, 
			AsmUtils asmUtils
	)
	{
		super(ASM8_EXPERIMENTAL, classVisitor, classInfo);
		this.decoratingSettings = decoratingSettings;
		this.decorationScanner = decorationScanner;
		this.decorationsFinder = decorationsFinder;
		this.asmUtils = asmUtils;
	}
	
	@Override
	public MethodVisitor visitMethodInformated(MethodInfo methodInfo, int access, String name, String descriptor, String signature, String[] exceptions)
	{
		MethodVisitor mv = super.visitMethodInformated(methodInfo, access, name, descriptor, signature, exceptions);
		
		var decoratorClasses = decorationsFinder.findDecoratorsFor(methodInfo);
		var decorators = new ExtendedList<MethodDecoration>();
		
		for (var decoratorClass : decoratorClasses)
		{
			var decorator = decorationScanner.scan(methodInfo, decoratorClass);
			
			decorators.add(decorator);
		}
		
		if (!CollectionsSWL.isNullOrEmpty(decorators))
		{
			mv = new DecoratorMethodVisitor(asmUtils, decorators, methodInfo, mv, access, name, descriptor);
		}
		
		return mv;
	}
}
