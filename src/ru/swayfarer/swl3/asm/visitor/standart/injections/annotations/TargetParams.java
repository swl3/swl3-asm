package ru.swayfarer.swl3.asm.visitor.standart.injections.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

@Retention(RUNTIME)
public @interface TargetParams
{
	public String[] descriptors() default "";
	public Class<?>[] types() default {};
}
