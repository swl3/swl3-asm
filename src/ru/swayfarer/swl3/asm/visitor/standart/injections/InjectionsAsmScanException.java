package ru.swayfarer.swl3.asm.visitor.standart.injections;

@SuppressWarnings("serial")
public class InjectionsAsmScanException extends RuntimeException {

	public InjectionsAsmScanException()
	{
		super();
	}

	public InjectionsAsmScanException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public InjectionsAsmScanException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public InjectionsAsmScanException(String message)
	{
		super(message);
	}

	public InjectionsAsmScanException(Throwable cause)
	{
		super(cause);
	}

}
