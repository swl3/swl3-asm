package ru.swayfarer.swl3.asm.visitor.standart.injections;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.commons.AdviceAdapter;

public class InjectionsMethodVisitor extends AdviceAdapter {

	@NonNull
	public ExtendedList<MethodInjection> injections;
	
	@NonNull
	public MethodInfo methodInfo;
	
	@Internal
	public AsmUtils asmUtils = new AsmUtils();
	
	@Internal
	public Type returnControllerType = Type.getType(ExtendedOptional.class);
	
	@Internal
	public boolean willThrow;
	
	@Internal
	public boolean injectOnExit;
	
	@Internal
	public int retControllerVarId = -1;
	
	@Internal
	public int retValueVarId = -1;
	
	public InjectionsMethodVisitor(ExtendedList<MethodInjection> injections, MethodInfo methodInfo, MethodVisitor methodVisitor, int access, String name, String descriptor)
	{
		super(ASM8_EXPERIMENTAL, methodVisitor, access, name, descriptor);
		this.methodInfo = methodInfo;
		this.injections = injections;
	}
	
	@Override
	protected void onMethodEnter()
	{
		for (var injection : injections)
			inject(injection, false, -1);
		
		super.onMethodEnter();
	}
	
	@Override
	protected void onMethodExit(int opcode)
	{
		for (var injection : injections)
		{
			if (injection.getInjectionInfo().onThrowsToo || opcode != ATHROW)
			{
				inject(injection, true, opcode);
			}
		}
		
		super.onMethodExit(opcode);
	}
	
	public void inject(MethodInjection methodInjection, boolean onExit, int opcode)
	{
		willThrow = opcode == ATHROW;
		injectOnExit = methodInjection.getInjectionInfo().isInjectOnExit();
		
		if (onExit != injectOnExit)
			return;
		
		var hasReturnController = methodInjection.getInjectionInfo().hasReturnController;
		
		if (isNonVoidOrThrows())
		{
			if (injectOnExit)
			{
				retValueVarId = newLocal(willThrow ? Type.getType(Throwable.class) : getReturnType());
				storeLocal(retValueVarId);
			}
		}
		
		if (hasReturnController)
		{
			retControllerVarId = newLocal(returnControllerType);
			
			if (!willThrow && isNonVoid() && injectOnExit)
			{
				loadLocal(retValueVarId);
				asmUtils.types().convesion().box(getReturnType(), this);
				visitMethodInsn(INVOKESTATIC, returnControllerType.getInternalName(), "ofNonNull", "(Ljava/lang/Object;)" + returnControllerType.getDescriptor(), false);
			}
			else
			{
				visitMethodInsn(INVOKESTATIC, returnControllerType.getInternalName(), "empty", "()" + returnControllerType.getDescriptor(), false);
			}
			
			storeLocal(retControllerVarId);
		}
		
		if (methodInfo.getAccess().isStatic())
			visitInsn(ACONST_NULL);
		else
			loadThis();
		
		loadArgs();
		
		if (hasReturnController)
			loadLocal(retControllerVarId);
		
		asmUtils.methods().invokeStatic(
			this, 
			methodInjection.getInjectionInfo().getInjectionMethod()
		);
		
		if (injectOnExit)
		{
			if (hasReturnController)
			{
				returnFromControllerIfPresent(injectOnExit, willThrow);
			}
			else
			{
				if (isNonVoidOrThrows())
					loadLocal(retValueVarId);
				
				visitInsn(opcode);
			}
		}
		else 
		{
			if (hasReturnController)
			{
				returnFromControllerIfPresent(injectOnExit, willThrow);
			}
		}
	}

	public void returnFromControllerIfPresent(boolean injectOnExit, boolean willThrow)
	{
		var lblIfNotPresent = newLabel();
		
		loadLocal(retControllerVarId);
		
		if (injectOnExit)
		{
			loadAndReturnController();
		}
		else
		{
			visitMethodInsn(INVOKEVIRTUAL, returnControllerType.getInternalName(), "isPresent", "()Z", false);
			visitJumpInsn(IFEQ, lblIfNotPresent);
			
			loadAndReturnController();
			
			mark(lblIfNotPresent);
			
			if (isNonVoidOrThrows())
				loadLocal(retValueVarId);
		}
	}
	
	public void loadAndReturnController()
	{
		if (isNonVoid())
		{
			loadLocal(retControllerVarId);
			returnFromController();
		}
		else
		{
			returnValue();
		}
	}
	
	public boolean isNonVoidOrThrows()
	{
		return isNonVoid() || willThrow;
	}
	
	public boolean isNonVoid()
	{
		return !Type.VOID_TYPE.equals(getReturnType());
	}
	
	public void returnFromController()
	{
		visitMethodInsn(Opcodes.INVOKEVIRTUAL, returnControllerType.getInternalName(), "orNull", "()Ljava/lang/Object;", false);
		castAndBox();
		returnValue();
	}
	
	public void castAndBox()
	{
		asmUtils.types().convesion().convert(AsmUtils.OBJECT_TYPE, getReturnType(), this);
	}
	
	public Long helloWorld()
	{
		return 1l;
	}
}
