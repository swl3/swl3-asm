package ru.swayfarer.swl3.asm.visitor.standart.injections;

import lombok.var;
import ru.swayfarer.swl3.asm.info.AnnotationInfo;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.info.VariableInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.asm.visitor.standart.injections.annotations.InjectOnExit;
import ru.swayfarer.swl3.asm.visitor.standart.injections.annotations.InjectionMethod;
import ru.swayfarer.swl3.asm.visitor.standart.injections.annotations.OnThrowsToo;
import ru.swayfarer.swl3.asm.visitor.standart.injections.annotations.RetController;
import ru.swayfarer.swl3.asm.visitor.standart.injections.annotations.TargetParams;
import ru.swayfarer.swl3.asm.visitor.standart.injections.annotations.TargetType;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

public class InjectionsContainerScanner {

	public BytecodeScanner bytecodeScanner;
	
	public Type onThrowsTooAnnotationType = Type.getType(OnThrowsToo.class);
	public Type retControllerAnnotationType = Type.getType(RetController.class);
	public Type targetTypeAnnotationType = Type.getType(TargetType.class);
	public Type targetParamsAnnotationType = Type.getType(TargetParams.class);
	public Type injectOnExitAnnotationType = Type.getType(InjectOnExit.class);
	public Type retControllerType = Type.getType(ExtendedOptional.class);
	
	public ExtendedList<String> retControllerParamNames = CollectionsSWL.list(
			"ret", 
			"retRef", 
			"returnRef", 
			"returnReference"
	);
	
	public ExtendedList<MethodInjection> foundInjections = CollectionsSWL.list();
	
	public InjectionsContainerScanner scan(ClassInfo classInfo)
	{
		classInfo.methodsStream()
			.annotations().has(InjectionMethod.class)
			.withParams().moreThan(0)
			.access().statics()
			.access().publics()
			.each(this::scan)
		;
		
		return this;
	}
	
	public void scan(MethodInfo methodInfo)
	{
		var params = methodInfo.getParams().copy();
		
		ExtendedList<Type> targetParamsTypes = null;
		
		var targetMethodName = CollectionsSWL.list(methodInfo.getName());
		var targetTypes = CollectionsSWL.<Type>list();
		var hasReturnController = false;
		var onThrowsToo = false;
		var injectOnExit = methodInfo.getAnnotationsInfo().hasAnnotation(injectOnExitAnnotationType);
		
		var lastParam = params.last();
		
		if (isReturnController(lastParam))
		{
			hasReturnController = true;
			params.removeLast();
			
			if (params.isEmpty())
				return;
		}
		
		var firstParam = params.first();
		var firstParamType = firstParam.getParamType();
		params.removeFirst();
		
		ExceptionsUtils.IfNull(
			firstParamType, 
			NoSuchFieldException.class,
			"Can't find target class type in", methodInfo
		);
		
		if (targetParamsTypes == null)
			targetParamsTypes = params.map(VariableInfo::getParamType);
		
		var targetParamsAnnoation = methodInfo.getAnnotationsInfo().first(targetParamsAnnotationType);
		
		if (targetParamsAnnoation != null)
		{
			var paramTypesFromAnnotation = readTargetMethodAnnotation(methodInfo, targetParamsAnnoation);
			
			if (!CollectionsSWL.isNullOrEmpty(paramTypesFromAnnotation))
			{
				targetParamsTypes.setAll(paramTypesFromAnnotation);
			}
		}
		
		var targetTypeAnnotation = methodInfo.getAnnotationsInfo().first(targetTypeAnnotationType);
		
		if (targetTypeAnnotation != null)
		{
			var typesFromAnnotation = readTargetTypeAnnotation(targetTypeAnnotation);
			
			if (!CollectionsSWL.isNullOrEmpty(typesFromAnnotation))
			{
				targetTypes.addAll(typesFromAnnotation);
			}
		}
		
		if (methodInfo.getAnnotationsInfo().hasAnnotation(onThrowsTooAnnotationType))
		{
			onThrowsToo = true;
		}
		
		var descStart = new DynamicString();
		descStart.append("(");

		for (var paramType : targetParamsTypes)
		{
			descStart.append(paramType.getDescriptor());
		}

		descStart.append(")");
		
		String targetMethodDescStart = descStart.toString();
		
		if (targetTypes.isEmpty())
		{
			targetTypes.add(firstParamType);
		}
		
		var targetMethodInfo = new MethodInjection.TargetMethodInfo(targetMethodName, targetMethodDescStart);
		
		var injectionInfo = new MethodInjection.InjectionInfo(methodInfo, firstParam, params);
		injectionInfo.setHasReturnController(hasReturnController);
		injectionInfo.setInjectOnExit(injectOnExit || hasReturnController);
		injectionInfo.setOnThrowsToo(onThrowsToo);
		
		var methodInjection = new MethodInjection(targetTypes, targetMethodInfo, injectionInfo);

		System.out.println("Found method injection: " + methodInjection.getTargetMethodInfo().getMethodDescStart());
		foundInjections.add(methodInjection);
	}
	
	public ExtendedList<Type> readTargetTypeAnnotation(AnnotationInfo targetTypeAnnoation)
	{
		var targetTypeDescriptors = targetTypeAnnoation.getParams("descriptors", String.class);
		
		var list = new ExtendedList<Type>();
		
		if (!CollectionsSWL.isNullOrEmpty(targetTypeDescriptors))
		{
			for (var desc : targetTypeDescriptors)
			{
				list.add(Type.getType(desc));
			}
		}
		
		var targetTypes = targetTypeAnnoation.getParams("types", Type.class);
		
		if (!CollectionsSWL.isNullOrEmpty(targetTypes))
			list.addAll(targetTypes);
		
		System.out.println("Dbg: " + targetTypeAnnoation.getAnnotationType().getClassName());
		
		return list.isEmpty() ? null : list;
	}
	
	public ExtendedList<Type> readTargetMethodAnnotation(MethodInfo sourceMethodInfo, AnnotationInfo targetTypeAnnoation)
	{
		var ret = new ExtendedList<Type>();
		var targetParamsDescriptors = targetTypeAnnoation.getParams("descriptors", String.class);
		
		if (!CollectionsSWL.isNullOrEmpty(targetParamsDescriptors))
		{
			for (var desc : targetParamsDescriptors)
			{
				ret.add(Type.getType(desc));
			}
		}
		
		var targetParamsClasses = targetTypeAnnoation.getParams("types", Type.class);
		
		if (!CollectionsSWL.isNullOrEmpty(targetParamsClasses))
		{
			if (ret.isNotEmpty())
			{
				ExceptionsUtils.Throw(
						InjectionsAsmScanException.class, 
						"Param 'descriptors' of annotation", targetParamsAnnotationType.getClassName(), "exists together with 'types'. It's not allowed. In method", sourceMethodInfo.getShortInfo()
				);
			}
			
			for (var paramClass : targetParamsClasses)
			{
				ret.add(paramClass);
			}
		}
		
		return ret;
	}
	
	public boolean isReturnController(VariableInfo paramInfo)
	{
		if (!retControllerType.equals(paramInfo.getParamType()))
			return false;
		
		if (retControllerParamNames.contains(paramInfo.getName()))
			return true;
		
		if (paramInfo.getAnnotationsInfo().hasAnnotation(retControllerAnnotationType))
			return true;
		
		return false;
	}
}
