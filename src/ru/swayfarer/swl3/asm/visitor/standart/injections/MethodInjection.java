package ru.swayfarer.swl3.asm.visitor.standart.injections;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.info.VariableInfo;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Data @Accessors(chain = true)
public class MethodInjection {

	@NonNull
	public ExtendedList<Type> targetTypes;
	
	@NonNull
	public TargetMethodInfo targetMethodInfo;
	
	@NonNull
	public InjectionInfo injectionInfo;
	
	public boolean isAccepts(MethodInfo methodInfo)
	{
		var methodOwnerType = methodInfo.getOwner().getClassType();
		
		if (!targetTypes.contains(methodOwnerType))
			return false;
		
		var methodParams = methodInfo.getParams();
		var injectionMethodParams = injectionInfo.getMethodParams();
		
		if (methodParams.size() != injectionMethodParams.size())
			return false;
		
		var iterator = methodParams.iterator();
		
		for (var injectionParam : injectionMethodParams)
		{
			var methodParam = iterator.next();
			
			if (!injectionParam.canAccept(methodParam))
			{
				return false;
			}
		}
		
		return true;
	}
	
	@Data @Accessors(chain = true)
	public static class InjectionInfo {
		public boolean injectOnExit;
		public boolean hasReturnController;
		public boolean onThrowsToo;
		
		@NonNull
		public MethodInfo injectionMethod;
		
		@NonNull
		public VariableInfo instanceParam;
		
		@NonNull
		public ExtendedList<VariableInfo> methodParams;
	}
	
	@Data @Accessors(chain = true)
	public static class TargetMethodInfo {
		
		@NonNull
		public ExtendedList<String> methodNames;
		
		@NonNull
		public String methodDescStart;
		
		public boolean isAccepts(MethodInfo methodInfo)
		{
			return false;
		}
	}
	
}
