package ru.swayfarer.swl3.asm.visitor.standart.injections;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.visitor.InformatedClassVisitor;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;

@Data @Accessors(chain = true) @EqualsAndHashCode(callSuper = false)
public class InjectionsClassVisitor extends InformatedClassVisitor {

	public ExtendedList<MethodInjection> injections = new ExtendedList<>();
	
	public InjectionsClassVisitor(@NonNull ClassInfo classInfo, ClassVisitor classVisitor)
	{
		super(classVisitor, classInfo);
	}
	
	@Override
	public MethodVisitor visitMethodInformated(MethodInfo methodInfo, int access, String name, String descriptor, String signature, String[] exceptions)
	{
		var mv = super.visitMethodInformated(methodInfo, access, name, descriptor, signature, exceptions);
		
		var classType = classInfo.getClassType();
		
		var methodInjections = injections.exStream()
			.filter((e) -> e.getTargetTypes().contains(classType))
			.filter((e) -> e.getTargetMethodInfo().getMethodNames().contains(name))
			.filter((e) -> descriptor.startsWith(e.getTargetMethodInfo().getMethodDescStart()))
			.filter((e) -> e.isAccepts(methodInfo))
		.toExList();
		
		mv = new InjectionsMethodVisitor(methodInjections, methodInfo, mv, access, name, descriptor);
		
		return mv;
	}
}
