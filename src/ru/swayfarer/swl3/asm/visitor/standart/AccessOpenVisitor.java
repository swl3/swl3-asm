package ru.swayfarer.swl3.asm.visitor.standart;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.Access;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.FieldVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

import java.lang.reflect.Modifier;

@Getter
@Setter
@Accessors(chain = true)
public class AccessOpenVisitor extends ClassVisitor {

	@Internal
	public boolean skip;

	public AccessOpenVisitor(ClassVisitor classVisitor)
	{
		super(ASM8_EXPERIMENTAL, classVisitor);
	}
	
	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces)
	{
		var swlAccess = new Access(access);
		skip = swlAccess.isEnum();

		super.visit(version, getOpenedAccess(access), name, signature, superName, interfaces);
	}
	
	@Override
	public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions)
	{
		return super.visitMethod(getOpenedAccess(access), name, descriptor, signature, exceptions);
	}
	
	@Override
	public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value)
	{
		return super.visitField(getOpenedAccess(access), name, descriptor, signature, value);
	}

	public int getOpenedAccess(int access)
	{
		if (skip)
			return access;

		return Access.open(access);
	}
}