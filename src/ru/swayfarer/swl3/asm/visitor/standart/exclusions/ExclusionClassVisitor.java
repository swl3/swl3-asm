package ru.swayfarer.swl3.asm.visitor.standart.exclusions;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.AnnotationVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.FieldVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.TypePath;

@Getter
@Setter
@Accessors(chain = true)
public class ExclusionClassVisitor extends ClassVisitor
{
    @Internal
    public String classInternalName;

    @NonNull
    @Internal
    public ExclusionsConfig config;

    public ExclusionClassVisitor(ClassVisitor classVisitor, ExclusionsConfig config)
    {
        super(classVisitor);
        this.config = config;
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces)
    {
        classInternalName = name;
        super.visit(version, access, name, signature, superName, interfaces);
    }

    @Override
    public AnnotationVisitor visitAnnotation(String descriptor, boolean visible)
    {
        var type = Type.getType(descriptor);

        if (config.isAcceptsType(type))
            return null;

        return new ExclusionAnnotationVisitor(api, super.visitAnnotation(descriptor, visible), config);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions)
    {
        var methodReturn = Type.getReturnType(descriptor);
        var methodParams = Type.getArgumentTypes(descriptor);

        if (config.isAcceptsMethod(name, Type.getType("L" + classInternalName + ";"), Type.getType(descriptor)))
        {
            System.out.println("Reason 1 Dropped method " + classInternalName + "::" + name + descriptor);
            return null;
        }

        if (config.isAcceptsType(methodReturn))
        {
            System.out.println("Reason 2 Dropped method " + classInternalName + "::" + name + descriptor);
            return null;
        }


        if (ExtendedStream.of(methodParams).anyMatches(config::isAcceptsType))
        {
            System.out.println("Reason 3 Dropped method " + classInternalName + "::" + name + descriptor);
            return null;
        }


        if (!CollectionsSWL.isNullOrEmpty(exceptions))
        {
            var newExceptions = new ExtendedList<String>();

            for (var exception : exceptions)
            {
                var type = Type.getType("L" + exception + ";");

                if (!config.isAcceptsType(type))
                    newExceptions.add(exception);
            }

            exceptions = newExceptions.toArray(String.class);
        }

        return new ExclusionMethodVisitor(api, super.visitMethod(access, name, descriptor, signature, exceptions), config);
    }

    @Override
    public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value)
    {
        var fieldType = Type.getType(descriptor);

        if (config.isAcceptsField(name, Type.getType("L" + classInternalName + ";"), fieldType))
            return null;

        if (config.isAcceptsType(fieldType))
            return null;

        return new ExclusionFieldVisitor(api, super.visitField(access, name, descriptor, signature, value), config);
    }

//    @Override
//    public void visitInnerClass(String name, String outerName, String innerName, int access)
//    {
//        var type = Type.getType("L" +outerName + "$" + innerName + ";");
//
//        if (config.isAcceptsType(type))
//            return;
//
//        super.visitInnerClass(name, outerName, innerName, access);
//    }
//
//    @Override
//    public void visitNestHost(String nestHost)
//    {
//        var type = Type.getType(nestHost);
//
//        if (config.isAcceptsType(type))
//            return;
//
//        super.visitNestHost(nestHost);
//    }
//
//    @Override
//    public void visitOuterClass(String owner, String name, String descriptor)
//    {
//        var type = Type.getType("L" + owner + ";");
//
//        if (config.isAcceptsType(type))
//            return;
//
//        super.visitOuterClass(owner, name, descriptor);
//    }

    @Override
    public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible)
    {
        var type = Type.getType(descriptor);

        if (config.isAcceptsType(type))
            return null;

        return new ExclusionAnnotationVisitor(api, super.visitTypeAnnotation(typeRef, typePath, descriptor, visible), config);
    }
}
