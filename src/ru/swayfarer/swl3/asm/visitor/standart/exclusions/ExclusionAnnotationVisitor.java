package ru.swayfarer.swl3.asm.visitor.standart.exclusions;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.AnnotationVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Getter
@Setter
@Accessors(chain = true)
public class ExclusionAnnotationVisitor extends AnnotationVisitor
{
    public ExclusionsConfig config;

    public ExclusionAnnotationVisitor(int api, AnnotationVisitor annotationVisitor, ExclusionsConfig config)
    {
        super(api, annotationVisitor);
        this.config = config;
    }

    @Override
    public AnnotationVisitor visitAnnotation(String name, String descriptor)
    {
        var type = Type.getType(descriptor);

        if (config.isAcceptsType(type))
            return null;

        return new ExclusionAnnotationVisitor(api, super.visitAnnotation(name, descriptor), config);
    }

    @Override
    public AnnotationVisitor visitArray(String name)
    {
        return new ExclusionAnnotationVisitor(api, super.visitArray(name), config);
    }

    @Override
    public void visitEnum(String name, String descriptor, String value)
    {
        var type = Type.getType(descriptor);

        if (config.isAcceptsType(type))
            return;

        super.visitEnum(name, descriptor, value);
    }
}
