package ru.swayfarer.swl3.asm.visitor.standart.exclusions;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.AnnotationVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Label;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.TypePath;

@Getter
@Setter
@Accessors(chain = true)
public class ExclusionMethodVisitor extends MethodVisitor implements Opcodes
{
    public ExclusionsConfig config;

    public ExclusionMethodVisitor(int api, MethodVisitor methodVisitor, ExclusionsConfig config)
    {
        super(api, methodVisitor);
        this.config = config;
    }

    @Override
    public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible)
    {
        var type = Type.getType(descriptor);

        if (config.isAcceptsType(type))
            return null;

        return new ExclusionAnnotationVisitor(api, super.visitTypeAnnotation(typeRef, typePath, descriptor, visible), config);
    }

    @Override
    public AnnotationVisitor visitAnnotation(String descriptor, boolean visible)
    {
        var type = Type.getType(descriptor);

        if (config.isAcceptsType(type))
            return null;

        return new ExclusionAnnotationVisitor(api, super.visitAnnotation(descriptor, visible), config);
    }

    @Override
    public AnnotationVisitor visitAnnotationDefault()
    {
        return new ExclusionAnnotationVisitor(api, super.visitAnnotationDefault(), config);
    }

    @Override
    public AnnotationVisitor visitParameterAnnotation(int parameter, String descriptor, boolean visible)
    {
        var type = Type.getType(descriptor);

        if (config.isAcceptsType(type))
            return null;

        return new ExclusionAnnotationVisitor(api, super.visitParameterAnnotation(parameter, descriptor, visible), config);
    }

    @Override
    public AnnotationVisitor visitLocalVariableAnnotation(int typeRef, TypePath typePath, Label[] start, Label[] end, int[] index, String descriptor, boolean visible)
    {
        var type = Type.getType(descriptor);

        if (config.isAcceptsType(type))
            return null;

        return new ExclusionAnnotationVisitor(api, super.visitLocalVariableAnnotation(typeRef, typePath, start, end, index, descriptor, visible), config);
    }
}
