package ru.swayfarer.swl3.asm.visitor.standart.exclusions;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.AnnotationVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.FieldVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.TypePath;

@Getter
@Setter
@Accessors(chain = true)
public class ExclusionFieldVisitor extends FieldVisitor
{
    @Internal
    public ExclusionsConfig config;

    public ExclusionFieldVisitor(int api, FieldVisitor fieldVisitor, ExclusionsConfig config)
    {
        super(api, fieldVisitor);
        this.config = config;
    }

    @Override
    public AnnotationVisitor visitAnnotation(String descriptor, boolean visible)
    {
        return new ExclusionAnnotationVisitor(api, super.visitAnnotation(descriptor, visible), config);
    }

    @Override
    public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible)
    {
        return new ExclusionAnnotationVisitor(api, super.visitTypeAnnotation(typeRef, typePath, descriptor, visible), config);
    }
}
