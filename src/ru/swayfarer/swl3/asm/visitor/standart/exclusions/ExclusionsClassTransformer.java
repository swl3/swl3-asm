package ru.swayfarer.swl3.asm.visitor.standart.exclusions;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.asm.transformer.ClassBytecodeInfo;
import ru.swayfarer.swl3.asm.transformer.InformatedAsmClassTransformer;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassReader;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassWriter;

@Getter
@Setter
@Accessors(chain = true)
public class ExclusionsClassTransformer extends InformatedAsmClassTransformer
{
    @NonNull
    @Internal
    public ExclusionsConfig config;

    @NonNull
    @Internal
    public ExclusionsStubGenerator stubGenerator;

    public ExclusionsClassTransformer(@NonNull BytecodeScanner bytecodeScanner, ExclusionsConfig config)
    {
        super(bytecodeScanner);
        this.config = config;
        stubGenerator = new ExclusionsStubGenerator();
    }

    @Override
    public void transform(@NonNull ClassBytecodeInfo classLoadingEvent)
    {
        var classInfo = findClassInfo(classLoadingEvent);

        if (classInfo != null && config.isAcceptsType(classInfo.getClassType()))
        {
            var newBytes = stubGenerator.generateStubClass(classInfo);
            classLoadingEvent.setClassBytes(newBytes);
        }

        super.transform(classLoadingEvent);
    }

    @Override
    public ClassVisitor transformInformated(ClassInfo classInfo, ClassReader reader, ClassWriter writer, ClassBytecodeInfo event)
    {
        return new ExclusionClassVisitor(writer, config);
    }
}
