package ru.swayfarer.swl3.asm.visitor.standart.exclusions;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassWriter;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

import static ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes.RETURN;

@Getter
@Setter
@Accessors(chain = true)
public class ExclusionsStubGenerator
{
    public byte[] generateStubClass(ClassInfo classInfo)
    {
        var parentType = classInfo.getParentType();
        var interfaces = classInfo.getInterfaces();

        var cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        cw.visit(
                classInfo.getVersion(),
                classInfo.getAccess().getModifier(),
                classInfo.getClassType().getInternalName(),
                null,
                parentType.getInternalName(),
                interfaces.map(Type::getInternalName).toArray(String.class)
        );

        // Default constructor
        {
            var mv = cw.visitMethod(Opcodes.ACC_PUBLIC, "<init>", "()V", null, null);
            mv.visitCode();
            mv.visitVarInsn(Opcodes.ALOAD, 0);
            mv.visitMethodInsn(Opcodes.INVOKESPECIAL, parentType.getInternalName(), "<init>", "()V", false);
            mv.visitInsn(RETURN);
            mv.visitMaxs(1, 1);
            mv.visitEnd();
        }

        cw.visitEnd();

        return cw.toByteArray();
    }
}
