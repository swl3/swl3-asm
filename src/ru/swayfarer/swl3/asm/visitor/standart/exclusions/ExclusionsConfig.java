package ru.swayfarer.swl3.asm.visitor.standart.exclusions;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction3;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Getter
@Setter
@Accessors(chain = true)
public class ExclusionsConfig
{
    @Internal
    public BytecodeScanner bytecodeScanner;

    @Internal
    public IFunction1<Type, Boolean> typeExclusionFilter = (e) -> false;

    @Internal
    public IFunction3<String, Type, Type, Boolean> methodExclusionFilter = (e1, e2, e3) -> false;

    @Internal
    public IFunction3<String, Type, Type, Boolean> fieldExclusionFilter = (e1, e2, e3) -> false;

    public boolean isAcceptsType(Type type)
    {
        return typeExclusionFilter.apply(type);
    }

    public boolean isAcceptsMethod(String name, Type owner, Type method)
    {
        return methodExclusionFilter.apply(name, owner, method);
    }

    public boolean isAcceptsField(String name, Type owner, Type field)
    {
        return fieldExclusionFilter.apply(name, owner, field);
    }
}
