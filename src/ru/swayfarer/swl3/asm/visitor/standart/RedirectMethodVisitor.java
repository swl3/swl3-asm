package ru.swayfarer.swl3.asm.visitor.standart;

import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.AnnotationVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Attribute;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Handle;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Label;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.TypePath;

public class RedirectMethodVisitor extends MethodVisitor
{
    @Internal
    public MethodVisitor wrappedMethodVisitor;

    public RedirectMethodVisitor(MethodVisitor wrappedMethodVisitor)
    {
        super(wrappedMethodVisitor.api);
        this.wrappedMethodVisitor = wrappedMethodVisitor;
    }

    @Override
    public void visitParameter(String name, int access)
    {
        wrappedMethodVisitor.visitParameter(name, access);
    }

    @Override
    public AnnotationVisitor visitAnnotationDefault()
    {
        return wrappedMethodVisitor.visitAnnotationDefault();
    }

    @Override
    public AnnotationVisitor visitAnnotation(String descriptor, boolean visible)
    {
        return wrappedMethodVisitor.visitAnnotation(descriptor, visible);
    }

    @Override
    public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible)
    {
        return wrappedMethodVisitor.visitTypeAnnotation(typeRef, typePath, descriptor, visible);
    }

    @Override
    public void visitAnnotableParameterCount(int parameterCount, boolean visible)
    {
        wrappedMethodVisitor.visitAnnotableParameterCount(parameterCount, visible);
    }

    @Override
    public AnnotationVisitor visitParameterAnnotation(int parameter, String descriptor, boolean visible)
    {
        return wrappedMethodVisitor.visitParameterAnnotation(parameter, descriptor, visible);
    }

    @Override
    public void visitParameterCount(int count)
    {
        wrappedMethodVisitor.visitParameterCount(count);
    }

    @Override
    public void visitParameter(String name, String signature, int index)
    {
        wrappedMethodVisitor.visitParameter(name, signature, index);
    }

    @Override
    public void visitAttribute(Attribute attribute)
    {
        wrappedMethodVisitor.visitAttribute(attribute);
    }

    @Override
    public void visitCode()
    {
        wrappedMethodVisitor.visitCode();
    }

    @Override
    public void visitFrame(int type, int numLocal, Object[] local, int numStack, Object[] stack)
    {
        wrappedMethodVisitor.visitFrame(type, numLocal, local, numStack, stack);
    }

    @Override
    public void visitInsn(int opcode)
    {
        wrappedMethodVisitor.visitInsn(opcode);
    }

    @Override
    public void visitIntInsn(int opcode, int operand)
    {
        wrappedMethodVisitor.visitIntInsn(opcode, operand);
    }

    @Override
    public void visitVarInsn(int opcode, int var)
    {
        wrappedMethodVisitor.visitVarInsn(opcode, var);
    }

    @Override
    public void visitTypeInsn(int opcode, String type)
    {
        wrappedMethodVisitor.visitTypeInsn(opcode, type);
    }

    @Override
    public void visitFieldInsn(int opcode, String owner, String name, String descriptor)
    {
        wrappedMethodVisitor.visitFieldInsn(opcode, owner, name, descriptor);
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String descriptor)
    {
        wrappedMethodVisitor.visitMethodInsn(opcode, owner, name, descriptor);
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface)
    {
        wrappedMethodVisitor.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
    }

    @Override
    public void visitInvokeDynamicInsn(String name, String descriptor, Handle bootstrapMethodHandle, Object... bootstrapMethodArguments)
    {
        wrappedMethodVisitor.visitInvokeDynamicInsn(name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments);
    }

    @Override
    public void visitJumpInsn(int opcode, Label label)
    {
        wrappedMethodVisitor.visitJumpInsn(opcode, label);
    }

    @Override
    public void visitLabel(Label label)
    {
        wrappedMethodVisitor.visitLabel(label);
    }

    @Override
    public void visitLdcInsn(Object value)
    {
        wrappedMethodVisitor.visitLdcInsn(value);
    }

    @Override
    public void visitIincInsn(int var, int increment)
    {
        wrappedMethodVisitor.visitIincInsn(var, increment);
    }

    @Override
    public void visitTableSwitchInsn(int min, int max, Label dflt, Label... labels)
    {
        wrappedMethodVisitor.visitTableSwitchInsn(min, max, dflt, labels);
    }

    @Override
    public void visitLookupSwitchInsn(Label dflt, int[] keys, Label[] labels)
    {
        wrappedMethodVisitor.visitLookupSwitchInsn(dflt, keys, labels);
    }

    @Override
    public void visitMultiANewArrayInsn(String descriptor, int numDimensions)
    {
        wrappedMethodVisitor.visitMultiANewArrayInsn(descriptor, numDimensions);
    }

    @Override
    public AnnotationVisitor visitInsnAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible)
    {
        return wrappedMethodVisitor.visitInsnAnnotation(typeRef, typePath, descriptor, visible);
    }

    @Override
    public void visitTryCatchBlock(Label start, Label end, Label handler, String type)
    {
        wrappedMethodVisitor.visitTryCatchBlock(start, end, handler, type);
    }

    @Override
    public AnnotationVisitor visitTryCatchAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible)
    {
        return wrappedMethodVisitor.visitTryCatchAnnotation(typeRef, typePath, descriptor, visible);
    }

    @Override
    public void visitLocalVariable(String name, String descriptor, String signature, Label start, Label end, int index)
    {
        wrappedMethodVisitor.visitLocalVariable(name, descriptor, signature, start, end, index);
    }

    @Override
    public AnnotationVisitor visitLocalVariableAnnotation(int typeRef, TypePath typePath, Label[] start, Label[] end, int[] index, String descriptor, boolean visible)
    {
        return wrappedMethodVisitor.visitLocalVariableAnnotation(typeRef, typePath, start, end, index, descriptor, visible);
    }

    @Override
    public void visitLineNumber(int line, Label start)
    {
        wrappedMethodVisitor.visitLineNumber(line, start);
    }

    @Override
    public void visitMaxs(int maxStack, int maxLocals)
    {
        wrappedMethodVisitor.visitMaxs(maxStack, maxLocals);
    }

    @Override
    public void visitEnd()
    {
        wrappedMethodVisitor.visitEnd();
    }
}
