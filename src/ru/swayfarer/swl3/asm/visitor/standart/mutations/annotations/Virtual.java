package ru.swayfarer.swl3.asm.visitor.standart.mutations.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Virtual
{
}
