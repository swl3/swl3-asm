package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.asm.visitor.InformatedClassVisitor;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.mutators.DefaultMutator;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Getter
@Setter
@Accessors(chain = true)
public class MutationClassVisitor extends InformatedClassVisitor
{
    @Internal
    public MutationConfig mutationConfig;

    @Internal
    public AsmUtils asmUtils;

    public MutationClassVisitor(
            ClassVisitor classVisitor,
            @NonNull ClassInfo classInfo,
            @NonNull MutationConfig mutationConfig,
            @NonNull AsmUtils asmUtils
    )
    {
        super(classVisitor, classInfo);
        this.mutationConfig = mutationConfig;
        this.asmUtils = asmUtils;
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces)
    {
        // Find new class interfaces and append it to visit method
        var newInterfaces = mutationConfig.findNewInterfacesFor(classInfo)
                .flatMap((e) -> e.getInterfaces().stream())
                .nonNull()
                .map(Type::getInternalName)
                .distinct()
                .toArray(String.class)
        ;

        if (CollectionsSWL.isNullOrEmpty(interfaces))
            interfaces = newInterfaces;
        else
        {
            var interfacesList = new ExtendedList<String>();
            interfacesList.addAll(interfaces);
            interfacesList.addAll(newInterfaces);
            interfacesList.distinct();
            interfaces = interfacesList.toArray(String.class);
        }

        super.visit(version, access, name, signature, superName, interfaces);


        // Append all new methods
        mutationConfig.findNewMethodsFor(classInfo)
                .each((newMethod) -> {
                    var methodContent = newMethod.getMethodContent();
                    var mv = super.visitMethod(methodContent.getAccess(), methodContent.getMethodName(), methodContent.getDesc(), null, null);
                    newMethod.writeTo(mv, classInfo.getClassType());
                })
        ;

        // Append all new fields
        mutationConfig.findNewFieldsFor(classInfo)
                .each((newField) -> {
                    visitField(newField.getAccess(), newField.getName(), newField.getFieldType().getDescriptor(), null, null);
                })
        ;
    }

    @Override
    public MethodVisitor visitMethodInformated(MethodInfo methodInfo, int access, String name, String descriptor, String signature, String[] exceptions)
    {
        var mv = super.visitMethodInformated(methodInfo, access, name, descriptor, signature, exceptions);

        if (methodInfo != null)
        {
            var mmv = new MutationMethodVisitor(api, mv, methodInfo, this);
            mmv.getMutators().add(new DefaultMutator(mutationConfig, asmUtils));
            mmv.getMutators().add(new NewConstructorContentMutator(mutationConfig, asmUtils));

            mv = mmv;
        }

        return mv;
    }
}
