package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Label;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.commons.AdviceAdapter;

@Getter
@Setter
@Accessors(chain = true)
public class MutationMethodVisitor extends AdviceAdapter implements Opcodes
{
    public ExtendedList<IMethodMutator> mutators = new ExtendedList<>();
    public MethodInfo methodInfo;
    public ClassVisitor cv;
    public ExtendedMap<Label, Integer> labelToLine = new ExtendedMap<>();

    public MutationMethodVisitor(int api, MethodVisitor methodVisitor, MethodInfo methodInfo, ClassVisitor cv)
    {
        super(api, methodVisitor, methodInfo.getAccess().getModifier(), methodInfo.getName(), methodInfo.getDescriptor());
        this.methodInfo = methodInfo;
        this.cv = cv;
        this.autoConstructorInvokedDisable = false;
    }

//    @Override
//    public void visitMethodInsn(int opcode, String owner, String name, String descriptor)
//    {
//        mutators.each((mutator) -> mutator.onMethodInvokeStart(this, methodInfo, owner, name, descriptor));
//        super.visitMethodInsn(opcode, owner, name, descriptor);
//        mutators.each((mutator) -> mutator.onMethodInvokeEnd(this, methodInfo, owner, name, descriptor));
//    }

    @Override
    public void visitLineNumber(int line, Label start)
    {
        mutators.each((mutator) -> mutator.onLineJoin(this, methodInfo, line));
        super.visitLineNumber(line, start);
    }

    @Override
    public void visitMethodInsn(int opcodeAndSource, String owner, String name, String descriptor, boolean isInterface)
    {
        mutators.each((mutator) -> mutator.onMethodInvokeStart(this, methodInfo, owner, name, descriptor));
        super.visitMethodInsn(opcodeAndSource, owner, name, descriptor, isInterface);
        mutators.each((mutator) -> mutator.onMethodInvokeEnd(this, methodInfo, owner, name, descriptor));
    }

    @Override
    protected void onMethodEnter()
    {
        super.onMethodEnter();
        mutators.each((mutator) -> mutator.onMethodJoin(this, methodInfo));
    }

    @Override
    protected void onMethodExit(int opcode)
    {
        mutators.each((mutator) -> mutator.onMethodReturn(this, methodInfo, opcode));
        super.onMethodExit(opcode);
    }
}
