package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.storage.MethodDataStorage;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.commons.MethodRemapper;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.commons.Remapper;

@Getter
@Setter
@Accessors(chain = true)
public class NewInitContent
{
    @Internal
    @NonNull
    public IFunction1<ClassInfo, Boolean> typeFilter;

    @Internal
    @NonNull
    public MethodDataStorage newContent;

    @Internal
    @NonNull
    public Type mutationsHolderType;

    public boolean constructor;

    public void writeTo(MethodVisitor mv, Type targetClass)
    {
        var holderInternalName = getMutationsHolderType().getInternalName();

        var remapper = new Remapper()
        {
            @Override
            public String map(String internalName)
            {
                if (holderInternalName.equals(internalName))
                    return targetClass.getInternalName();

                return internalName;
            }
        };

        mv = new MethodRemapper(mv, remapper);
        mv = new NewContentConstructorVisitor(mv.api, mv).setNeedsToSkipInvokeSpecial(isConstructor());

        newContent.writeTo(mv);
    }
}
