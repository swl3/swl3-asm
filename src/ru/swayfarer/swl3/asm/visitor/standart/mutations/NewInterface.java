package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Getter
@Setter
@Accessors(chain = true)
public class NewInterface
{
    @Internal
    @NonNull
    public IFunction1<ClassInfo, Boolean> typeFilter;

    @Internal
    @NonNull
    public ExtendedList<Type> interfaces = new ExtendedList<>();
}
