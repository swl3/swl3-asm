package ru.swayfarer.swl3.asm.visitor.standart.mutations.mutators;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.MethodMutation;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.MethodMutationTarget;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.MutationConfig;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.MutationMethodVisitor;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.objects.EqualsUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

public class DefaultMutator extends AbstractMethodMutator
{
    public ExtendedMap<String, Boolean> alreadyAddedMethods = new ExtendedMap<>();

    public ILogger logger = LogFactory.getLogger();

    public DefaultMutator(@NonNull MutationConfig config, AsmUtils asmUtils)
    {
        super(config, asmUtils);
    }

    @Override
    public void onMethodJoin(MutationMethodVisitor mv, MethodInfo methodInfo)
    {
        injectMutationCall(mv, MethodMutationTarget.Start, methodInfo, -1);
        super.onMethodJoin(mv, methodInfo);
    }

    @Override
    public void onMethodReturn(MutationMethodVisitor mv, MethodInfo methodInfo, int opcode)
    {
        injectMutationCall(mv, MethodMutationTarget.Return, methodInfo, opcode);
        super.onMethodReturn(mv, methodInfo, opcode);
    }

    @Override
    public void onMethodInvokeStart(MutationMethodVisitor mv, MethodInfo callerMethodInfo, String invokeMethodOwner, String invokeMethodName, String invokeMethodDesc)
    {
        injectOnMethodCall(mv, callerMethodInfo, invokeMethodOwner, invokeMethodName, invokeMethodDesc, false);
        super.onMethodInvokeStart(mv, callerMethodInfo, invokeMethodOwner, invokeMethodName, invokeMethodDesc);
    }

    @Override
    public void onMethodInvokeEnd(MutationMethodVisitor mv, MethodInfo callerMethodInfo, String invokeMethodOwner, String invokeMethodName, String invokeMethodDesc)
    {
        injectOnMethodCall(mv, callerMethodInfo, invokeMethodOwner, invokeMethodName, invokeMethodDesc, true);
        super.onMethodInvokeEnd(mv, callerMethodInfo, invokeMethodOwner, invokeMethodName, invokeMethodDesc);
    }

    @Override
    public void onLineJoin(MutationMethodVisitor mv, MethodInfo methodInfo, int line)
    {
        IFunction1<MethodMutation, Boolean> filter = (mutation) -> {
            var dataObj = mutation.getTargetCustomData();
            if (StringUtils.isBlank(dataObj))
                return false;

            var dataStr = (String) dataObj;

            var lineNumber = Integer.valueOf(dataStr);

            if (lineNumber != line)
                return false;

            return true;
        };

        injectMutationCall(mv, MethodMutationTarget.Line, filter, methodInfo, -1);
        super.onLineJoin(mv, methodInfo, line);
    }

    private void injectOnMethodCall(MutationMethodVisitor mv, MethodInfo callerMethodInfo, String invokeMethodOwner, String invokeMethodName, String invokeMethodDesc, boolean needsAfter)
    {
        IFunction1<MethodMutation, Boolean> filterFun = (mutation) -> {
            var targetCustomData = mutation.getTargetCustomData();

            if (StringUtils.isBlank(targetCustomData))
                return false;

            var dataStr = (String) targetCustomData;

            var after = dataStr.startsWith("after ");

            if (needsAfter != after)
                return false;

            if (after)
                dataStr = dataStr.substring("after ".length());

            var firstOnly = false;

            if (dataStr.startsWith("first "))
            {
                firstOnly = true;
                dataStr = dataStr.substring("first ".length());
            }

            String owner = null;
            var targetName = dataStr;
            var descStart = "";
            var indexOfOwnerEnd = dataStr.indexOf(':');

            if (indexOfOwnerEnd > 0)
            {
                owner = toInternalName(StringUtils.strip(dataStr.substring(0, indexOfOwnerEnd)));
                dataStr = StringUtils.strip(dataStr.substring(indexOfOwnerEnd + 1));
            }

            var indexOfBracket = dataStr.indexOf('(');

            if (indexOfBracket > 0)
            {
                targetName = StringUtils.strip(dataStr.substring(0, indexOfBracket));
                descStart = getDescString(dataStr.substring(indexOfBracket + 1));
            }
            else
            {
                targetName = StringUtils.strip(dataStr);
            }

            if (!EqualsUtils.objectEquals(targetName, invokeMethodName))
                return false;

            if (!StringUtils.isBlank(descStart) && !invokeMethodDesc.startsWith(descStart))
                return false;

            if (!StringUtils.isBlank(owner) && !owner.equals(invokeMethodOwner))
                return false;

            if (firstOnly)
            {
                var key = after + owner + targetName + descStart;

                if (alreadyAddedMethods.containsKey(key))
                    return false;

                alreadyAddedMethods.put(key, true);
            }

            return true;
        };

        injectMutationCall(mv, MethodMutationTarget.MethodCall, filterFun, callerMethodInfo, -1);
    }

    public String getDescString(String str)
    {
        str = str.replace("(", "").replace(")", "");

        if (str.contains(",") || str.contains(".") || isAllLowercace(str))
        {
            var buffer = new DynamicString();
            buffer.append("(");
            var split = str.split(",");

            for (var splitPart : split)
            {
                buffer.append(toDesc(StringUtils.strip(splitPart)));
            }

            buffer.append(")");

            return buffer.toString();
        }

        return str;
    }

    public boolean isAllLowercace(String str)
    {
        for (var ch : str.toCharArray())
        {
            if (!Character.isLowerCase(ch))
                return false;
        }

        return true;
    }

    public String toInternalName(String javaFancyOwner)
    {
        if (javaFancyOwner.contains("."))
            return javaFancyOwner.replace(".", "/");

        return javaFancyOwner;
    }

    public String toDesc(String javaFancyName)
    {
        if (StringUtils.isBlank(javaFancyName))
            return javaFancyName;

        switch (javaFancyName)
        {
            case "boolean":
                return Type.BOOLEAN_TYPE.getDescriptor();

            case "char":
                return Type.CHAR_TYPE.getDescriptor();

            case "byte":
                return Type.BYTE_TYPE.getDescriptor();
            case "short":
                return Type.SHORT_TYPE.getDescriptor();
            case "int":
                return Type.INT_TYPE.getDescriptor();
            case "long":
                return Type.LONG_TYPE.getDescriptor();

            case "float":
                return Type.FLOAT_TYPE.getDescriptor();
            case "double":
                return Type.DOUBLE_TYPE.getDescriptor();
        }

        return "L" + javaFancyName.replace(".", "/") + ";";
    }
}
