package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.storage.MethodDataStorage;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.commons.MethodRemapper;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.commons.Remapper;

@Getter
@Setter
@Accessors(chain = true)
public class MethodMutation
{
    @NonNull
    @Internal
    public int targetMethodAccess;

    @NonNull
    @Internal
    public String targetMethodName;

    @NonNull
    @Internal
    public String targetMethodDesc;

    @NonNull
    @Internal
    public Type mutationsHolder;

    @NonNull
    @Internal
    public IFunction1<ClassInfo, Boolean> typeFilter;

    @NonNull
    @Internal
    public MethodMutationTarget target;

    @Internal
    public Object targetCustomData;

    @NonNull
    @Internal
    public MethodDataStorage mutationContent;

    public boolean hasMethodController;

    public void writeTo(MethodVisitor mv, Type targetClass)
    {
        var holderInternalName = mutationsHolder.getInternalName();

        var remapper = new Remapper()
        {
            @Override
            public String map(String internalName)
            {
                if (holderInternalName.equals(internalName))
                    return targetClass.getInternalName();

                return internalName;
            }
        };

        mv = new MethodRemapper(mv, remapper);

        mutationContent.writeTo(mv);
    }
}
