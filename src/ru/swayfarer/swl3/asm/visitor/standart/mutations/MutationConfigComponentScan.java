package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.classscan.ClassScan;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.annotations.MutateClass;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class MutationConfigComponentScan
{
    @Internal
    public ClassScan classScan;

    @Internal
    public BytecodeScanner bytecodeScanner;

    @Internal
    public AsmUtils asmUtils;

    @Internal
    public MutationConfigScanner configScanner;

    public MutationConfigComponentScan(ClassScan classScan, BytecodeScanner bytecodeScanner, AsmUtils asmUtils)
    {
        this.classScan = classScan;
        this.bytecodeScanner = bytecodeScanner;
        this.asmUtils = asmUtils;
        this.configScanner = new MutationConfigScanner(asmUtils, bytecodeScanner);

        classScan.eventClassScan.subscribe().by(this::scanClass);
    }

    public void scanClass(ClassInfo classInfo)
    {
        if (classInfo.getAnnotationsInfo().hasAnnotation(MutateClass.class))
        {
            configScanner.scanMutations(classInfo);
        }
    }
}
