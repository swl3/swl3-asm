package ru.swayfarer.swl3.asm.visitor.standart.mutations;

public enum MethodMutationTarget
{
    /** Inject on other method invocation in current method body */
    MethodCall,

    /** Inject on current method start */
    Start,

    /** Inject at all current method returns */
    Return,

    /** Inject at current line */
    Line
}
