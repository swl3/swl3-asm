package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import lombok.var;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.mutators.AbstractMethodMutator;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;

public class NewConstructorContentMutator extends AbstractMethodMutator
{
    public NewConstructorContentMutator(MutationConfig config, AsmUtils asmUtils)
    {
        super(config, asmUtils);
    }

    @Override
    public void onMethodReturn(MutationMethodVisitor mv, MethodInfo methodInfo, int opcode)
    {
        if (opcode != ATHROW)
        {
            if (methodInfo.isConstructor() && !methodInfo.isSecondaryConstructor())
            {
                config.findNewContstructorContentFor(methodInfo.getOwner())
                        .each((newContent) -> {
                            var methodName = genRandomName("__$swl3_mutation_constr__" + getHolderShortName(newContent.getMutationsHolderType()));
                            var cv = mv.cv;
                            var cmv = cv.visitMethod(ACC_PRIVATE, methodName, "()V", null, null);

                            newContent.writeTo(cmv, methodInfo.getOwner().getClassType());

                            mv.loadThis();
                            mv.visitMethodInsn(INVOKEVIRTUAL, methodInfo.getOwner().getClassType().getInternalName(), methodName, "()V");
                        })
                ;
            }
            else if (methodInfo.getName().equals("<clinit>"))
            {
                config.findNewClassInitContentFor(methodInfo.getOwner())
                        .each((newContent) -> {
                            var methodName = genRandomName("__$swl3_mutation_clinit__" + getHolderShortName(newContent.getMutationsHolderType()));
                            var cv = mv.cv;
                            var cmv = cv.visitMethod(ACC_PRIVATE | ACC_STATIC, methodName, "()V", null, null);

                            newContent.writeTo(cmv, methodInfo.getOwner().getClassType());

                            mv.visitMethodInsn(INVOKESTATIC, methodInfo.getOwner().getClassType().getInternalName(), methodName, "()V");
                        })
                ;
            }
        }

        super.onMethodReturn(mv, methodInfo, opcode);
    }
}
