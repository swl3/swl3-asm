package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import ru.swayfarer.swl3.asm.info.MethodInfo;

public interface IMethodMutator
{
    public void onLineJoin(MutationMethodVisitor mv, MethodInfo methodInfo, int line);
    public void onMethodJoin(MutationMethodVisitor mv, MethodInfo methodInfo);
    public void onMethodReturn(MutationMethodVisitor mv, MethodInfo methodInfo, int opcode);
    public void onMethodInvokeStart(MutationMethodVisitor mv, MethodInfo callerMethodInfo, String invokeMethodOwner, String invokeMethodName, String invokeMethodDesc);
    public void onMethodInvokeEnd(MutationMethodVisitor mv, MethodInfo callerMethodInfo, String invokeMethodOwner, String invokeMethodName, String invokeMethodDesc);
}
