package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Getter
@Setter
@Accessors(chain = true)
public class NewField
{
    @Internal
    @NonNull
    public IFunction1<ClassInfo, Boolean> typeFilter;

    @Internal
    @NonNull
    public Type fieldType;

    @Internal
    @NonNull
    public Type mutationsHolderType;

    @Internal
    @NonNull
    public String name;

    @Internal
    public int access;
}
