package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.objects.EqualsUtils;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

import java.lang.reflect.Modifier;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class MutationConfig
{
    @Internal
    public Type methodControllerType = Type.getType(MethodController.class);

    @Internal
    public Type methodControllerPoolType = Type.getType(MethodControllerPool.class);

    @Internal
    public ExtendedList<NewField> newFields = new ExtendedList<>();

    @Internal
    public ExtendedList<NewInterface> newInterfaces = new ExtendedList<>();

    @Internal
    public ExtendedList<NewMethod> newMethods = new ExtendedList<>();

    @Internal
    public ExtendedList<MethodMutation> mutations = new ExtendedList<>();

    @Internal
    public ExtendedList<NewInitContent> newConstructorContents = new ExtendedList<>();

    @Internal
    public ExtendedList<NewInitContent> newClassInitContents = new ExtendedList<>();

    public ExtendedStream<MethodMutation> findMutationsFor(MethodInfo method)
    {
        return mutations.exStream()
                .filter((mutation) -> {

                    if (!EqualsUtils.objectEquals(mutation.getTargetMethodName(), method.getName()))
                        return false;

                    if (!method.getDescriptor().startsWith(mutation.getTargetMethodDesc()))
                        return false;

                    if (mutation.getTypeFilter() == null || !Boolean.TRUE.equals(mutation.getTypeFilter().apply(method.getOwner())))
                        return false;

                    if (!isAccessesCompatible(mutation.getTargetMethodAccess(), method.getAccess().getModifier()))
                        return false;

                    return true;
                })
        ;
    }

    public boolean isAccessesCompatible(int mod1, int mod2)
    {
        if (Modifier.isStatic(mod1) != Modifier.isStatic(mod2))
            return false;

        if (Modifier.isAbstract(mod1) || Modifier.isAbstract(mod2))
            return false;

        return true;
    }

    public ExtendedStream<NewField> findNewFieldsFor(ClassInfo classInfo)
    {
        return newFields.exStream()
                .filter((newMethod) -> newMethod.getTypeFilter() != null)
                .filter((newMethod) -> Boolean.TRUE.equals(newMethod.getTypeFilter().apply(classInfo)))
        ;
    }

    public ExtendedStream<NewMethod> findNewMethodsFor(ClassInfo classInfo)
    {
        return newMethods.exStream()
                .filter((newMethod) -> newMethod.getTypeFilter() != null)
                .filter((newMethod) -> Boolean.TRUE.equals(newMethod.getTypeFilter().apply(classInfo)))
        ;
    }

    public ExtendedStream<NewInterface> findNewInterfacesFor(ClassInfo classInfo)
    {
        return newInterfaces.exStream()
                .filter((newMethod) -> newMethod.getTypeFilter() != null)
                .filter((newMethod) -> Boolean.TRUE.equals(newMethod.getTypeFilter().apply(classInfo)))
        ;
    }

    public ExtendedStream<NewInitContent> findNewContstructorContentFor(ClassInfo classInfo)
    {
        return newConstructorContents.exStream()
                .filter((newMethod) -> newMethod.getTypeFilter() != null)
                .filter((newMethod) -> Boolean.TRUE.equals(newMethod.getTypeFilter().apply(classInfo)))
        ;
    }

    public ExtendedStream<NewInitContent> findNewClassInitContentFor(ClassInfo classInfo)
    {
        return newClassInitContents.exStream()
                .filter((newMethod) -> newMethod.getTypeFilter() != null)
                .filter((newMethod) -> Boolean.TRUE.equals(newMethod.getTypeFilter().apply(classInfo)))
        ;
    }
}
