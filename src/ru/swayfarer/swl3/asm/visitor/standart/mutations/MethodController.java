package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import lombok.Getter;
import lombok.Setter;
import ru.swayfarer.swl3.markers.Internal;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Getter
@Setter
public class MethodController
{
    @Internal
    public AtomicBoolean returnTouched = new AtomicBoolean();

    @Internal
    public AtomicReference<Throwable> exception = new AtomicReference<>();

    @Internal
    public AtomicReference<Object> returnValue = new AtomicReference<>();

    public Object getReturnValue()
    {
        return returnValue.get();
    }

    public void setReturnValue(Object returnValue)
    {
        this.returnValue.set(returnValue);
        this.returnTouched.set(true);
    }

    public void setReturnValue()
    {
        setReturnValue(null);
    }

    public void setException(Throwable e)
    {
        exception.set(e);
    }

    public boolean isReturnTouched()
    {
        return returnTouched.get();
    }

    public void setReturnTouched(boolean value)
    {
        returnTouched.set(value);
    }

    @Internal
    public void clear()
    {
        returnTouched.set(false);
        returnValue.set(null);
        exception.set(null);
    }
}
