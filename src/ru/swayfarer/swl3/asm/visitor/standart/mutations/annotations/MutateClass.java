package ru.swayfarer.swl3.asm.visitor.standart.mutations.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface MutateClass
{
    /** The mutation target. Can be replaced by classname-version {@link #targetName()}*/
    public Class<?>[] target() default Object.class;

    /** The mutation target classname. Uses java-classnames, as java.lang.String and some.pkg.Class$InternalClass */
    public String[] targetName() default "";
}
