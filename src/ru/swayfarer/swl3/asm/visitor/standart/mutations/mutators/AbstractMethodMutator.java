package ru.swayfarer.swl3.asm.visitor.standart.mutations.mutators;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.IMethodMutator;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.MethodMutation;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.MethodMutationTarget;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.MutationConfig;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.MutationMethodVisitor;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.objects.EqualsUtils;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Opcodes;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
public class AbstractMethodMutator implements IMethodMutator, Opcodes
{
    @Internal
    public MutationConfig config;

    @Internal
    public AsmUtils asmUtils;

    public boolean transforming;

    public AbstractMethodMutator(MutationConfig config, AsmUtils asmUtils)
    {
        this.config = config;
        this.asmUtils = asmUtils;
    }

    @Override
    public void onLineJoin(MutationMethodVisitor mv, MethodInfo methodInfo, int line)
    {

    }

    @Override
    public void onMethodJoin(MutationMethodVisitor mv, MethodInfo methodInfo)
    {

    }

    @Override
    public void onMethodReturn(MutationMethodVisitor mv, MethodInfo methodInfo, int opcode)
    {

    }

    @Override
    public void onMethodInvokeStart(MutationMethodVisitor mv, MethodInfo callerMethodInfo, String invokeMethodOwner, String invokeMethodName, String invokeMethodDesc)
    {

    }

    @Override
    public void onMethodInvokeEnd(MutationMethodVisitor mv, MethodInfo callerMethodInfo, String invokeMethodOwner, String invokeMethodName, String invokeMethodDesc)
    {

    }

    public void injectMutationCall(MutationMethodVisitor mv, MethodMutationTarget targetPoint, MethodInfo methodInfo, int returnOpcode)
    {
        injectMutationCall(mv, targetPoint, (e) -> true, methodInfo, returnOpcode);
    }

    public void injectMutationCall(MutationMethodVisitor mv, MethodMutationTarget targetPoint, IFunction1<MethodMutation, Boolean> filter, MethodInfo methodInfo, int returnOpcode)
    {
        if (isTransforming())
            return;

        transforming = true;

        var mutationsStream = config.findMutationsFor(methodInfo)
                .filter((e) -> e.getTarget().equals(targetPoint))
                .filter(filter)
        ;

        mutationsStream.each((mutation) -> {
            var controllerType = config.getMethodControllerType();
            var controllerPoolType = config.getMethodControllerPoolType();

            var mutationContent = mutation.getMutationContent();
            var mutationMethodName = genRandomName("__$swl3_mutation_method__" + getHolderShortName(mutation.getMutationsHolder()) + "__" +mutation.getMutationContent().getMethodName());
            var targetReturnType = methodInfo.getReturnType();
            var atReturn = returnOpcode != -1;
            var isThrow = returnOpcode == ATHROW;

            var cv = mv.cv;

            var methodControllerVarId = -1;
            var originalReturnValueCacheVarId = -1;
            var throwedExceptionCacheVarId = -1;

            if (isThrow)
            {
                throwedExceptionCacheVarId = mv.newLocal(Type.getType(Throwable.class));
                mv.visitVarInsn(ASTORE, throwedExceptionCacheVarId);
            }
            else if (atReturn && !targetReturnType.equals(Type.VOID_TYPE))
            {
                originalReturnValueCacheVarId = mv.newLocal(targetReturnType);
                mv.visitVarInsn(targetReturnType.getOpcode(ISTORE), originalReturnValueCacheVarId);
            }

            // Write mutation body
            var mutationMv = cv.visitMethod(mutationContent.getAccess(), mutationMethodName, mutationContent.getDesc(), null, null);
            mutation.writeTo(mutationMv, methodInfo.getOwner().getClassType());

            // Load args for mutation method call
            if (!methodInfo.getAccess().isStatic())
            {
                mv.loadThis();
            }

            mv.loadArgs();

            if (mutation.isHasMethodController())
            {
                methodControllerVarId = mv.newLocal(controllerType);

                mv.visitMethodInsn(Opcodes.INVOKESTATIC, controllerPoolType.getInternalName(), "allocateController", "()" + controllerType.getDescriptor(), false);
                mv.visitVarInsn(ASTORE, methodControllerVarId);

                if (atReturn)
                {
                    mv.visitVarInsn(ALOAD, methodControllerVarId);

                    if (!isThrow && originalReturnValueCacheVarId != -1)
                    {
                        mv.visitVarInsn(targetReturnType.getOpcode(ILOAD), originalReturnValueCacheVarId);
                        mv.visitFieldInsn(PUTFIELD, controllerType.getInternalName(), "returnValue", "Ljava/lang/Object;");
                    }
                    else if (throwedExceptionCacheVarId != -1)
                    {
                        mv.visitVarInsn(ALOAD, throwedExceptionCacheVarId);
                        mv.visitFieldInsn(PUTFIELD, controllerType.getInternalName(), "exception", "Ljava/lang/Throwable;");
                    }
                }
            }

            // Find opcode. If mutation static than using static call, else normal INVOKEVIRTUAL
            var mutationInvokeOpcode = methodInfo.getAccess().isStatic() ? INVOKESTATIC : INVOKEVIRTUAL;

            if (methodControllerVarId != -1)
                mv.visitVarInsn(controllerType.getOpcode(ILOAD), methodControllerVarId);

            // And call mutation method
            mv.visitMethodInsn(
                    mutationInvokeOpcode,
                    methodInfo.getOwner().getClassType().getInternalName(),
                    mutationMethodName,
                    mutationContent.getDesc(),
                    false
            );

            var mutationReturnType = Type.getReturnType(mutationContent.getDesc());

            // If mutation method return type is not void than we need to clean stack from its return result
            if (!EqualsUtils.objectEquals(mutationReturnType, Type.VOID_TYPE))
                mv.visitInsn(mutationReturnType.getOpcode(POP));

            if (methodControllerVarId != -1)
            {
                mv.visitVarInsn(controllerType.getOpcode(ILOAD), methodControllerVarId);
                mv.visitMethodInsn(INVOKEVIRTUAL, controllerType.getInternalName(), "isReturnTouched", "()Z", false);
                var ifNotReturnLabel = mv.newLabel();
                mv.visitJumpInsn(IFEQ, ifNotReturnLabel);

                if (!targetReturnType.equals(Type.VOID_TYPE))
                {
                    mv.visitVarInsn(ALOAD, methodControllerVarId);
                    mv.visitMethodInsn(INVOKEVIRTUAL, controllerType.getInternalName(), "getReturnValue", "()Ljava/lang/Object;", false);
                    asmUtils.types().convesion().convert(AsmUtils.OBJECT_TYPE, targetReturnType, mv);
                }

                // Release method controller after getting return value
                mv.visitVarInsn(Opcodes.ALOAD, methodControllerVarId);
                mv.visitMethodInsn(Opcodes.INVOKESTATIC, controllerPoolType.getInternalName(), "releaseController", "(" + controllerType.getDescriptor() + ")V", false);

                mv.visitInsn(targetReturnType.getOpcode(IRETURN));
                mv.visitLabel(ifNotReturnLabel);

                // Release method controller after method return (if controller has'nt return value)
                mv.visitVarInsn(Opcodes.ALOAD, methodControllerVarId);
                mv.visitMethodInsn(Opcodes.INVOKESTATIC, controllerPoolType.getInternalName(), "releaseController", "(" + controllerType.getDescriptor() + ")V", false);
            }

            if (throwedExceptionCacheVarId != -1)
                mv.visitVarInsn(ALOAD, throwedExceptionCacheVarId);
            else if (!targetReturnType.equals(Type.VOID_TYPE) && originalReturnValueCacheVarId != -1)
                mv.visitVarInsn(targetReturnType.getOpcode(ILOAD), originalReturnValueCacheVarId);

        });


        transforming = false;
    }

    public String getHolderShortName(Type type)
    {
        var classname = type.getClassName();
        return ReflectionsUtils.getShortedClassName(classname, 1).replace(".", "_");
    }

    public String genRandomName(String prefix)
    {
        return prefix.concat("_").concat(UUID.randomUUID().toString().replace("-", ""));
    }
}
