package ru.swayfarer.swl3.asm.visitor.standart.mutations.annotations;

import ru.swayfarer.swl3.asm.visitor.standart.mutations.MethodMutationTarget;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface MutateMethod
{
    /** The point of mutation inject */
    public MethodMutationTarget targetPoint() default MethodMutationTarget.Start;

    /** The custom info of mutation inject point */
    public String targetPointCustom() default "";

    /** The custom target method name (by default used mutation-method name) */
    public String targetMethodName() default "";

    /** The custom target method desc (by default used most-compatible with mutation-method candidate from target class) */
    public String targetMethodDesc() default "";
}
