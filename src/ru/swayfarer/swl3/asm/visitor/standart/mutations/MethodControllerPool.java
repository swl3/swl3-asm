package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import ru.swayfarer.swl3.collections.pool.CachableObjectProvider;

public class MethodControllerPool
{
    public static boolean enabled = false;

    public static CachableObjectProvider<MethodController> pool = new CachableObjectProvider<>(MethodController::new, MethodController::clear, 100);

    public static MethodController allocateController()
    {
        if (enabled)
            return pool.request();

        return new MethodController();
    }

    public static void releaseController(MethodController methodController)
    {
        if (enabled)
            pool.release(methodController);
    }
}
