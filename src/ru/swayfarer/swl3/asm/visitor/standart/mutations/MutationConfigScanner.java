package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.asm.storage.StorageClassVisitor;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.annotations.MutateClass;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.annotations.MutateMethod;
import ru.swayfarer.swl3.asm.visitor.standart.mutations.annotations.Virtual;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

import java.util.stream.Collectors;

@Getter
@Setter
@Accessors(chain = true)
public class MutationConfigScanner
{
    @Internal
    public ILogger logger = LogFactory.getLogger();

    @Internal
    public AsmUtils asmUtils;

    @Internal
    public BytecodeScanner bytecodeScanner;

    @Internal
    public MutationConfig resultConfig = new MutationConfig();

    public MutationConfigScanner(AsmUtils asmUtils, BytecodeScanner bytecodeScanner)
    {
        this.asmUtils = asmUtils;
        this.bytecodeScanner = bytecodeScanner;
    }

    public void scanMutations(ClassInfo mutationsHolder)
    {
        if (mutationsHolder.getAnnotationsInfo().hasAnnotation(MutateClass.class))
        {
            logger.dev("Found mutations holder class", mutationsHolder.getClassType().getClassName());

            var targets = getMutateClassTargets(mutationsHolder);

            if (CollectionsSWL.isNullOrEmpty(targets))
            {
                logger.warn("Can't find targets for mutation holder class", mutationsHolder.getClassType().getClassName(), "that annotated as mutations holder! Skipping...");
                return;
            }

            var typesSet = targets.exStream()
                    .filter((target) -> !target.getAccess().isInterface())
                    .map(ClassInfo::getClassType)
                    .collect(Collectors.toSet())
                    ;

            var classStoredData = StorageClassVisitor.scanClass(
                    mutationsHolder.getClassType().getClassName(),
                    bytecodeScanner,
                    (visitor) -> {}
            );

            var interfaces = mutationsHolder.getInterfaces();

            if (!CollectionsSWL.isNullOrEmpty(interfaces))
            {
                logger.dev("Found new interfaces at mutation holder", mutationsHolder.getClassType().getClassName());
                var newInterface = new NewInterface();
                newInterface.setInterfaces(mutationsHolder.getInterfaces());
                newInterface.setTypeFilter((c) -> c != null && typesSet.contains(c.getClassType()));

                resultConfig.getNewInterfaces().add(newInterface);
            }

            mutationsHolder.fieldsStream()
                    .not().annotations().has(Virtual.class)
                    .each((field) -> {
                        logger.dev("Found new fields in mutation holder", mutationsHolder.getClassType().getClassName());
                        var newField = new NewField();
                        newField.setFieldType(field.getFieldType());
                        newField.setAccess(field.getAccess().getModifier());
                        newField.setName(field.getName());
                        newField.setMutationsHolderType(mutationsHolder.getClassType());
                        newField.setTypeFilter((c) -> c != null && typesSet.contains(c.getClassType()));
                        resultConfig.getNewFields().add(newField);
                    })
            ;

            mutationsHolder.methodsStream()
                    .not().annotations().has(Virtual.class)
                    .not().annotations().has(MutateMethod.class)
                    .filter(MethodInfo::isConstructor)
                    .each((constructor) -> {
                        logger.info("Found new constructor contents in mutations holder", mutationsHolder.getClassType().getClassName());

                        var newConstructorContent = new NewInitContent();
                        newConstructorContent.setMutationsHolderType(mutationsHolder.getClassType());
                        newConstructorContent.setTypeFilter((c) -> c != null && typesSet.contains(c.getClassType()));
                        newConstructorContent.setConstructor(true);
                        newConstructorContent.setNewContent(classStoredData.findMethod(constructor.getName(), constructor.getDescriptor()));
                        resultConfig.getNewConstructorContents().add(newConstructorContent);
                    })
            ;


            mutationsHolder.methodsStream()
                    .not().annotations().has(Virtual.class)
                    .not().annotations().has(MutateMethod.class)
                    .access().statics()
                    .name().is("<clinit>")
                    .each((constructor) -> {
                        logger.info("Found new class init contents in mutations holder", mutationsHolder.getClassType().getClassName());

                        var newClassInitContent = new NewInitContent();
                        newClassInitContent.setMutationsHolderType(mutationsHolder.getClassType());
                        newClassInitContent.setTypeFilter((c) -> c != null && typesSet.contains(c.getClassType()));
                        newClassInitContent.setConstructor(false);
                        newClassInitContent.setNewContent(classStoredData.findMethod(constructor.getName(), constructor.getDescriptor()));
                        resultConfig.getNewClassInitContents().add(newClassInitContent);
                    })
            ;

            mutationsHolder.methodsStream()
                    .not().annotations().has(Virtual.class)
                    .not().annotations().has(MutateMethod.class)
                    .not().filter(MethodInfo::isConstructor)
                    .each((methodInfo) -> {
                        logger.info("Found new method in mutations holder", mutationsHolder.getClassType().getClassName());

                        var newMethod = new NewMethod();
                        newMethod.setTypeFilter((c) -> c != null && typesSet.contains(c.getClassType()));
                        newMethod.setMethodContent(classStoredData.findMethod(methodInfo.getName(), methodInfo.getDescriptor()));
                        newMethod.setMutationsHolderType(mutationsHolder.getClassType());

                        resultConfig.getNewMethods().add(newMethod);
                    })
            ;

            mutationsHolder.methodsStream()
                    .annotations().has(MutateMethod.class)
                    .each((methodInfo) -> {
                        logger.dev("Found mutation method in holder class", mutationsHolder.getClassType().getClassName());

                        var mutation = loadMethodMutationSettings(methodInfo);

                        mutation.setTypeFilter((c) -> c != null && typesSet.contains(c.getClassType()));
                        mutation.setMutationContent(classStoredData.findMethod(methodInfo.getName(), methodInfo.getDescriptor()));

                        resultConfig.getMutations().add(mutation);
                    })
            ;
        }
    }

    public void scanMutations(Type classWithMutations)
    {
        var mutationsHolder = bytecodeScanner.findOrCreateClassInfo(classWithMutations);
        scanMutations(mutationsHolder);
    }

    public MethodMutation loadMethodMutationSettings(MethodInfo methodInfo)
    {
        var mutation = new MethodMutation();

        var mutateMethodAnnotation = methodInfo.getAnnotationsInfo().first(MutateMethod.class);

        // Trying to load target point from annotation or using default value - MethodMutationTarget.Start
        var mutationTagetPoint = MethodMutationTarget.Start;
        var mutationTargetPointParam = mutateMethodAnnotation.getParam("targetPoint", String.class);

        if (!StringUtils.isBlank(mutationTargetPointParam))
            mutationTagetPoint = MethodMutationTarget.valueOf(mutationTargetPointParam);

        var mutationTargetPointCustomDataParam = mutateMethodAnnotation.getParam("targetPointCustom", String.class);

        // Trying to load target method name from annotation or using default value - mutation method name
        var mutationTargetMethodName = methodInfo.getName();
        var mutationTargetMethodNameParam = mutateMethodAnnotation.getParam("targetMethodName", String.class);

        if (!StringUtils.isBlank(mutationTargetMethodNameParam))
            mutationTargetMethodName = mutationTargetMethodNameParam;

        // Trying to find fixed target method descriptor. If not found the most compatible candidates will be used later
        var mutationTargetMethodDesc = calculateTargetDescStart(methodInfo);
        var mutationTargetMethodDescParam = mutateMethodAnnotation.getParam("targetMethodDesc", String.class);

        if (!StringUtils.isBlank(mutationTargetMethodDescParam))
            mutationTargetMethodDesc = mutationTargetMethodDescParam;

        mutation
                .setTargetMethodAccess(methodInfo.getAccess().getModifier())

                .setTargetMethodName(mutationTargetMethodName)
                .setTargetMethodDesc(mutationTargetMethodDesc)
                .setMutationsHolder(methodInfo.getOwner().getClassType())

                .setTarget(mutationTagetPoint)
                .setTargetCustomData(mutationTargetPointCustomDataParam)

                .setHasMethodController(methodInfo.getParams().exStream().contains((e) -> e.getParamType().equals(resultConfig.getMethodControllerType())))
        ;

        return mutation;
    }

    public String calculateTargetDescStart(MethodInfo methodInfo)
    {
        var params = methodInfo.getParams().copy();
        params.filter((param) -> !param.getParamType().equals(resultConfig.getMethodControllerType()));
        var buffer = new DynamicString();
        buffer.append("(");
        for (var param : params)
        {
            buffer.append(param.getParamType().getDescriptor());
        }
        buffer.append(")");

        return buffer.toString();
    }

    public ExtendedList<ClassInfo> getMutateClassTargets(ClassInfo mutationsHolder)
    {
        var mutateClassAnnotation = mutationsHolder.getAnnotationsInfo().first(MutateClass.class);
        var targets = new ExtendedList<ClassInfo>();

        // Trying to find target class by classname
        {
            var customTargetNames = mutateClassAnnotation.getParams("targetName", String.class);

            if (!CollectionsSWL.isNullOrEmpty(customTargetNames))
            {
                for (var customTargetName : customTargetNames)
                {
                    var classDesc = "L" + customTargetName.replace(".", "/") + ";";
                    var targetClassInfo = bytecodeScanner.findOrCreateClassInfo(Type.getType(classDesc));

                    if (targetClassInfo == null)
                        logger.warn("Can't find mutation target class", customTargetName);

                    if (targetClassInfo != null)
                        targets.add(targetClassInfo);
                }
            }
        }

        // Trying to find target class by class parameter
        {
            var targetClassTypes = mutateClassAnnotation.getParams("target", Type.class);

            if (!CollectionsSWL.isNullOrEmpty(targetClassTypes))
            {
                for (var targetClassType : targetClassTypes)
                {
                    if (targetClassType != null)
                    {
                        var targetClassInfo = bytecodeScanner.findOrCreateClassInfo(targetClassType);

                        if (targetClassInfo == null)
                            logger.warn("Can't find mutation target class", targetClassType.getClassName());

                        if (targetClassInfo != null)
                            targets.add(targetClassInfo);
                    }
                }
            }
        }

        return targets.exStream().distinctBy((e) -> e.getClassType().getInternalName()).toExList();
    }
}
