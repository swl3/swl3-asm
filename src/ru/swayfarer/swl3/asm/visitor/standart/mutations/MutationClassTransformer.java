package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import lombok.NonNull;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.asm.transformer.ClassBytecodeInfo;
import ru.swayfarer.swl3.asm.transformer.InformatedAsmClassTransformer;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassReader;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassWriter;

public class MutationClassTransformer extends InformatedAsmClassTransformer
{
    @Internal
    public MutationConfig config;

    @Internal
    public AsmUtils asmUtils;

    public MutationClassTransformer(@NonNull MutationConfig config, @NonNull BytecodeScanner bytecodeScanner, @NonNull AsmUtils asmUtils)
    {
        super(bytecodeScanner);
        this.config = config;
        this.asmUtils = asmUtils;
    }

    @Override
    public ClassVisitor transformInformated(ClassInfo classInfo, ClassReader reader, ClassWriter writer, ClassBytecodeInfo event)
    {
        return new MutationClassVisitor(writer, classInfo, config, asmUtils);
    }

//    @Internal
//    public ClassWriter getWriter(@NonNull ClassBytecodeInfo event)
//    {
//        return new ClassWriter(0);
//    }
}
