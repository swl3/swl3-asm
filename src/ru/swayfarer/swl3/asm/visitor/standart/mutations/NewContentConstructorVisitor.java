package ru.swayfarer.swl3.asm.visitor.standart.mutations;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;

@Getter
@Setter
@Accessors(chain = true)
public class NewContentConstructorVisitor extends MethodVisitor
{
    public long newCount;
    public boolean needsToSkipInvokeSpecial;

    public NewContentConstructorVisitor(int api, MethodVisitor methodVisitor)
    {
        super(api, methodVisitor);
    }

    @Override
    public void visitInsn(int opcode)
    {
        if (opcode == NEW)
            newCount ++;

        super.visitInsn(opcode);
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface)
    {
        if (newCount == 0)
        {
            if (isNeedsToSkipInvokeSpecial() && opcode == INVOKESPECIAL)
            {
                visitInsn(POP);
                needsToSkipInvokeSpecial = false;
                return;
            }
        }
        else if (opcode == INVOKESPECIAL)
        {
            newCount --;
        }

        super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
    }
}
