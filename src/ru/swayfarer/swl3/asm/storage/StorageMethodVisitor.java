package ru.swayfarer.swl3.asm.storage;

import lombok.var;
import ru.swayfarer.swl3.asm.utils.AsmUtils;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.AnnotationVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Attribute;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassWriter;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Handle;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Label;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.TypePath;

import java.util.UUID;

public class StorageMethodVisitor extends MethodVisitor
{
    public ClassWriter cw;
    public MethodVisitor cwmv;
    public String name;
    public String desc;
    public int access;

    public StorageMethodVisitor(
            int api,
            MethodVisitor mv,
            int classVersion,
            int access,
            String name,
            String descriptor,
            String signature,
            String[] exceptions
    )
    {
        super(api, mv);

        this.name = name;
        this.desc = descriptor;
        this.access = access;

        cw = new ClassWriter(0);
        var className = UUID.randomUUID().toString().replace("-", "_") + UUID.randomUUID().toString().replace("-", "_");
        cw.visit(classVersion, ACC_PUBLIC | ACC_SUPER, className, null, AsmUtils.OBJECT_TYPE.getInternalName(), null);
        cwmv = cw.visitMethod(access, name, descriptor,signature,exceptions);
    }

    public MethodDataStorage getMethodData()
    {
        cw.visitEnd();
        var rawClassBytes = cw.toByteArray();

        var storage = new MethodDataStorage();
        storage.setMethodName(name);
        storage.setAccess(access);
        storage.setDesc(desc);
        storage.setMethodBytes(rawClassBytes);

        return storage;
    }

    @Override
    public void visitParameter(String name, int access)
    {
        cwmv.visitParameter(name, access);
        super.visitParameter(name, access);
    }

    @Override
    public AnnotationVisitor visitAnnotationDefault()
    {
        var delegate = cwmv.visitAnnotationDefault();
        return new StorageAnnotationVisitor(api, super.visitAnnotationDefault(), delegate);
    }

    @Override
    public AnnotationVisitor visitAnnotation(String descriptor, boolean visible)
    {
        var delegate = cwmv.visitAnnotation(descriptor, visible);
        return new StorageAnnotationVisitor(api, super.visitAnnotation(descriptor, visible), delegate);
    }

    @Override
    public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible)
    {
        var delegate = cwmv.visitTypeAnnotation(typeRef, typePath, descriptor, visible);
        return new StorageAnnotationVisitor(api, super.visitTypeAnnotation(typeRef, typePath, descriptor, visible), delegate);
    }

    @Override
    public void visitAnnotableParameterCount(int parameterCount, boolean visible)
    {
        cwmv.visitAnnotableParameterCount(parameterCount, visible);
        super.visitAnnotableParameterCount(parameterCount, visible);
    }

    @Override
    public AnnotationVisitor visitParameterAnnotation(int parameter, String descriptor, boolean visible)
    {
        var delegate = cwmv.visitParameterAnnotation(parameter, descriptor, visible);
        return new StorageAnnotationVisitor(api, super.visitParameterAnnotation(parameter, descriptor, visible), delegate);
    }

    @Override
    public void visitParameterCount(int count)
    {
        cwmv.visitParameterCount(count);
        super.visitParameterCount(count);
    }

    @Override
    public void visitParameter(String name, String signature, int index)
    {
        cwmv.visitParameter(name, signature, index);
        super.visitParameter(name, signature, index);
    }

    @Override
    public void visitAttribute(Attribute attribute)
    {
        cwmv.visitAttribute(attribute);
        super.visitAttribute(attribute);
    }

    @Override
    public void visitCode()
    {
        cwmv.visitCode();
        super.visitCode();
    }

    @Override
    public void visitFrame(int type, int numLocal, Object[] local, int numStack, Object[] stack)
    {
        cwmv.visitFrame(type, numLocal, local, numStack, stack);
        super.visitFrame(type, numLocal, local, numStack, stack);
    }

    @Override
    public void visitInsn(int opcode)
    {
        cwmv.visitInsn(opcode);
        super.visitInsn(opcode);
    }

    @Override
    public void visitIntInsn(int opcode, int operand)
    {
        cwmv.visitIntInsn(opcode, operand);
        super.visitIntInsn(opcode, operand);
    }

    @Override
    public void visitVarInsn(int opcode, int var)
    {
        cwmv.visitVarInsn(opcode, var);
        super.visitVarInsn(opcode, var);
    }

    @Override
    public void visitTypeInsn(int opcode, String type)
    {
        cwmv.visitTypeInsn(opcode, type);
        super.visitTypeInsn(opcode, type);
    }

    @Override
    public void visitFieldInsn(int opcode, String owner, String name, String descriptor)
    {
        cwmv.visitFieldInsn(opcode, owner, name, descriptor);
        super.visitFieldInsn(opcode, owner, name, descriptor);
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String descriptor)
    {
        cwmv.visitMethodInsn(opcode, owner, name, descriptor);
        super.visitMethodInsn(opcode, owner, name, descriptor);
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface)
    {
        cwmv.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
        super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
    }

    @Override
    public void visitInvokeDynamicInsn(String name, String descriptor, Handle bootstrapMethodHandle, Object... bootstrapMethodArguments)
    {
        cwmv.visitInvokeDynamicInsn(name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments);
        super.visitInvokeDynamicInsn(name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments);
    }

    @Override
    public void visitJumpInsn(int opcode, Label label)
    {
        cwmv.visitJumpInsn(opcode, label);
        super.visitJumpInsn(opcode, label);
    }

    @Override
    public void visitLabel(Label label)
    {
        cwmv.visitLabel(label);
        super.visitLabel(label);
    }

    @Override
    public void visitLdcInsn(Object value)
    {
        cwmv.visitLdcInsn(value);
        super.visitLdcInsn(value);
    }

    @Override
    public void visitIincInsn(int var, int increment)
    {
        cwmv.visitIincInsn(var, increment);
        super.visitIincInsn(var, increment);
    }

    @Override
    public void visitTableSwitchInsn(int min, int max, Label dflt, Label... labels)
    {
        cwmv.visitTableSwitchInsn(min, max, dflt, labels);
        super.visitTableSwitchInsn(min, max, dflt, labels);
    }

    @Override
    public void visitLookupSwitchInsn(Label dflt, int[] keys, Label[] labels)
    {
        cwmv.visitLookupSwitchInsn(dflt, keys, labels);
        super.visitLookupSwitchInsn(dflt, keys, labels);
    }

    @Override
    public void visitMultiANewArrayInsn(String descriptor, int numDimensions)
    {
        cwmv.visitMultiANewArrayInsn(descriptor, numDimensions);
        super.visitMultiANewArrayInsn(descriptor, numDimensions);
    }

    @Override
    public AnnotationVisitor visitInsnAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible)
    {
        var delegate = cwmv.visitInsnAnnotation(typeRef, typePath, descriptor, visible);
        return new StorageAnnotationVisitor(api, super.visitInsnAnnotation(typeRef, typePath, descriptor, visible), delegate);
    }

    @Override
    public void visitTryCatchBlock(Label start, Label end, Label handler, String type)
    {
        cwmv.visitTryCatchBlock(start, end, handler, type);
        super.visitTryCatchBlock(start, end, handler, type);
    }

    @Override
    public AnnotationVisitor visitTryCatchAnnotation(int typeRef, TypePath typePath, String descriptor, boolean visible)
    {
        var delegate = cwmv.visitTryCatchAnnotation(typeRef, typePath, descriptor, visible);
        return new StorageAnnotationVisitor(api, super.visitTryCatchAnnotation(typeRef, typePath, descriptor, visible), delegate);
    }

    @Override
    public void visitLocalVariable(String name, String descriptor, String signature, Label start, Label end, int index)
    {
        cwmv.visitLocalVariable(name, descriptor, signature, start, end, index);
        super.visitLocalVariable(name, descriptor, signature, start, end, index);
    }

    @Override
    public AnnotationVisitor visitLocalVariableAnnotation(int typeRef, TypePath typePath, Label[] start, Label[] end, int[] index, String descriptor, boolean visible)
    {
        var delegate = cwmv.visitLocalVariableAnnotation(typeRef, typePath, start, end, index, descriptor, visible);
        return new StorageAnnotationVisitor(api, super.visitLocalVariableAnnotation(typeRef, typePath, start, end, index, descriptor, visible), delegate);
    }

    @Override
    public void visitLineNumber(int line, Label start)
    {
        cwmv.visitLineNumber(line, start);
        super.visitLineNumber(line, start);
    }

    @Override
    public void visitMaxs(int maxStack, int maxLocals)
    {
        cwmv.visitMaxs(maxStack, maxLocals);
        super.visitMaxs(maxStack, maxLocals);
    }

    @Override
    public void visitEnd()
    {
        cwmv.visitEnd();
        super.visitEnd();
    }
}
