package ru.swayfarer.swl3.asm.storage;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.visitor.standart.RedirectMethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassReader;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;

@Getter
@Setter
@Accessors(chain = true)
public class MethodDataStorage
{
    /** Stores dummy-class with only one method - target*/
    @NonNull
    public byte[] methodBytes;

    /** The method name*/
    @NonNull
    public String methodName;

    /** The method desc */
    @NonNull
    public String desc;

    public int access;

    public void writeTo(MethodVisitor mv)
    {
        var redirect = new MethodVisitor(mv.api, mv){};
        var cv = new WriteClassVisitor(redirect, desc, methodName);

        var classReader = new ClassReader(methodBytes);
        classReader.accept(cv, 0);
    }

    public static class WriteClassVisitor extends ClassVisitor
    {
        public String targetDesc;
        public String targetName;
        public MethodVisitor redirectMv;

        public WriteClassVisitor(MethodVisitor redirectMv, String targetDesc, String targetName)
        {
            super(redirectMv.api);
            this.targetDesc = targetDesc;
            this.targetName = targetName;
            this.redirectMv = redirectMv;
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions)
        {
            if (name.equals(targetName) && descriptor.equals(targetDesc))
                return redirectMv;

            return super.visitMethod(access, name, descriptor, signature, exceptions);
        }
    }
}
