package ru.swayfarer.swl3.asm.storage;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class ClassDataStorage
{
    @Internal
    public ExtendedList<MethodDataStorage> methods = new ExtendedList<>();

    public MethodDataStorage findMethod(String name, String desc)
    {
        return methods.exStream()
                .nonNull()
                .filter((m) -> m.getMethodName().equals(name))
                .filter((m) -> m.getDesc().equals(desc))
                .findFirst().orNull()
        ;
    }
}
