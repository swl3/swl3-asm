package ru.swayfarer.swl3.asm.storage;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asm.info.ClassInfo;
import ru.swayfarer.swl3.asm.info.MethodInfo;
import ru.swayfarer.swl3.asm.scanner.BytecodeScanner;
import ru.swayfarer.swl3.asm.visitor.InformatedClassVisitor;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassReader;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.ClassVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.MethodVisitor;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.Type;

@Getter
@Setter
@Accessors(chain = true)
public class StorageClassVisitor extends InformatedClassVisitor
{
    @Internal
    public ExtendedList<StorageMethodVisitor> methodDataVisitors = new ExtendedList<>();

    @Internal
    @NonNull
    public IFunction1<MethodInfo, Boolean> methodsStoreFilter = (method) -> true;

    public StorageClassVisitor(ClassVisitor classVisitor, @NonNull ClassInfo classInfo)
    {
        super(classVisitor, classInfo);
    }

    @Override
    public MethodVisitor visitMethodInformated(MethodInfo methodInfo, int access, String name, String descriptor, String signature, String[] exceptions)
    {
        var mv = super.visitMethodInformated(methodInfo, access, name, descriptor, signature, exceptions);

        if (Boolean.TRUE.equals(methodsStoreFilter.apply(methodInfo)))
        {
            var smv = new StorageMethodVisitor(api, mv, classInfo.version, access, name, descriptor, signature, exceptions);
            methodDataVisitors.add(smv);
            mv = smv;
        }

        return mv;
    }

    public ExtendedList<MethodDataStorage> getStoredMethods()
    {
        return methodDataVisitors.exStream()
                .map(StorageMethodVisitor::getMethodData)
                .toExList()
        ;
    }

    @SneakyThrows
    public static ClassDataStorage scanClass(
            String classname,
            BytecodeScanner bytecodeScanner,
            IFunction1NoR<StorageClassVisitor> configurator
    )
    {
        var classDesc = "L" + classname.replace(".","/") + ";";
        var classInfo = bytecodeScanner.findOrCreateClassInfo(Type.getType(classDesc));

        var cr = new ClassReader(classname);
        var cv = new StorageClassVisitor(null, classInfo);

        if (configurator != null)
            configurator.apply(cv);

        cr.accept(cv, 0);

        var result = new ClassDataStorage();
        result.getMethods().addAll(cv.getStoredMethods());

        return result;
    }
}
