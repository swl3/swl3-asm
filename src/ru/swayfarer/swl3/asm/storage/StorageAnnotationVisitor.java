package ru.swayfarer.swl3.asm.storage;

import lombok.var;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.z.dependencies.org.objectweb.asm.AnnotationVisitor;

public class StorageAnnotationVisitor extends AnnotationVisitor
{
    @Internal
    public AnnotationVisitor cwav;

    public StorageAnnotationVisitor(int api, AnnotationVisitor annotationVisitor, AnnotationVisitor cwav)
    {
        super(api, annotationVisitor);
        this.cwav = cwav;
    }

    @Override
    public void visit(String name, Object value)
    {
        cwav.visit(name, value);
        super.visit(name, value);
    }

    @Override
    public void visitEnum(String name, String descriptor, String value)
    {
        cwav.visitEnum(name, descriptor, value);
        super.visitEnum(name, descriptor, value);
    }

    @Override
    public AnnotationVisitor visitAnnotation(String name, String descriptor)
    {
        var delegate = cwav.visitAnnotation(name, descriptor);
        return new StorageAnnotationVisitor(api, super.visitAnnotation(name, descriptor), delegate);
    }

    @Override
    public AnnotationVisitor visitArray(String name)
    {
        var delegate = cwav.visitArray(name);
        return new StorageAnnotationVisitor(api, super.visitArray(name), delegate);
    }

    @Override
    public void visitEnd()
    {
        cwav.visitEnd();
    }
}
